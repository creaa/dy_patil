<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*======================================================================
							ABOUT-US ROUTES
=======================================================================*/
$route['about-university']			= 'Home/about_university';
$route['founder']					= 'Home/founder';
$route['president']					= 'Home/president';
$route['managing']					= 'Home/managing';
$route['vice-chancellor']			= 'Home/vice_chancellor';
$route['registrar']		       		= 'Home/register';
$route['controller-of-examination']	= 'Home/controller_of_examination';
$route['finance-and-accounts-officer']= 'Home/finance_and_accounts_officer';
$route['faculties']		        = 'Home/faculties';

$route['vision-mision']			= 'Home/vision_mision';
$route['core-values']			= 'Home/core_values';
$route['ethics']				= 'Home/ethics';
$route['ranking']				= 'Home/ranking';
$route['advisory']				= 'Home/advisory';
$route['administration']		= 'Home/administration';
$route['committees']			= 'Home/committees';
$route['university-regulations']= 'Home/university_regulations';
/*======================================================================
							SCHOOL ROUTES
========================================================================*/
$route['civil-infrastructural-engineering']	= 'Home/civil_engineering';
$route['electrical-electronics-engineering']= 'Home/ele_electronics_engineering';
$route['mechanical-engineering']	= 'Home/mechanical_engineering';
$route['information-technology-engineering']= 'Home/it_engineering';
$route['computer-information']		= 'Home/computer_information';
$route['automobile-engineering']	= 'Home/automobile_engineering';
$route['school-of-architecture']	= 'Home/school_of_architecture';
$route['school-of-pharmacy']		= 'Home/school_of_pharmacy';
$route['school-of-business-management']='Home/business_management_school';
$route['school-of-hospitality-tourism-studies']= 'Home/hospitality_tourism_studies';
/*======================================================================
							ADMISSION ROUTES
=======================================================================*/
$route['admission-process']			= "Home/admission_process";
$route['video']					= "Home/Videos";
/*=======================================================================
							RESEARCH ROUTES
=======================================================================*/
$route['directorate-of-research']	="Home/directorate_of_research";
$route['plagiarism-policy']			="Home/plagiarism_policy";
$route['research-policy']			="Home/research_policy";
$route['research-committee']		="Home/research_committee";
/*=======================================================================
						CAMPUS FACILITIES ROUTES
=======================================================================*/
$route['library']			= "Home/library";
$route['transportation']	= "Home/transportation";
$route['student-residences']= "Home/student_residences";
$route['cafeteria']			= "Home/cafeteria";
$route['banking']			= "Home/banking";
$route['gymnasium']			= "Home/gymnasium";
$route['healthcare']		= "Home/healthcare";
$route['sports']			= "Home/sports";
/*====================================================================
					SPORTS AND CULTURAL	ROUTES
======================================================================*/
$route['about-sports']		= "Home/about_sports";
$route['facilities']		= "Home/facilities";
$route['events']			= "Home/events";
/*====================================================================
					PLACEMENTS & INTERNSHIPS
======================================================================*/
$route['placement-about']	= "Home/placement_about";
$route['recruiters']		= "Home/recruiters";
/*====================================================================
						CONTACT US ROUTES
/*====================================================================*/
$route['sitemap']		= "Home/sitemap";
$route['contact']		= "Home/contact";
/*====================================================================*/