<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	private $num_rows = 8;
	public function __construct()
	{
		parent:: __construct();
		$this->method_call=& get_instance();
		error_reporting(1);
		$this->load->helper(array('url','master_helper','date','email','pagination'));
		$this->load->library(array('form_validation','session'));
		$this->load->model('Welcome_model');
		$this->load->model('admin/Home_model');
		$this->load->model('admin/Values_List_Model');
	}
	function index()
	{
		$result	=	$this->Home_model->home_page_details();
		$value	=	$this->Values_List_Model->get_all_values();
		 foreach($result as $val){
			if($val->page_section_id=='1'){
				$data['dypu'][]=$val;
			}
			if($val->page_section_id=='2'){
				$data['Programs_Offered'][]=$val;
			}
			if($val->page_section_id=='3'){
				$data['testimonials'][]=$val;
			}
		} 
		// echo'<pre>';print_r($data['dypu']); die;
		$this->template->set_layout(MAIN)->build('index.php',$data);
	}
	public function about_university()
	{
		$where=4;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('about-university',$data);
	}
	public function founder()
	{
		$where=5;
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('founder',$data);
	}
	public function president()
	{
		$where=6;
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('founder',$data);
	}
	public function managing()
	{
		$where=7;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('managing',$data);
	}
	public function vice_chancellor()
	{
		$where=8;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('vice-chancellor',$data);
	}
	public function register()
	{
		$where=47;
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('registrar',$data);
	}
	public function controller_of_examination()
	{
		$where=48;
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('controller-of-examination',$data);
	}
	public function finance_and_accounts_officer()
	{
		$where=49;
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('finance-and-accounts-officer',$data);
	}
	public function faculties()
	{
		$value	=	$this->Values_List_Model->get_all_values();
		foreach($value as $liv){
			$data['name'][$liv->id]=$liv->values;
		}
		$faculties=$this->Welcome_model->faculties_details();
		foreach($faculties as $key=>$val){
			$data['fac'][$val['dpt_id']][]=$val;
			$data['dpt'][$val['dpt_id']]=$val['dpt_id'];
		}
		$this->template->set_layout(MAIN)->build('faculties',$data);
	}
	public function vision_mision()
	{
		$where=9;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$detail=$this->Welcome_model->get_about_us_details($where);
		foreach($detail as $key=>$vl){
			$mainid[]=$vl['id'];
		}
		foreach($detail as $key=>$vals){
			foreach($vals as $ky=>$value){
				$cnt=0;
				$cnt1=1;
				if($ky !="image" && $ky !="date_created" && $ky !="date_modified" && $ky !="page_name_id" && $ky !="id"){
					$data[$vals['id']][$cnt][]=$value;
				}
				elseif($ky=="image" && $ky!="date_created" && $ky !="date_modified" && $ky !="page_name_id"){
					$data[$vals['id']][$cnt1][2]=$value;
				}
				$cnt++;
				$cnt1++;
			}
		}
		$this->template->set_layout(MAIN)->build('vision-mision',array('ft'=>$data,'main'=>$mainid));
	}
	public function core_values()
	{
		$where=10;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('core-values',$data);
	}
	public function ethics()
	{
		$where=11;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$detail=$this->Welcome_model->get_about_us_details($where);
		foreach($detail as $key=>$val){
			if(strtolower($val['title'])=="ethics & code of conduct" || $val['id']==11){
				$data['heading']=$val;
			}
			else{
				$data['accordians'][]=$val;
			}
		}
		$this->template->set_layout(MAIN)->build('ethics',$data);
	}
	public function ranking()
	{
		$where=12;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('ranking',$data);
	}
	public function advisory()
	{
		$where=13;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('advisory',$data);
	}
	public function administration()
	{
		$where=14;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('administration',$data);
	}
	public function university_regulations()
	{
		$where=15;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['detail']=$this->Welcome_model->get_about_us_details($where);
		$this->template->set_layout(MAIN)->build('university-regulations',$data);
	}
	public function committees()
	{
		$where=44;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$committees=$this->Welcome_model->get_committees();
		foreach($committees as $names){
			if($names['committee_name']=="" || $names['no_of_members']==""){
				$data['heading']=$names;
			}
			else{
				$data['committee'][]=$names;
			}
		}
		$this->template->set_layout(MAIN)->build('committees',$data);
	}
	/* ================================== */
	public function school_of_architecture()
	{
		$where=17;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']	 =	$this->Welcome_model->school_information($where);
		$data['calendar']=	$this->Welcome_model->academic_calendar($where);
		$data['header']	 =	$this->Welcome_model->calendar_header($where);
		$this->template->set_layout(MAIN)->build('school-of-architecture',$data);
	}
	public function civil_engineering()
	{
		$where=16;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('civil-infrastructural-engineering',$data);
	}
	public function ele_electronics_engineering()
	{
		$where=21;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('electrical-electronics-engineering',$data);
	}
	public function mechanical_engineering()
	{
		$where=22;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('mechanical-engineering',$data);
	}
	public function it_engineering()
	{
		$where=23;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('information-technology-engineering',$data);
	}
	public function computer_information()
	{
		$where=24;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('computer-information',$data);
	}
	public function automobile_engineering()
	{
		$where=25;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('automobile-engineering',$data);
	}
	public function school_of_pharmacy()
	{
		$where=18;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('school-of-pharmacy',$data);
	}
	public function business_management_school()
	{
		$where=19;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('school-of-business-management',$data);
	}
	public function hospitality_tourism_studies()
	{
		$where=20;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['info']		=	$this->Welcome_model->school_information($where);
		$data['calendar']	=	$this->Welcome_model->academic_calendar($where);
		$data['header']		=	$this->Welcome_model->calendar_header();
		$this->template->set_layout(MAIN)->build('school-of-hospitality-tourism-studies',$data);
	}
	public function directorate_of_research()
	{
		$where=26;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['dir']=$this->Welcome_model->get_research($where);
		// echo'<pre>';print_r($data['dir']);
		$this->template->set_layout(MAIN)->build('directorate-of-research',$data);
	}
	public function plagiarism_policy()
	{
		$where=27;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['plagiarism']=$this->Welcome_model->get_research($where);
		// echo'<pre>';print_r($data); die;
		$this->template->set_layout(MAIN)->build('plagiarism-policy',$data);
	}
	public function research_policy()
	{
		$where=28;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['policy']=$this->Welcome_model->get_research($where);
		$this->template->set_layout(MAIN)->build('research-policy',$data);
	}
	public function research_committee()
	{
		$where=29;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['comm']=$this->Welcome_model->get_research($where);
		// echo'<pre>';print_r($data); die;
		$this->template->set_layout(MAIN)->build('research-committee',$data);
	}
	public function library()
	{
		$where=30;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('247-library',$data);
	}
	public function transportation()
	{
		$where=31;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('transportation',$data);
	}
	public function student_residences()
	{
		$where=32;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('student-residences',$data);
	}
	public function cafeteria()
	{
		$where=33;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('cafeteria',$data);
	}
	public function banking()
	{
		$where=34;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('banking',$data);
	}
	public function gymnasium()
	{
		$where=35;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('gymnasium',$data);
	}
	public function healthcare()
	{
		$where=36;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('healthcare',$data);
	}
	public function sports()
	{
		$where=37;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['fac']=$this->Welcome_model->campus_facilities($where);
		$this->template->set_layout(MAIN)->build('sports',$data);
	}
	public function about_sports()
	{
		$where=38;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['sc']=$this->Welcome_model->sports_and_cultural($where);
		$this->template->set_layout(MAIN)->build('about-sports',$data);
	}
	public function facilities()
	{
		$where=39;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['sc']=$this->Welcome_model->sports_and_cultural($where);
		$this->template->set_layout(MAIN)->build('facilities',$data);
	}
	public function events()
	{
		$where=40;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['sc']=$this->Welcome_model->sports_and_cultural($where);
		$this->template->set_layout(MAIN)->build('events',$data);
	}
	public function placement_about()
	{
		$where=41;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['pi']=$this->Welcome_model->placement_intern($where);
		// print_r($data['pi']);die;
		$this->template->set_layout(MAIN)->build('placement-about',$data);
	}
	public function recruiters()
	{
		$where=42;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['pi']=$this->Welcome_model->placement_intern($where);
		$this->template->set_layout(MAIN)->build('recruiters',$data);
	}
	public function sitemap()
	{
		$this->template->set_layout(MAIN)->build('sitemap',$data);
	}
	public function contact()
	{
		$this->template->set_layout(MAIN)->build('contact',$data);
	}
	public function admission_process()
	{
		$where=45;
		$data['banners']=$this->Welcome_model->banner_images($where);
		$data['adm']=$this->Welcome_model->admission_process();
		$this->template->set_layout(MAIN)->build('admission-process',$data);
	}
	public function Videos()
	{
		$this->template->set_layout(MAIN)->build('video',$data);
	}
}
?>