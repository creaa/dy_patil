<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		// var_dump(is_logged_in());
		if(is_logged_in()){
			redirect(admin_url('clients'),'refresh');
		}
		$data = array();
		if($this->input->post(NULL,TRUE))
		{
			if($this->form_validation->run('login') !== FALSE)
			{
				$data = $this->input->post(NULL,TRUE);
				$validate_user = $this->dashboard->validate_user($data['username'],$data['password']);
				// print_r();
				if(!$validate_user)
				{
					$data['password_error'] = 'Username Password Mismatch';
				}
				else
				{
					 // echo 'succ'; die;
					// print_r($this->session); die;
					$this->session->set_userdata('user_id',$validate_user['id']);
					$this->session->set_userdata('name',$validate_user['username']);
					// var_dump($this->session->all_userdata());
					// echo admin_url('testimonials');exit;
					redirect(admin_url('testimonials'), 'refresh');
				}
			}
		}
			$this->load->view('admin/login',$data);
		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(admin_url('login'));
	}

	public function change_password()
	{
		if(!is_logged_in()){
			redirect(admin_url('login'),'refresh');
		}
		$data = array();
		$data['tab_title'] = 'Change Password';

		if($this->input->post())
		{
			$post = $this->input->post(NULL,TRUE);
			if($this->form_validation->run('change_password'))
			{	
				$this->dashboard->update_data('admin_user',['password'=>password_hash($post['password'],PASSWORD_BCRYPT)],['id'=>$this->session->userdata('user_id')]);
				redirect(admin_url('logout'),'refresh');

			}
		}

		$this->load->view('admin/header', $data);
		$this->load->view('admin/change_password');
		$this->load->view('admin/footer');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/admin/Login.php */