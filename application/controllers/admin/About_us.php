<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class About_us extends CI_controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/About_us_Model');
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['details']=$this->About_us_Model->get_aboutus_deatils();
		$this->load->view('admin/about_us',$data);
	}
	public function addcontent()
	{		
		// $this->form_validation->set_rules('university_title','Title','required|trim');
		$this->form_validation->set_rules('editor','Description','required|trim');
		$this->form_validation->set_rules('aboutus_page_id','Aboutus Page','required');
		if($this->input->post('eid')=='')
		{	
			/* if(empty($_FILES['image']['name'])){
				$this->form_validation->set_rules('image','Image','required');
			} */ 
			if($this->form_validation->run()==true){
				
				if ($_FILES['image']['name'] != '') 
				{   
					$config['upload_path'] = './assets/uploads/about_us';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$save['image']	= $data['file_name'];
				}
				$save['title']			= $this->input->post('aboutus_title');
				$save['page_name_id']	= $this->input->post('aboutus_page_id');
				$save['description']	= $this->input->post('editor');
				$save['date_created']	= date('Y-m-d');
				$result=$this->About_us_Model->save_aboutus_details($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/About_us');
			}
			else{
				$this->load->view('admin/about_us');
			}			
		}
		else
		{
			if($this->form_validation->run()==true){
				if ($_FILES['image']['name'] !='') {   
					$config['upload_path'] = './assets/uploads/about_us';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$update['image'] = $data['file_name'];
				}
				$update['title']			=	$this->input->post('aboutus_title');
				$update['page_name_id']		=   $this->input->post('aboutus_page_id');
				$update['description']		=	$this->input->post('editor');
				$update['date_modified']	=	date('Y-m-d');
			}
			$result=$this->About_us_Model->update_about_us($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/About_us');
		}
	}
	public function edit($id)
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->About_us_Model->get_one_row($id);
		$data['details']=$this->About_us_Model->get_aboutus_deatils();
		$this->load->view('admin/about_us',$data);
	}
	public function delete_record($id,$path='')
	{
		if($path !='')
		{
			$result=$this->About_us_Model->delete_row($id,$path);	
		}
		else{
			$result=$this->About_us_Model->delete_row($id);
		}
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/About_us');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/About_us');
	}
}

?>