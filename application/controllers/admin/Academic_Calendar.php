<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Academic_Calendar extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Values_List_Model');
		$this->load->model('admin/Academic_Calendar_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{	
		$data['details']=$this->Academic_Calendar_Model->calendar_information();
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/academic_calendar',$data);
	}
	public function addcontent()
	{
		// print_r($_POST); die;
		$this->form_validation->set_rules('department_id','Academic_Calendar Name','required');
		if($this->input->post('eid')=='')
		{	 
			if($this->form_validation->run()==true){
				$save['department_id']	=	$this->input->post('department_id');
				$save['commencement_end_of_term']= $this->input->post('Commencement');
				$save['first_term']		=	$this->input->post('term_first');
				$save['second_term']	=	$this->input->post('term_second');
				$save['vacation']		=	$this->input->post('vacation');
				$save['date_created']	=	date('Y-m-d');
				$result=$this->Academic_Calendar_Model->save_calendar_details($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Academic_Calendar');
			}
			else{
				$this->load->view('admin/academic_calendar');
			}					
		}
		else
		{
			if($this->form_validation->run()==true){
				$update['department_id']	=	$this->input->post('department_id');
				$update['commencement_end_of_term']= $this->input->post('Commencement');
				$update['first_term']		=	$this->input->post('term_first');
				$update['second_term']		=	$this->input->post('term_second');
				$update['vacation']			=	$this->input->post('vacation');
				$update['date_modified']	=	date('Y-m-d');
			}
			$result=$this->Academic_Calendar_Model->update_calendar($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Academic_Calendar');
		}
	}
	public function edit($id)
	{
		$data['details']	=	$this->Academic_Calendar_Model->calendar_information();
		$data['values']		=	$this->Values_List_Model->get_all_values();
		$data['edit']		=	$this->Academic_Calendar_Model->get_one_row($id);
		// echo'<pre>';print_r($data['single_row']);
		$this->load->view('admin/academic_calendar',$data);
	}
	public function delete_record($id,$path='')
	{
		$result=$this->Academic_Calendar_Model->delete_row($id);	
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Academic_Calendar');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Academic_Calendar');
	}
}
 ?>