<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Add_menu extends CI_Controller
{
	public function  __construct()
	{
		parent::__construct();
		$this->load->model('admin/Add_menu_model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['all_menu']=$this->Add_menu_model->get_all_menu();
		$this->load->view('admin/add_menu',$data);
	}
	public function addmenu()
	{
		if($this->input->post('eid')=='')
		{
			$data['title']=$this->input->post('eng_title');
			$data['parent_id']=$this->input->post('parent');
			$data['location']=$this->input->post('location');
			$data['position']=$this->input->post('position');
			$menu_link=ucfirst(preg_replace('![^a-z0-9]+!i','_',$this->input->post('eng_title')));
			$data['menu_link']=$menu_link;
			$result=$this->Add_menu_model->addmenu($data);
			$this->session->set_flashdata('submit','menu inserted successfully!!');
		}
		else
		{
			$mdata['title']=$this->input->post('eng_title');
			$mdata['parent_id']=$this->input->post('parent');
			$mdata['location']=$this->input->post('location');
			$mdata['position']=$this->input->post('position');
			$mmenu_link=strtolower(preg_replace('![^a-z0-9]+!i','_',$this->input->post('eng_title')));
			$mdata['menu_link']=$menu_link;
			$result=$this->Add_menu_model->update_menu($mdata,$_POST['eid']);
			$this->session->set_flashdata('update','menu updated successfully');
		}
		if($result)
		{
			redirect('admin/Add_menu');
		}
	}
	public function edit($id)
	{
		$data['menu_list']=$this->Add_menu_model->get_menu_row($id);
		$data['all_menu']=$this->Add_menu_model->get_all_menu();
		$this->load->view('admin/add_menu',$data);
	}
	public function set_status($id,$status)
	{
		// echo $id.'<br>';
		// echo $status; die;
		$mdata['status'] = $status;
		$res=$this->Add_menu_model->update_status($mdata,$id);
		if($res)
		{
			$this->session->set_flashdata('setstatus', 'Status updated sucessfully..!!');
			redirect('admin/Add_menu');
		}
	}
	public function delete_menu($id)
	{
		$result=$this->Add_menu_model->delete_single_menu($id);
		if($result)
		{
			redirect('admin/Add_menu');
			$this->session->set_flashdata('delete','menu deleted successfully');
		}
		
	}
}

?>