<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admission_Process extends CI_controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Admission_Process_Model');
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['details']=$this->Admission_Process_Model->get_admission();
		// print_r($data['details']); die;
		$this->load->view('admin/admission_process',$data);
	}
	public function addcontent(){		
		if($this->input->post('eid')=='')
		{	
			if ($_FILES['image']['name'] != '') 
			{   
				$config['upload_path'] = './assets/uploads/admission';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|PDF';
				$config['max_size']  = '53654270';
				$config['max_width']  = '0';
				$config['max_height']  = '0';
				$config['overwrite'] = FALSE; 
				$config['encrypt_name'] = TRUE; 
				$config['remove_spaces'] = TRUE; 
				$config['file_name'] = $_FILES['image']['name'];
				$this->load->library('upload', $config);
				$upload 		= $this->upload->do_upload('image');
				$data 			= $this->upload->data();
				$save['document']	= $data['file_name'];
			}
			$save['title']		 = $this->input->post('aboutus_title');
			$save['description'] = $this->input->post('editor');
			$save['date_created']= date('Y-m-d');
			$result=$this->Admission_Process_Model->save_admission_steps($save);
			$this->session->set_flashdata('insert','Record inserted successfully');
			redirect('admin/Admission_Process');
						
		}
		else{
			if ($_FILES['image']['name'] !='') {   
				$config['upload_path'] = './assets/uploads/admission';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|PDF';
				$config['max_size']  = '53654270';
				$config['max_width']  = '0';
				$config['max_height']  = '0';
				$config['overwrite'] = FALSE; 
				$config['encrypt_name'] = TRUE; 
				$config['remove_spaces'] = TRUE; 
				$config['file_name'] = $_FILES['image']['name'];
				$this->load->library('upload', $config);
				$upload 		= $this->upload->do_upload('image');
				$data 			= $this->upload->data();
				$update['document'] = $data['file_name'];
			}
			$update['title']			=	$this->input->post('title');
			$update['description']		=	$this->input->post('editor');
			$update['date_modified']	=	date('Y-m-d');
			$result=$this->Admission_Process_Model->update_admission_steps($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Admission_Process');
		}
	}
	public function edit($id)
	{
		$data['edit']=$this->Admission_Process_Model->get_one_row($id);
		$data['details']=$this->Admission_Process_Model->get_admission();
		$this->load->view('admin/admission_process',$data);
	}
	public function delete_record($id)
	{
		$result=$this->Admission_Process_Model->delete_row($id,$path);	
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Admission_Process');
		}
	}
	public function cancel_update()
	{
		redirect('admin/Admission_Process');
	}
}

?>