<?php
class Banner_Master extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->method_call=& get_instance();
		$this->load->model('admin/Values_List_Model');
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
		$this->load->model('admin/Banner_Master_Model');
	}
	public function index($i=0)
	{
		// echo'<pre>'; print_r($datas['locaton']); die;
		$data['banners']=$this->Banner_Master_Model->get_banners();
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/banner_master',$data);
	}
	public function addbanners()
	{
		$this->form_validation->set_rules('page_id','Pages','required');
		if($_POST['edit_id']==""){
			$data=array();
			if($this->form_validation->run()==true){
				if(count($_FILES['banners_img']['name'])>0){
					$count=count($_FILES['banners_img']['name']);
					for($i=0;$i<$count;$i++){
						$_FILES['userfile']['name']		= $_FILES['banners_img']['name'][$i];
						$_FILES['userfile']['type']		= $_FILES['banners_img']['type'][$i];
						$_FILES['userfile']['tmp_name']	= $_FILES['banners_img']['tmp_name'][$i];
						$_FILES['userfile']['error']	= $_FILES['banners_img']['error'][$i];
						$_FILES['userfile']['size']		= $_FILES['banners_img']['size'][$i];
						
						$config['upload_path'] 			= 'assets/uploads/banners';
						$config['allowed_types'] 		= 'gif|jpg|png|bmp|jpeg';
						$config['max_size']  			= '100000';
						$config['max_size'] 			= '100000';
						$config['max_width']  			= '';
						$config['max_height']  			= '';
						$config['overwrite'] 			= FALSE; 
						$config['encrypt_name'] 		= TRUE; 
						$config['remove_spaces'] 		= TRUE;
						$this->load->library('upload', $config);
						$upload 						= $this->upload->do_upload('userfile');
						$datafile 						= $this->upload->data();
						$name['banner_image'][$i] 		= $datafile['file_name'];
					}
				}
				$banners	= implode(',',$name['banner_image']);
				$data['image']	= $banners;
				$data['page_id']		= $this->input->post('page_id');
				$data['date_created']	= date('Y-m-d');
				echo'<pre>';print_r($data);
				$res=$this->Banner_Master_Model->save_banners($data);
				if($res){
					redirect('admin/Banner_Master');
				}
			}
			else{
				$this->load->view('admin/banner_master');
			}
		}
		else{
			$updatedata=array();
			if($this->form_validation->run()==true){
				
				if(count($_FILES['banners_img']['name'])>1){					
					$count=count($_FILES['banners_img']['name']);
					for($i=0;$i<$count;$i++){
						
						$_FILES['userfile']['name']		= $_FILES['banners_img']['name'][$i];
						$_FILES['userfile']['type']		= $_FILES['banners_img']['type'][$i];
						$_FILES['userfile']['tmp_name']	= $_FILES['banners_img']['tmp_name'][$i];
						$_FILES['userfile']['error']	= $_FILES['banners_img']['error'][$i];
						$_FILES['userfile']['size']		= $_FILES['banners_img']['size'][$i];
						
						$config['upload_path'] 			= 'assets/uploads/banners';
						$config['allowed_types'] 		= 'gif|jpg|png|bmp|jpeg';
						$config['max_size']  			= '100000';
						$config['max_size'] 			= '100000';
						$config['max_width']  			= '';
						$config['max_height']  			= '';
						$config['overwrite'] 			= FALSE; 
						$config['encrypt_name'] 		= TRUE; 
						$config['remove_spaces'] 		= TRUE;
					
						$this->load->library('upload', $config);
						$upload = $this->upload->do_upload('userfile');
						$datafile = $this->upload->data();
						$updatedata['banner_image'][$i] = $datafile['file_name'];
					}
					$updateimgs = implode(',',$updatedata['banner_image']);
					$updatedata['image']	= $updateimgs;
				}				
				$updatedata['page_id']	= $this->input->post('page_id');
				$updatedata['date_modified'] = date('Y-m-d');
				// echo'<pre>';print_r($updatedata); die;
				$res=$this->Banner_Master_Model->edit_banners($updatedata,$_POST['edit_id']);
				if($res){
					$this->session->set_flashdata('success','Record updated successfully');
					redirect('admin/Banner_Master');
				}
			}
			else{
				$this->load->view('admin/banner_master');
			}
		}
	}
	public function edit($id)
	{
		$data['banners']=$this->Banner_Master_Model->get_banners();
		$data['pageid']=$this->Banner_Master_Model->get_single_banner();
		$this->load->view('admin/banner_master');	
	}
	public function delete_banners($id)
	{
		$path=$this->Banner_Master_Model->get_img_path($id);
		unlink('assets/uploads/banners/'.$path->image);
		$res=$this->Banner_Master_Model->deletebaneers($id);
		if($res){
			redirect('admin/Banner_Master');
		}
	}
}

?>