<?php
class Calendar_Headings extends CI_controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Calendar_Headings_Model');
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['details']=$this->Calendar_Headings_Model->calendar_headings();
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/calendar_headings',$data);
	}
	public function addcontent()
	{
		// print_r($_POST); die;
		$this->form_validation->set_rules('department_id','Department Name','required');
		if($this->input->post('eid')=='')
		{	 
			if($this->form_validation->run()==true){
				$save['department_id']	=	$this->input->post('department_id');
				$save['session']		= 	$this->input->post('calendar_title');
				$save['heading_first']	=	$this->input->post('heading_first');
				$save['heading_second']	=	$this->input->post('heading_second');
				$save['heading_third']	=	$this->input->post('heading_third');
				$save['date_created']	=	date('Y-m-d');
				$result=$this->Calendar_Headings_Model->save_calendar_heading($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Calendar_Headings');
			}
			else{
				$this->load->view('admin/calendar_headings');
			}					
		}
		else
		{
			if($this->form_validation->run()==true){
				$update['department_id']	=	$this->input->post('department_id');
				$update['session']			= 	$this->input->post('calendar_title');
				$update['heading_first']	=	$this->input->post('heading_first');
				$update['heading_second']	=	$this->input->post('heading_second');
				$update['heading_third']	=	$this->input->post('heading_third');
				$update['date_modified']	=	date('Y-m-d');
			}
			$result=$this->Calendar_Headings_Model->update_calendar_heading($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Calendar_Headings');
		}
	}
	public function edit($id)
	{
		$data['details']=$this->Calendar_Headings_Model->calendar_headings();
		$data['values']		=	$this->Values_List_Model->get_all_values();
		$data['edit']		=	$this->Calendar_Headings_Model->get_one_row($id);
		// echo'<pre>';print_r($data['single_row']);
		$this->load->view('admin/calendar_headings',$data);
	}
	public function delete_record($id)
	{
		$result=$this->Calendar_Headings_Model->delete_row($id);	
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Calendar_Headings');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Calendar_Headings');
	}
}
?>