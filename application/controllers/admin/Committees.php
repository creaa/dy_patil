<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Committees extends CI_controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/Committees_Model');
		$this->method_call=& get_instance();
		error_reporting(1);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['committees']=$this->Committees_Model->Getcommittees_name();
		$this->load->view('admin/committees',$data);
	}
	public function addcontent()
	{		
		if($_POST['title']=="" && $_POST['editor']==""){
			$this->form_validation->set_rules('committee_name','Committee Name','required');
			$this->form_validation->set_rules('no_of_members','No.of Members','required');	
		}
		else{
			$this->form_validation->set_rules('title','Title','required|trim');
			$this->form_validation->set_rules('editor','Description','required|trim');
		}
		if($this->input->post('eid')=='')
		{	
			if($this->form_validation->run()==true){
				$save['title']			= $this->input->post('title');
				$save['description']	= $this->input->post('editor');
				$save['no_of_members']	= $this->input->post('no_of_members');
				$save['committee_name']	= $this->input->post('committee_name');
				$save['date_created']	= date('Y-m-d');
				$result=$this->Committees_Model->save_committes($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/committees');
			}
			else{
				$this->load->view('admin/committees');
			}			
		}
		else
		{
			if($this->form_validation->run()==true){
				$update['title']			= $this->input->post('title');
				$update['description']		= $this->input->post('editor');
				$update['no_of_members']	= $this->input->post('no_of_members');
				$update['committee_name']	= $this->input->post('committee_name');
				$update['date_modified']	=	date('Y-m-d');
			}
			$result=$this->Committees_Model->update_committes($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Committees');
		}
	}
	public function edit($id)
	{
		$data['edit']=$this->Committees_Model->get_one_row($id);
		$data['committees']=$this->Committees_Model->Getcommittees_name();
		$this->load->view('admin/committees',$data);
	}
	public function delete_record($id)
	{
		$result=$this->Committees_Model->delete_row($id);
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Committees');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Committees');
	}
}
?>