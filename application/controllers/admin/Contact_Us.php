<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact_Us extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Contact_Us_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['contacts']=$this->Contact_Us_Model->get_contact_details();
		$this->load->view('admin/contact_us',$data);
	}
	public function addcontent()
	{
		if($this->input->post('eid')=='')
		{
			
			if ($_FILES['IMAGE']['name'] != '') 
			{   
				$config['upload_path'] = './assets/uploads';
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg|mp4|mov|avi|flv|wmv';
				$config['max_size']  = '5000';
				$config['max_width']  = '0';
				$config['max_height']  = '0';
				$config['overwrite'] = FALSE; 
				$config['encrypt_name'] = TRUE; 
				$config['remove_spaces'] = TRUE; 
				$config['file_name'] = $_FILES['IMAGE']['name'];
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('IMAGE');
				$data = $this->upload->data();
				$idata['kb_contact_banner'] = $data['file_name'];
			}				
			$idata['title']=$this->input->post('TITLE');
			$idata['website_link']=$this->input->post('Website_link');
			$idata['email']=$this->input->post('EMAIL_ID');
			$idata['phone']=$this->input->post('PHONE_NO');
			$idata['address']=$this->input->post('editor1');
			$idata['map']=$this->input->post('map');
			$result=$this->Contact_Us_Model->add_contacts_details($idata);
			$this->session->set_flashdata('submit','contact details added successfully');
		}
		else{
			if ($_FILES['IMAGE']['name'] != '') 
			{   
				$config['upload_path'] = './assets/uploads';
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg|mp4|mov|avi|flv|wmv';
				$config['max_size']  = '5000';
				$config['max_width']  = '0';
				$config['max_height']  = '0';
				$config['overwrite'] = FALSE; 
				$config['encrypt_name'] = TRUE; 
				$config['remove_spaces'] = TRUE; 
				$config['file_name'] = $_FILES['IMAGE']['name'];
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('IMAGE');
				$data = $this->upload->data();
				$udata['kb_contact_banner'] = $data['file_name'];
			}				
			$udata['title']=$this->input->post('TITLE');
			$udata['website_link']=$this->input->post('Website_link');
			$udata['email']=$this->input->post('EMAIL_ID');
			$udata['phone']=$this->input->post('PHONE_NO');
			$udata['address']=$this->input->post('editor1');
			$udata['map']=$this->input->post('map');
			$result=$this->Contact_Us_Model->update_contact_details($udata,$_POST['eid']);
			$this->session->set_flashdata('update','contact details updated successfully');
		}
		if($result)
		{
			redirect('admin/Contact_Us');
		}
	}
	public function cancel_update()
	{
		redirect('admin/Contact_Us');
		$this->session->set_flashdata('cancel','you have canceled update');
	}
	public function edit($id)
	{
		$data['contacts']=$this->Contact_Us_Model->get_contact_details();
		$data['single_row']=$this->Contact_Us_Model->get_single_row($id);
		$this->load->view('admin/Contact_Us',$data);
	} 
	public function delete_data($id,$path='')
	{
		if($path=''){
			$result=$this->Contact_Us_Model->delete_row($id);
		}
		else{
			$result=$this->Contact_Us_Model->delete_row($id,$path);
		}
		if($result)
		{
			$this->session->set_flashdata('delete','contact details deleted successfully');
			redirect('admin/Contact_Us');
		}
	}
}
 ?>