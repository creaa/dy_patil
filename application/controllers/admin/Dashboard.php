<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
class Dashboard extends CI_Controller
{
 public function __construct()
 {
	parent::__construct() ;
	$this->load->helper('master_helper');
	if($this->session->userdata('admin')==FALSE)
			{
				redirect('admin/login');
			}
	
 }	
 
 public function index()
 {
  $this->load->view('admin/dashboard');	 
 }

 function feedbacks(){
 	$this->load->view('admin/feedback_list');
 }

 function subscriber_list(){

 	$this->load->view('admin/subscribe_list');
 }
 
 
}
?>