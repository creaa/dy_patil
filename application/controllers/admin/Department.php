<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Department extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Values_List_Model');
		$this->load->model('admin/Department_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{	
		$data['details']=$this->Department_Model->department_information();
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/department',$data);
	}
	public function addcontent()
	{
		// print_r($_POST); die;
		$this->form_validation->set_rules('department_id','Department Name','required');
		if($this->input->post('eid')=='')
		{	
			/* if(empty($_FILES['image']['name'])){
				$this->form_validation->set_rules('image','Image','required');
			} */ 
			if($this->form_validation->run()==true){
				
				if ($_FILES['image']['name'] != '') 
				{   
					$config['upload_path'] = './assets/uploads/school';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$save['image']	= $data['file_name'];
				}
				$save['title']=$this->input->post('title');
				$save['department_id']=$this->input->post('department_id');
				$save['description']=$this->input->post('editor');
				$save['date_created']=date('Y-m-d');
				$result=$this->Department_Model->save_department_details($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Department');
			}else{
				$this->load->view('admin/department');
			}					
		}
		else
		{
			if($this->form_validation->run()==true){
				if ($_FILES['image']['name'] !='') {   
					$config['upload_path'] = './assets/uploads/school';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload = $this->upload->do_upload('image');
					$data = $this->upload->data();
					$update['image'] = $data['file_name'];
				}
				$update['title']			=	$this->input->post('title');
				$update['department_id']	=	$this->input->post('department_id');
				$update['description']		=	$this->input->post('editor');
				$update['date_created']		=	date('Y-m-d');
				$update['date_created']		=	date('Y-m-d');
			}
			$result=$this->Department_Model->update_department($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Department');
		}
	}
	public function edit($id)
	{
		$data['details']=$this->Department_Model->department_information();
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->Department_Model->get_one_row($id);
		// echo'<pre>';print_r($data['edit']);
		$this->load->view('admin/department',$data);
	}
	public function delete_record($id,$path='')
	{
		if($path !='')
		{
			$result=$this->Department_Model->delete_row($id,$path);	
		}
		else{
			$result=$this->Department_Model->delete_row($id);
		}
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Department');
		}
	}
	public function set_status($id,$status)
	{
		$mdata['status'] = $status;
		$res=$this->Department_Model->change_status($id,$mdata);
		if($res)
		{
			$this->session->set_flashdata('setstatus', 'Status updated sucessfully..!!');
			redirect('admin/Department');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Department');
	}
}
 ?>