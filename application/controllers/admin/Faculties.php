<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Faculties extends CI_controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Faculties_Model');
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['details']=$this->Faculties_Model->get_faculties_deatils();
		$this->load->view('admin/faculties',$data);
	}
	public function addcontent()
	{		
		$this->form_validation->set_rules('dpt_id','Department','required');
		$this->form_validation->set_rules('faculty_name','Faculty Name','required|trim');
		$this->form_validation->set_rules('contact_no','Contact No','required|trim');
		$this->form_validation->set_rules('email_id','Email Id','required|trim');
		$this->form_validation->set_rules('specialization','Specialization','required|trim');
		if($this->input->post('eid')=='')
		{	 
			if($this->form_validation->run()==true){
				$save['dpt_id']		 	= $this->input->post('dpt_id');
				$save['faculty_name']	= $this->input->post('faculty_name');
				$save['contact_no']	 	= $this->input->post('contact_no');
				$save['email_id']		= $this->input->post('email_id');
				$save['specialization']	= $this->input->post('specialization');
				$save['date_created']	= date('Y-m-d');
				$result=$this->Faculties_Model->save_faculty($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Faculties');
			}
			else{
				$this->load->view('admin/faculties');
			}			
		}
		else
		{
			if($this->form_validation->run()==true){
				$update['dpt_id']		= $this->input->post('dpt_id');
				$update['faculty_name']	= $this->input->post('faculty_name');
				$update['contact_no']	= $this->input->post('contact_no');
				$update['email_id']		= $this->input->post('email_id');
				$update['specialization']= $this->input->post('specialization');
				$update['date_modified'] = date('Y-m-d');
			}
			$result=$this->Faculties_Model->update_faculty($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Faculties');
		}
	}
	public function edit($id)
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->Faculties_Model->get_one_row($id);
		$data['details']=$this->Faculties_Model->get_faculties_deatils();
		$this->load->view('admin/faculties',$data);
	}
	public function delete_record($id,$path='')
	{
		$result=$this->Faculties_Model->delete_row($id,$path);	
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Faculties');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Faculties');
	}
}

?>