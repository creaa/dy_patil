<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Values_List_Model');
		$this->load->model('admin/Home_model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{	
		$data['details']=$this->Home_model->home_page_details();
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/home',$data);
	}
	public function addcontent()
	{
		// print_r($_POST); die;
		$this->form_validation->set_rules('home_page_section','Page Section','required');
		if($this->input->post('eid')=='')
		{	
			if(empty($_FILES['image']['name'])){
				$this->form_validation->set_rules('image','Image','required');
			} 
			if($this->form_validation->run()==true){
				
				if ($_FILES['image']['name'] != '') 
				{   
					$config['upload_path'] = './assets/uploads/home';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$save['image']	= $data['file_name'];
				}
				$save['title']=$this->input->post('home_page_title');
				$save['page_section_id']=$this->input->post('home_page_section');
				$save['vew_detail_link']=$this->input->post('view_detail_link');
				$save['dsecription']=$this->input->post('editor');
				$save['date_created']=date('Y-m-d');
				$result=$this->Home_model->save_home_page_details($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Home');
			}					
		}
		else
		{
			if($this->form_validation->run()==true){
				if ($_FILES['image']['name'] !='') {   
					$config['upload_path'] = './assets/uploads/home';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload = $this->upload->do_upload('image');
					$data = $this->upload->data();
					$update['image'] = $data['file_name'];
				}
				$update['title']			=	$this->input->post('home_page_title');
				$update['page_section_id']	=	$this->input->post('home_page_section');
				$update['vew_detail_link']	=	$this->input->post('view_detail_link');
				$update['dsecription']		=	$this->input->post('editor');
				$update['date_created']		=	date('Y-m-d');
			}
			$result=$this->Home_model->update_home_page($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Home');
		}
	}
	public function edit($id)
	{
		$data['details']=$this->Home_model->home_page_details();
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->Home_model->get_one_row($id);
		// echo'<pre>';print_r($data['single_row']);
		$this->load->view('admin/home',$data);
	}
	public function delete_record($id,$path='')
	{
		if($path !='')
		{
			$result=$this->Home_model->delete_row($id,$path);	
		}
		else{
			$result=$this->Home_model->delete_row($id);
		}
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Home');
		}
	}
	public function set_status($id,$status)
	{
		$mdata['status'] = $status;
		$res=$this->Home_model->change_status($id,$mdata);
		if($res)
		{
			$this->session->set_flashdata('setstatus', 'Status updated sucessfully..!!');
			redirect('admin/Home');
		}
	}
	public function cancel_update()
	{
		// $this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Home');
	}
}
 ?>