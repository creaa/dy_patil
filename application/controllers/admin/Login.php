<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$this->load->model('admin/Login_model');
	}	
	public function index()
	{
		$this->load->view('admin/login');	 
	}
	public function admin_login()
	{
		$username=$this->input->post("username");
		$password=$this->input->post("password");
		$res=$this->Login_model->login($username,$password);
		if($res==TRUE)
		{
			redirect('admin/dashboard');	
		}
		else
		{      
			$this->session->set_flashdata('loginerror',"Wrong Username And Password Please Try Again");
			redirect('admin/login');            	
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('adminid');
		redirect('admin/login');
	}
}
?>