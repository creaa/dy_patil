<?php
class Placements_And_Internships extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('admin/Placements_And_Internships_Model');
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['details']=$this->Placements_And_Internships_Model->get_placements_and_internships();
		$this->load->view('admin/placements_and_internships',$data);
	}
	public function addcontent()
	{		
		$this->form_validation->set_rules('page_id','Facility Page','required');
		if($this->input->post('eid')=='')
		{	
			if($this->form_validation->run()==true){
				
				if ($_FILES['image']['name'] != '') 
				{   
					$config['upload_path'] = './assets/uploads/placement_interns';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$save['image']	= $data['file_name'];
				}
				$save['title']		 = $this->input->post('title');
				$save['page_section']= $this->input->post('page_section');
				$save['page_id']	 = $this->input->post('page_id');
				$save['description'] = $this->input->post('editor');
				$save['date_created']= date('Y-m-d');
				$result=$this->Placements_And_Internships_Model->save_placements_and_internships($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Placements_And_Internships');
			}
			else{
				$data['values']=$this->Values_List_Model->get_all_values();
				$this->load->view('admin/placements_and_internships');
			}			
		}
		else
		{
			if($this->form_validation->run()==true){
				if ($_FILES['image']['name'] !='') {   
					$config['upload_path'] = './assets/uploads/placement_interns';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size']  = '53654270';
					$config['max_width']  = '0';
					$config['max_height']  = '0';
					$config['overwrite'] = FALSE; 
					$config['encrypt_name'] = TRUE; 
					$config['remove_spaces'] = TRUE; 
					$config['file_name'] = $_FILES['image']['name'];
					$this->load->library('upload', $config);
					$upload 		= $this->upload->do_upload('image');
					$data 			= $this->upload->data();
					$update['image']= $data['file_name'];
				}
				$update['title']		= $this->input->post('title');
				$update['page_section']	= $this->input->post('page_section');
				$update['page_id']	 	= $this->input->post('page_id');
				$update['description'] 	= $this->input->post('editor');
				$update['date_modified']= date('Y-m-d');
			}
			$result=$this->Placements_And_Internships_Model->update_placements_and_internships($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");				
		}
		if($result)
		{
			redirect('admin/Placements_And_Internships');
		}
	}
	public function edit($id)
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->Placements_And_Internships_Model->get_one_row($id);
		$data['details']=$this->Placements_And_Internships_Model->get_placements_and_internships();
		$this->load->view('admin/Placements_And_Internships',$data);
	}
	public function delete_record($id,$path='')
	{
		if($path !='')
		{
			$result=$this->Placements_And_Internships_Model->delete_row($id,$path);	
		}
		else{
			$result=$this->Placements_And_Internships_Model->delete_row($id);
		}
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Placements_And_Internships');
		}
	}
	public function cancel_update()
	{
		$this->session->set_flashdata('cancel',"update cancelled successfully");
		redirect('admin/Placements_And_Internships');
	}
}
?>