<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
class Profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct() ;
		$this->load->model('admin/Register_model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}	
	public function index()
	{	
		$adata['user']=$this->Register_model->get_profile_details($this->session->userdata('adminid'));
		$this->load->view('admin/update_profile',$adata);	 
	}
	public function update_pro()
	{
		$config['upload_path'] = './assets/upload/profile/';
		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
		$config['max_size']  = '2048';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['overwrite'] = FALSE; 
		$config['encrypt_name'] = TRUE; 
		$config['remove_spaces'] = TRUE; 
		$config['file_name'] = ''.time();
		$this->load->library('upload',$config);
		$upload = $this->upload->do_upload('image');	 
		$data = $this->upload->data();		$upload1 = $this->upload->do_upload('benner_image');	  		$data1 = $this->upload->data();		$res=$this->Register_model->get_profile_details($this->session->userdata('adminid'));		if($res['image']!='')
		{
			$imagename=$res['image'];
			$imagename1=$res['benner_image'];
			$path="assets/upload/profile/".$imagename;	
			$path="assets/upload/profile/".$imagename1;
			unlink($path); 
		}
			$udata['fname'] =$this->input->post('fname'); 
			$udata['lname'] =$this->input->post('lname'); 
			$udata['email'] =$this->input->post('email'); 
			$udata['contact_no'] =$this->input->post('mobile');  
			$udata['address'] =$this->input->post('address'); 
			$udata['country'] =$this->input->post('country'); 
			$udata['state'] =$this->input->post('state'); 
			$udata['city'] =$this->input->post('city'); 
			$udata['country'] =$this->input->post('country'); 
			$udata['zip'] =$this->input->post('zip');
			$udata['description'] =$this->input->post('discription');
			if($data['file_name']!='')
			{
				$udata['profile_pic'] = $data['file_name'];
			}
			if($data1['file_name']!='')	
			{				$udata['banner_pic'] = $data1['file_name'];
			}
			$data['uprofile']=$this->Register_model->update_profile($this->session->userdata('adminid'),$udata);
			$this->session->set_flashdata('proupdate','Your Profile has been updated successfully! ');	
			redirect('admin/profile');
	}
	public function addsocial()
	{
		$social['social_link']=$this->Register_model->get_all_social($this->session->userdata('adminid'));
		$this->load->view('admin/addsocial_link',$social);
	}
	public function add_social_link()
	{
		$res=$this->Register_model->check_user_id($this->session->userdata('adminid'));	
		if(count($res)==0)
		{
			$udata['user_id']=$this->session->userdata('adminid');
			$udata['facebook']=$this->input->post('facebook');
			$udata['twitter']=$this->input->post('twitter');
			$udata['google']=$this->input->post('google');
			$udata['youtube']=$this->input->post('youtube');
			$udata['linkin']=$this->input->post('linkin');
			$udata['pinterest']=$this->input->post('pinterest');
			$udata['steam']=$this->input->post('steam');
			$udata['instagram']=$this->input->post('instagram');
			$udata['vimeo']=$this->input->post('vimeo');
			$this->Register_model->insert_social_link($udata);
			$this->session->set_flashdata('submit','Insert Social Link Successfully');
			redirect('admin/profile/addsocial');	
		}
		else
		{			$udata['facebook']=$this->input->post('facebook');
			$udata['twitter']=$this->input->post('twitter');
			$udata['google']=$this->input->post('google');
			$udata['youtube']=$this->input->post('youtube');
			$udata['linkin']=$this->input->post('linkin');
			$udata['pinterest']=$this->input->post('pinterest');
			$udata['steam']=$this->input->post('steam');
			$udata['instagram']=$this->input->post('instagram');
			$udata['vimeo']=$this->input->post('vimeo');
			$this->Register_model->update_social_link($this->session->userdata('adminid'),$udata);
			$this->session->set_flashdata('submit','Update Social Link Successfully');
			redirect('admin/profile/addsocial');		}	}
}