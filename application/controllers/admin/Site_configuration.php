<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<?php 
class Site_configuration extends CI_controller
{
public function __construct()
{
parent::__construct() ;
$this->load->model('admin/Site_configuration_model');

}	

public function index()
{
$res= $this->db->get('site_cofig')->row_array();
if(isset($res))
{
$data['site_details']=$this->Site_configuration_model->get_site_configuration($res['id']);
$this->load->view('admin/siteconfigration',$data);
}
else{
 $this->load->view('admin/siteconfigration');
}
}

public function add_site_configuration()
{

if($this->input->post('eid')=='')
{
 if ($_FILES['logo_img']['name'] != '') {
                $config['upload_path'] = './assets/uploads/logo/';
                $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
                $config['max_size'] = '2048';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $this->load->library('upload', $config);
                $upload = $this->upload->do_upload('logo_img');
                $data = $this->upload->data();
                $sitedata['logo'] = $data['file_name'];
            }
            if ($_FILES['favicon_img']['name'] != '') {
                $config1['upload_path'] = './assets/uploads/logo/';
                $config1['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
                $config1['max_size'] = '2048';
                $config1['max_width'] = '0';
                $config1['max_height'] = '0';
                $config1['overwrite'] = FALSE;
                $config1['encrypt_name'] = TRUE;
                $config1['remove_spaces'] = TRUE;
                $this->load->library('upload', $config1);
                $upload = $this->upload->do_upload('favicon_img');
                $data = $this->upload->data();
                $sitedata['favicon'] = $data['file_name'];
            }


$sitedata['email']=$this->input->post('email');
$sitedata['contact_no']=$this->input->post('contact');
$sitedata['address']=$this->input->post('address');
$sitedata['copyright']=$this->input->post('copy');
$sitedata['facebook']=$this->input->post('facebook');
$sitedata['twitter']=$this->input->post('twitter');
$sitedata['google']=$this->input->post('google');
$sitedata['linkedin']=$this->input->post('linkedin');
$sitedata['youtube']=$this->input->post('youtube');
$sitedata['instragram']=$this->input->post('instragram');
$sitedata['site_title']=$this->input->post('title');
$sitedata['keyword']=$this->input->post('keyword');
$sitedata['description']=$this->input->post('description');
$sitedata['latitude']=$this->input->post('lati');
$sitedata['longitude']=$this->input->post('long');
$sitedata['footer_title']=$this->input->post('footer_title');
$sitedata['footer_description']=$this->input->post('footer_description');



$res=$this->Site_configuration_model->add_config($sitedata);
}
else {
	
if($_FILES['logo_img']['name']!='')
{ 

$img=$this->Site_configuration_model->get_site_configuration($this->input->post('eid'));
		 
			 if($img['logo']!='')
			 {
				unlink('./assets/uploads/logo/'.$img['logo']); 
			 }
	  $config['upload_path'] = './assets/upload/logo/';
      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      $config['max_size']  = '2048';
      $config['max_width']  = '0';
      $config['max_height']  = '0';
      $config['overwrite'] = FALSE; 
      $config['encrypt_name'] = TRUE; 
      $config['remove_spaces'] = TRUE; 
      $this->load->library('upload', $config);	  
      $upload = $this->upload->do_upload('logo_img');
	  $data = $this->upload->data();
	  $mdata['logo'] = $data['file_name'];
	
		}
		if($_FILES['favicon_img']['name']!='')
		{ 
		 $img=$this->Site_configuration_model->get_site_configuration($this->input->post('eid'));
			 if($img['favicon']!='')
			 {
				unlink('./assets/uploads/logo/'.$img['favicon']); 
			 }
		  $config['upload_path'] = './assets/upload/logo/';
      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      $config['max_size']  = '2048';
      $config['max_width']  = '0';
      $config['max_height']  = '0';
      $config['overwrite'] = FALSE; 
      $config['encrypt_name'] = TRUE; 
      $config['remove_spaces'] = TRUE; 
      $this->load->library('upload', $config);	  
      $upload = $this->upload->do_upload('favicon_img');
	  $data = $this->upload->data();
		  $mdata['favicon'] = $data['file_name'];

		}
		
$mdata['email']=$this->input->post('email');
$mdata['contact_no']=$this->input->post('contact');
$mdata['address']=$this->input->post('address');
$mdata['copyright']=$this->input->post('copy');
$mdata['facebook']=$this->input->post('facebook');
$mdata['twitter']=$this->input->post('twitter');
$mdata['google']=$this->input->post('google');
$udata['linkedin']=$this->input->post('linkedin');
$mdata['youtube']=$this->input->post('youtube');
$mdata['instragram']=$this->input->post('instragram');
$mdata['site_title']=$this->input->post('title');
$mdata['keyword']=$this->input->post('keyword');
$mdata['description']=$this->input->post('description');
$mdata['latitude']=$this->input->post('lati');
$mdata['longitude']=$this->input->post('long');
$mdata['footer_title']=$this->input->post('footer_title');
$mdata['footer_description']=$this->input->post('footer_description');
print_r($mdata);

$res=$this->Site_configuration_model->update_config($_POST['eid'],$mdata);
redirect('admin/Site_configuration');
}

redirect('admin/Site_configuration');
 }


}
?>