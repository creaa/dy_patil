<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Values_List extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Values_List_Model');
		$this->method_call=& get_instance();
		error_reporting(0);
		if($this->session->userdata('admin')==FALSE)
		{
			redirect('admin/login');
		}
	}
	public function index()
	{	
		$data['values']=$this->Values_List_Model->get_all_values();
		$this->load->view('admin/list_of_values',$data);
	}
	public function addcontent()
	{
		// print_r($_POST); die;
		$this->form_validation->set_rules('value_name','Title','required');
		if($this->input->post('eid')=='')
		{	
			if($this->form_validation->run()==true){
				$save['values']			= $this->input->post('value_name');
				$save['date_created']	= date('Y-m-d');
				$result=$this->Values_List_Model->save_values($save);
				$this->session->set_flashdata('insert','Record inserted successfully');
				redirect('admin/Values_List');
			}					
		}
		else{
			if($this->form_validation->run()==true){
				$update['values']		=	$this->input->post('value_name');
				$update['date_created']	=	date('Y-m-d');
			}
			$result=$this->Values_List_Model->update_values($update,$_POST['eid']);
			$this->session->set_flashdata('update',"Record updated successfully");
		}
		if($result)
		{
			redirect('admin/Values_List');
		}
	}
	public function edit($id)
	{
		$data['values']=$this->Values_List_Model->get_all_values();
		$data['edit']=$this->Values_List_Model->get_one_row($id);
		// echo'<pre>';print_r($data['single_row']);
		$this->load->view('admin/list_of_values',$data);
	}
	public function delete_record($id)
	{
		$result=$this->Values_List_Model->delete_values($id,$path);	
		if($result)
		{
			$this->session->set_flashdata('delete',"data deleted successfully");
			redirect('admin/Values_List');
		}
	}
	public function cancel_update()
	{
		redirect('admin/Values_List');
	}
}
 ?>