<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	function get_sub_menu($id)
	{
		$CI =& get_instance();
		$CI->db->where('parent_id',$id);
		$query = $CI->db->get('category');
		return  $query->row_array();
	}
	function get_setting()
	{
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->from('site_cofig');
		$query=$CI->db->get();
		return $query->row_array();
	}
	function get_menu()
	{
		$CI =& get_instance();
		$CI->db->where(array('status'=>'active'));
		$query = $CI->db->get('menu');
		return $query->result_array();
	}
	function sub_menu()
	{
		$sbarray="status='active' && parent_id >0";
		$CI =& get_instance();
		$CI->db->where($sbarray);
		$query = $CI->db->get('menu');
		return $query->result_array();
	}
	function get_contact()
	{
		$CI =& get_instance();
		$CI->db->where('status','active');
		$query=$CI->db->get('tbl_contact');
		return $query->result();		
	}
	function Home_banner()
	{
		$CI =& get_instance();
		$CI->db->where(array('status'=>'active'));
		$query=$CI->db->get('tbl_home');
		return $query->result_array();
	}
	
?>