<?php defined('BASEPATH') OR exit('No direct script access allowed');

 class Welcome_model extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
	}
	public function school_information($where)
	{
		$qry=$this->db->where('department_id',$where)->get('school_department');
		return $qry->result_array();
	}
	public function academic_calendar($where)
	{
		$qry=$this->db->where('department_id',$where)->get('academic_calendar');
		return $qry->result_array();
	}
	public function calendar_header($where)
	{
		$qry=$this->db->get('academic_calendar_header');
		return $qry->result_array();
	}
	public function get_about_us_details($where)
	{
		return $this->db->where('page_name_id',$where)->get('about_us')->result_array();
	}
	public function get_committees()
	{
		return $this->db->get('committees')->result_array();
	}
	public function get_research($id)
	{
		return $this->db->where('page_id',$id)->get('research')->result_array();
	}
	public function campus_facilities($where)
	{
		return $this->db->where('page_id',$where)->get('facility')->result_array();
	}
	public function sports_and_cultural($where)
	{
		return $this->db->where('page_id',$where)->get('sports_cultural')->result_array();
	}
	public function placement_intern($id)
	{
		return $this->db->where('page_id',$id)->get('placements_and_internships')->result_array();
	}
	public function admission_process()
	{
		return $this->db->get('admission_process')->result_array();
	}
	public function banner_images($where)
	{
		return $this->db->where('page_id',$where)->get('banners')->result_array();
	}
	public function faculties_details()
	{
		return $this->db->get('faculty')->result_array();
	}
}





