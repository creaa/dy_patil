<?php 
class About_model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function insert_details($data)
	{ 
		return $this->db->insert('tbl_about',$data);	
	}
	public function get_details()
	{
		$query=$this->db->get('tbl_about');
		return $query->result();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('tbl_about',array('ID'=>$id));
		return $query->result_array();
	}
	public function update_data($data,$id)
	{
		$this->db->where('ID',$id);
		return $this->db->update('tbl_about',$data);
	}
	public function delete_row($id,$path='')
	{
		$this->db->where('ID',$id);
		if($path !=''){
			unlink('assets/uploads/'.$path);
		}	
		return $this->db->delete('tbl_about');
	}
	public function change_status($id,$data)
	{
		$this->db->where('ID',$id);
		return $this->db->update('tbl_about',$data);
	}
}
?>