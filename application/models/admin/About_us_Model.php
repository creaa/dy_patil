<?php 
class About_us_Model extends CI_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function save_aboutus_details($data)
	{
		return $this->db->insert('about_us',$data);
	}
	public function get_aboutus_deatils()
	{
		$qry=$this->db->get('about_us');
		return $qry->result_array();
	}
	public function get_one_row($id)
	{
		$qry=$this->db->where('id',$id)->get('about_us');
		return $qry->result_array();
	}
	public function update_about_us($data,$id)
	{
		return $this->db->where('id',$id)->update('about_us',$data);
	}
	public function delete_row($id,$path)
	{
		if($path !=''){
			unlink('assets/uploads/about_us/'.$path);
		}
		return $this->db->where('id',$id)->delete('about_us');
	}
}

?>