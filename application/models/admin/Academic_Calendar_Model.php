<?php 
class Academic_Calendar_Model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_calendar_details($data)
	{
		return $this->db->insert('academic_calendar',$data);
	}
	public function calendar_information()
	{
		$query=$this->db->get('academic_calendar');
		return $query->result();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('academic_calendar',array('id'=>$id));
		return $query->result_array();
	}
	public function update_calendar($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('academic_calendar',$data);
	}
	public function delete_row($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('academic_calendar');
	}
}

?>