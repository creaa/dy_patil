<?php 
class Add_menu_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function addmenu($data)
	{
		return $this->db->insert('menu',$data);
	}
	public function update_menu($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('menu',$data);
	}
	public function get_menu_row($id)
	{
		$query=$this->db->get_where('menu',array('id'=>$id));
		return $query->row_array();
	}
	public function get_all_menu()
	{
		// $condition=array('status'=>'active','parent_id ='=>0);
		// $this->db->where($condition);
		$query= $this->db->get('menu');
		return $query->result();
	}
	public function delete_single_menu($id)
	{
		$this->db->where('menu.id',$id);
		return $this->db->delete('menu');
	}
	public function update_status($udata,$uid)
	{
		$this->db->where("id",$uid);	 
		return $this->db->update("menu",$udata);	 
	}
}
?>