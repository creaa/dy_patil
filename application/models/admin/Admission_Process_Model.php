<?php 
class Admission_Process_Model   extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_admission_steps($data)
	{ 
		return $this->db->insert('admission_process',$data);	
	}
	public function get_admission()
	{
		$query=$this->db->get('admission_process');
		return $query->result_array();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('admission_process',array('id'=>$id));
		return $query->result_array();
	}
	public function update_admission_steps($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('admission_process',$data);
	}
	public function delete_row($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('admission_process');
	}
	public function change_status($id,$data)
	{
		$this->db->where('id',$id);
		return $this->db->update('admission_process',$data);
	}
}
?>