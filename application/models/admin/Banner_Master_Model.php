<?php
class Banner_Master_Model extends ci_model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function save_banners($data)
	{
		return $this->db->insert('banners',$data);
	}
	public function get_all_banners()
	{
		$qry=$this->db->get('banners');
		return $qry->result_array();
	}
	public function get_banners()
	{
		return $this->db->get('banners')->result_array();
	}
	public function get_img_path($id)
	{
		return $this->db->where('id',$id)->get('banners')->result();
	}
	public deletebaneers()
	{
		return $this->db->where('id',$id)->delete('banners');
	}
	public function get_single_banner($id)
	{
		return $this->db->where('id',$id)->get('banners')->result();
	}
}
?>