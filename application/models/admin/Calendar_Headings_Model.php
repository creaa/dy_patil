<?php
class Calendar_Headings_Model extends ci_model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function calendar_headings()
	{
		return $this->db->get('academic_calendar_header')->result();
	}
	public function save_calendar_heading($data)
	{
		return $this->db->insert('academic_calendar_header',$data);		
	}
	public function update_calendar_heading($data,$id)
	{
		return $this->db->where('id',$id)->update('academic_calendar_header',$data);
	}
	public function get_one_row($id)
	{
		return $this->db->where('id',$id)->get('academic_calendar_header')->result_array();
	}
	public function delete_row($id)
	{
		return $this->db->where('id',$id)->delete('academic_calendar_header');
	}
}
?>