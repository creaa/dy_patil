<?php
class Committees_Model extends ci_model
{
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function save_committes($data)
	{
		return $this->db->insert('committees',$data);
	}
	public function Getcommittees_name()
	{
		return $this->db->get('committees')->result_array();
	}
	public function get_one_row($id)
	{
		$Qry=$this->db->where('id',$id)->get('committees');
		return $Qry->result_array();
	}
	public function update_committes($data,$id)
	{
		return $this->db->where('id',$id)->update('committees',$data);
	}
}
?>