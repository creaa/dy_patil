<?php 
class Contact_Us_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function add_contacts_details($data)
	{
		return $this->db->insert('tbl_contact',$data);
	}
	public function update_contact_details($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('tbl_contact',$data);
	}
	public function get_contact_details()
	{
		$query=$this->db->get('tbl_contact');
		return $query->result();
	}
	public function get_single_row($id)
	{
		$query=$this->db->get_where('tbl_contact',array('id'=>$id));
		return $query->result_array();
	}
	public function delete_row($id,$path='')
	{
		$this->db->where('id',$id);
		if($path !=''){
			unlink('assets/uploads/'.$path);		
		}
		return $this->db->delete('tbl_contact');
	}
}

?>