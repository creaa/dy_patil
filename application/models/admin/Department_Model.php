<?php 
class Department_Model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_department_details($data)
	{
		return $this->db->insert('school_department',$data);
	}
	public function department_information()
	{
		$query=$this->db->get('school_department');
		return $query->result();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('school_department',array('id'=>$id));
		return $query->result_array();
	}
	public function update_department($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('school_department',$data);
	}
	public function delete_row($id,$path='')
	{
		$this->db->where('id',$id);
		if($path !=''){
			unlink('assets/uploads/school/'.$path);
		}
			
		return $this->db->delete('school_department');
	}
	public function change_status($id,$data)
	{
		$this->db->where('id',$id);
		return $this->db->update('school_department',$data);
	}
}

?>