<?php
class Facility_Model extends ci_model
{
	public function __construct(){
		parent::__construct();
	}
	public function save_facility($data)
	{
		return $this->db->insert('facility',$data);
	}
	public function get_facility_deatils()
	{
		return $this->db->get('facility')->result_array();
	}
	public function update_facility($post,$id)
	{
		return $this->db->where('id',$id)->update('facility',$post);
	}
	public function get_one_row($id)
	{
		return $this->db->where('id',$id)->get('facility')->result_array();
	}
	public function delete_row($id,$path)
	{
		unlink('assets/uploads/facility/'.$path);
		return $this->db->where('id',$id)->delete('facility');
	}
}
?>