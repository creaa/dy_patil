<?php 
class Faculties_Model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_faculty($data)
	{ 
		return $this->db->insert('faculty',$data);	
	}
	public function get_faculties_deatils()
	{
		$query=$this->db->get('faculty');
		return $query->result_array();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('faculty',array('id'=>$id));
		return $query->result_array();
	}
	public function update_faculty($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('faculty',$data);
	}
	public function delete_row($id,$path='')
	{
		$this->db->where('id',$id);	
		return $this->db->delete('faculty');
	}
	public function change_status($id,$data)
	{
		$this->db->where('id',$id);
		return $this->db->update('faculty',$data);
	}
}
?>