<?php 
class Home_model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_home_page_details($data)
	{
		return $this->db->insert('home',$data);
	}
	public function home_page_details()
	{
		$query=$this->db->get('home');
		return $query->result();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('home',array('id'=>$id));
		return $query->result_array();
	}
	public function update_home_page($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('home',$data);
	}
	public function delete_row($id,$path='')
	{
		$this->db->where('id',$id);
		if($path !=''){
			unlink('assets/uploads/home/'.$path);
		}
			
		return $this->db->delete('home');
	}
	public function change_status($id,$data)
	{
		$this->db->where('id',$id);
		return $this->db->update('home',$data);
	}
}

?>