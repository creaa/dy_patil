<?php
class Placements_And_Internships_Model extends ci_model
{
	public function __construct(){
		parent::__construct();
	}
	public function save_placements_and_internships($data)
	{
		return $this->db->insert('placements_and_internships',$data);
	}
	public function get_placements_and_internships()
	{
		return $this->db->get('placements_and_internships')->result_array();
	}
	public function update_placements_and_internships($post,$id)
	{
		return $this->db->where('id',$id)->update('placements_and_internships',$post);
	}
	public function get_one_row($id)
	{
		return $this->db->where('id',$id)->get('placements_and_internships')->result_array();
	}
	public function delete_row($id,$path)
	{
		unlink('assets/uploads/placements_and_internships/'.$path);
		return $this->db->where('id',$id)->delete('placements_and_internships');
	}
}
?>