<?php
class Register_model extends CI_Model
{
	public function __construct()
	{ 
		parent::__construct();
		$this->load->database();
	}
	public function update_profile($uid,$data)
	{ 
		$this->db->where('id',$uid);
		return $query=$this->db->update('tbl_login',$data) ;
	}
	public function get_profile_details($uid)
	{
		$query = $this->db->query('SELECT * FROM tbl_login where id="'.$uid.'"');
		return $query->row_array();	 
	}
	public function check_user_id($aid)
	{
		$query = $this->db->query('SELECT * FROM tbl_socia_link where user_id="'.$aid.'"');
		return $query->row_array();	
	}
	public function insert_social_link($udata)
	{
		return $this->db->insert("tbl_socia_link",$udata);
	} 
	public function update_social_link($uid,$data)
	{
		$this->db->where('user_id',$uid);
		$query=$this->db->update('tbl_socia_link',$data) ;
	}
	public function get_all_social($uid)
	{
		$query = $this->db->query('SELECT * FROM tbl_socia_link where user_id="'.$uid.'"');
		return $query->row_array();	 
	}
	public function show_all_social($uid)
	{
		$query = $this->db->query('SELECT * FROM tbl_socia_link where user_id="'.$uid.'"');
		return $query->result();	 
	}
//-------------- Bulk Send Mail script---------------------// 
	public function send_mail_data($mdata)
	{
		return $this->db->insert("tbl_send_mail",$mdata);	
	}
	public function get_newslatter_user()
	{
		$query= $this->db->get("tbl_newslatter");	
		return $query->result();
	}
	public function delete_subscribe_user($id)
	{  
		$this->db->where('tbl_newslatter.id',$id);
		return $this->db->delete('tbl_newslatter');	}
	public function update_userstatus($uid,$udata)
	{
		$this->db->where("id",$uid);	 
		return $this->db->update("tbl_user",$udata);	 
	}
	public function delete_userlist($id)
	{  
		$this->db->where('tbl_user.id',$id);
		return $this->db->delete('tbl_user');	}	
}
?>