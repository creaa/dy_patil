<?php
class Research_Model extends ci_model
{
	public function __construct(){
		parent::__construct();
	}
	public function save_research($data)
	{
		return $this->db->insert('research',$data);
	}
	public function get_research_deatils()
	{
		return $this->db->get('research')->result_array();
	}
	public function update_research($post,$id)
	{
		return $this->db->where('id',$id)->update('research',$post);
	}
	public function get_one_row($id)
	{
		return $this->db->where('id',$id)->get('research')->result_array();
	}
	public function delete_row($id,$path)
	{
		unlink('assets/uploads/research/'.$path);
		return $this->db->where('id',$id)->delete('research');
	}
}
?>