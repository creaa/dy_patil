<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_configuration_model extends CI_Model {

 protected $_table = 'site_cofig';
 protected $protected_attributes = array('id');

 public function __construct()
 {
  parent::__construct();
  error_reporting(0);
 } 
 
 function get_site_configuration($id)
 {
$this->db->where('id',$id);	 
return $this->db->get('site_cofig')->row_array();	 
}
 

 function get_active_nanny_user()
 {
 $this->db->where('user_type',3);
 $this->db->where('approve',1);
$this->db->from('mst_user');
return $this->db->count_all_results(); 
 } 
function get_active_recruiter()
 {
 $this->db->where('user_type',2);
 $this->db->where('approve',1);
$this->db->from('mst_user');
return $this->db->count_all_results(); 
 }
function get_unapprove_user()
 {
	  $this->db->where('status',1); 
 $this->db->where('approve',0);
$this->db->from('mst_user');
return $this->db->count_all_results(); 
 }
function unapproved_job_listing()
 {
$this->db->where('approve',0);	  
 $this->db->where('status',1);
$this->db->from('job_post');
return $this->db->count_all_results(); 
 }
function approved_job_listing()
 {
$this->db->where('approve',1);	  
 $this->db->where('status',1);
$this->db->from('job_post');
return $this->db->count_all_results(); 
 }

function add_config($data)
{
$this->db->insert('site_cofig',$data);	
}

function update_config($eid,$data)
{
$this->db->where('id',$eid);
$this->db->update('site_cofig',$data);	
}
	
}
?>