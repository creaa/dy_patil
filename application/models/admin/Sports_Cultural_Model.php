<?php
class Sports_Cultural_Model extends ci_model
{
	public function __construct(){
		parent::__construct();
	}
	public function save_sport_culture($data)
	{
		return $this->db->insert('sports_cultural',$data);
	}
	public function get_sport_culture()
	{
		return $this->db->get('sports_cultural')->result_array();
	}
	public function update_sport_culture($post,$id)
	{
		return $this->db->where('id',$id)->update('sports_cultural',$post);
	}
	public function get_one_row($id)
	{
		return $this->db->where('id',$id)->get('sports_cultural')->result_array();
	}
	public function delete_row($id,$path)
	{
		unlink('assets/uploads/sports_cultural/'.$path);
		return $this->db->where('id',$id)->delete('sports_cultural');
	}
}
?>