<?php 
class Values_List_Model extends CI_model
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->database();
	}
	public function save_values($data)
	{
		return $this->db->insert('list_of_values',$data);
	}
	public function get_all_values()
	{
		$query=$this->db->get('list_of_values');
		return $query->result();
	}
	public function get_one_row($id)
	{
		$query=$this->db->get_where('list_of_values',array('id'=>$id));
		return $query->result_array();
	}
	public function update_values($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('list_of_values',$data);
	}
	public function delete_values($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('list_of_values');
	}
}

?>