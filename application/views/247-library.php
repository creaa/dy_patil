<div class="banner">
<img src="<?php echo base_url('assets/uploads/banners/'.$banners[0]['image']);?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="recruiters_section1 transportation_section1">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<?php foreach($fac as $facility){ ?>
				<div class="transportation_sec_bottom">
					<h2><?=$facility['title']?></h2>
					<?=$facility['description']?>
					<h3>Library Details</h3>
					<p>Books, e-Books, Hadcopy Journals and E-Journals in Libraray Summay					
					</p>
				</div>
			<?php } ?>
			<div class="table-responsive tab_sec">          
	<table class="table">
		<thead>
		  <tr>
			<th style="background: #6d6e71; color: #fff;">Sr No</th>
			<th style="background: #6d6e71; color: #fff;">No. of Titles</th>
			<th style="background: #6d6e71; color: #fff;">No of Books/ Reference Books</th>
			<th style="background: #6d6e71; color: #fff;">No of Journals* </th>
			<th style="background: #6d6e71; color: #fff;">E Books*</th>
		  </tr>
		</thead>
    <tbody>      
      <tr>
        <td style="font-weight: 700;">Total</td>
        <td style="font-weight: 700;">33828</td>
        <td style="font-weight: 700;">13600</td>
        <td style="font-weight: 700;">9160</td>
        <td style="font-weight: 700;" rowspan="15">9703 (Springer E-Books Collection)</td>
      </tr>
     </tbody>
    <thead>
      <tr>
        <th colspan="5" style="background: #9f1c33; color: #fff; border:0; text-align: center; border: 1px solid #636363;">E-Journals  Packages </th>
      </tr>
      <tr>
        <th style="background: #6d6e71; color: #fff;">Sr No</th>
        <th style="background: #6d6e71; color: #fff;" colspan="3">E-Journals Pakages *</th>
        <th style="background: #6d6e71; color: #fff;">Total</th>
      </tr>
      </tr>
    </thead>

    <tbody>
    	<tr>
      	<td>1</td>
        <td colspan="3">ASME</td>
        <td>32</td>
      </tr>

      <tr>
      	<td>2</td>
        <td colspan="3">ASCE</td>
        <td>38</td>
      </tr>

      <tr>
      	<td>3</td>
        <td colspan="3">Springer 3 Subject Collections</td>
        <td>532</td>
      </tr>

      <tr>
        <td style="font-weight: 700;"></td>
        <td colspan="3" style="font-weight: 700;">Total</td>
        <td style="font-weight: 700;">602</td>
      </tr>


    </tbody>
  </table>
  </div>
			<div class="trans_slider">
				<?php foreach($fac as $fimg){ 
				if($fimg['image'] !=""){?>
				<div>
					<img src="<?php echo base_url('assets/uploads/facility/'.$fimg['image']);?>" class="img-responsive" alt="">
				</div>
				<?php } }?>
			</div>
		</div>
	</div>
</div>
</div></div>