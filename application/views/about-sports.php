<div class="banner">
<img src="<?php echo base_url('assets/uploads/banners/'.$banners[0]['image']);?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="about_sports_section1">
	<div class="container">
		<h2>About - Sports</h2>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<?php foreach($sc as $secone){ 
				if($secone['image']=="" && $secone['page_section']==1){ ?>
				<div class="about_sports_left">
					<?=$secone['description']?>
				</div>
				<?php } } ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<?php foreach($sc as $seconeimg){
				if($seconeimg['page_section']==1){ ?>
				<div class="about_sports_right">
					<img src="<?php echo base_url('assets/uploads/sports_cultural/'.$seconeimg['image']);?>" class="img-responsive" alt="">
				</div>
			<?php } } ?>
			</div>
		</div>
	</div>
</div>

<div class="about_sports_section2">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<?php foreach($sc as $sectwo){ 
			if($sectwo['page_section']==2 && $sectwo['image']==""){ ?>
				<div class="about_sports_section2_bottom">
					<?=$sectwo['description']?>
				</div>
			<?php } } ?>
				<?php foreach($sc as $sectwo){ 
			if($sectwo['page_section']==2 && $sectwo['image'] !=""){ ?>
				<div class="about_sport_img">
					<img src="<?php echo base_url('assets/uploads/sports_cultural/'.$sectwo['image']);?>" class="img-responsive" alt="">
				</div>
				<?php } } ?>
			</div>
		</div>
	</div>
</div>
</div>