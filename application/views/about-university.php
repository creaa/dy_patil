<!-- banner section start -->
<div class="banner">
<img src="<?php echo base_url('assets/images/about/about_university_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="about_section1">
<div class="container">
<div class="row">
	
<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
	<div class="about_section_left" data-aos="fade-right" data-aos-duration="1500">
		<h2><?php echo $detail[0]['title']?></h2>	
		<?php echo $detail[0]['description']?>
	</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 about_section_right_pos">
	<div class="about_section_right" data-aos="fade-left" data-aos-duration="1500">
	<img src="<?php echo base_url('assets/uploads/about_us/'.$detail[0]['image']);?>" class="img-responsive" alt="">	
</div>
</div>
</div>
</div>	
</div>
</div>