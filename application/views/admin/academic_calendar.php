<?php include 'include/header.php';?>
<div class="container-fluid-full">
	<div class="row-fluid">
		<?php include 'include/sidebar.php';?>
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		<div id="content" class="span10">
		<ul class="breadcrumb">
			<li>
				<i class="icon-department"></i>
				<a href="index.html">department</a>
				<i class="icon-angle-right"></i> 
			</li>
			<li>
				<i class="icon-edit"></i>
				<a href="#">Manage Academic Calendar</a>
			</li>
		</ul>
		<div class="row-fluid sortable">
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Calendar Information</h2>
					<div class="box-icon">
						<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
					</div>
				</div>
				<?php if(!empty($this->session->flashdata('insert') || $this->session->flashdata('update') || $this->session->flashdata('delete'))){ ?>
				<div class="alert alert-success del" role="alert">
					<?php echo $this->session->flashdata('insert'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete');?>
				</div>
				<?php } ?>
				<div class="box-content">
					<form class="form-horizontal"  method="post" action="<?php echo base_url();?>admin/Academic_Calendar/addcontent" enctype="multipart/form-data">
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Department Name</label>
						<div class="controls">
							<select name="department_id" class="input-xlarge focused">
								<option value="">-Select-</option>
								<?php foreach($values as $sections) { ?>
								<option value="<?=$sections->id;?>" <?php if($edit[0]['department_id']==$sections->id){ echo "selected"; } ?>><?=$sections->values;?></option>
								<?php } ?>
							</select>
							<span style="color: red"><?php echo form_error('department_id');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Commencement &  End of term</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="Commencement" type="text" value="<?php echo $edit[0]['commencement_end_of_term'];?>">
						<span style="color: red"><?php echo form_error('Commencement');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Term 1</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="term_first" type="text" value="<?php echo $edit[0]['first_term'];?>">
						<span style="color: red"><?php echo form_error('term_first');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Term 2</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="term_second" type="text" value="<?php echo $edit[0]['second_term'];?>">
						<span style="color: red"><?php echo form_error('term_second');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Vacation</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="vacation" type="text" value="<?php echo $edit[0]['vacation'];?>">
						<span style="color: red"><?php echo form_error('vacation');?></span>
						</div>
					</div>
					<div class="control-group"></div>
					<div class="form-actions">
						<input type="hidden" name="eid" value="<?php echo $edit[0]['id']; ?>"/>
						<input type="submit" class="btn btn-primary" value="Save" />
						<?php if($edit[0]['id'] !='')
						{ ?>
							<input type="submit" class="btn btn-danger" value="cancel" onclick="cancel();" />
						<?php } ?>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span> List</h2>
				<div class="box-icon">
				<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
				</div>
				<div class="box-content">
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						<thead>
							<tr>
								<th>Srno</th>
								<th>Department Name</th>
								<th>Commencement</th>
								<th>Term First</th>
								<th>Term Second</th>
								<th>Vacation</th>
								<th>Action</th>
							</tr>
						</thead>   
						<tbody>
						<?php
						foreach($values as $dpnmae){
							$name[$dpnmae->id]=$dpnmae->values;
						}
						$i=1;
						foreach($details as $de)
						{ ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td class="center"><?php echo $name[$de->department_id]; ?></td>
								<td class="center"><?php echo $de->commencement_end_of_term; ?></td>
								<td><?php echo $de->first_term; ?></td>
								<td class="center"><?php echo $de->second_term; ?></td>
								<td class="center"><?php echo $de->vacation; ?></td>
								
								<td>
								<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>Academic_Calendar/edit/<?php echo $de->id;?>" style="cursor:pointer;">
								<i class="halflings-icon white edit"></i>  
								</a>
								<a class="btn btn-danger" onclick="delete_confirm(<?php echo $de->id;?>,'<?php echo $de->image; ?>')" style="cursor:pointer;">
								<i class="halflings-icon white trash"></i> 
								</a>
								</td>
							</tr>
						<?php $i++; } ?>	
						</tbody>
					</table>            
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Settings</h3>
	</div>
	<div class="modal-body">
		<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>
<?php include 'include/footer.php';?>
<script>
function set_status(status,id)
{
	var r=confirm("Do you really want to "+status+"?");	
	if(r==true)
	{
		window.location='<?php echo base_url();?>admin/Academic_Calendar/set_status/'+id+'/'+status;
	}
}
function delete_confirm(id,path)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		if(path !=''){
			window.location='<?php echo base_url();?>admin/Academic_Calendar/delete_record/'+id+'/'+path;
		}
		else{
			window.location='<?php echo base_url();?>admin/Academic_Calendar/delete/'+id;
		}
			
	}
}
function cancel()
{
	window.location='<?php echo base_url();?>admin/Academic_Calendar/cancel_update';
}
$(function() {
	setTimeout(function() {
    $(".del").hide('blind', {}, 500)
	}, 5000);
});
</script>
 