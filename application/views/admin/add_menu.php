<?php include 'include/header.php';?>
<div class="container-fluid-full">
	<div class="row-fluid">
	<!-- start: Main Menu -->
	<?php include 'include/sidebar.php';?>
	<!-- end: Main Menu -->
	<noscript>
		<div class="alert alert-block span10">
			<h4 class="alert-heading">Warning!</h4>
			<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
		</div>
	</noscript>
	<!-- start: Content -->
	<div id="content" class="span10">
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="index.html">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="#">Manage Menu</a>
		</li>
	</ul>
	<!--/row-->
	<div class="row-fluid sortable">
	<div class="box span12">
	<div class="box-header" data-original-title>
		<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Menu</h2>
		<div class="box-icon">
			<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
			<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
			<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
		</div>
	</div>
	<div class="box-content">
		<form class="form-horizontal"  method="post" action="<?php echo URL_ROOT_ADMIN?>add_menu/addmenu" enctype="multipart/form-data">
		<p style="color:red;" class="del"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete'); echo $this->session->flashdata('setstatus');?></p>
		<fieldset>
		<div class="control-group col-md-6">
			<label class="control-label" for="focusedInput"> Menu&nbsp;&nbsp;&nbsp;&nbsp; </label>
			<div class="controls">
				<input class="input-xlarge focused" id="focusedInput" name="eng_title" type="text" value="<?php echo $menu_list['title']; ?>">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="focusedInput">Select Parent Menu &nbsp;&nbsp; </label>
			<div class="controls"> 
				<select id="selectError3" name="parent" <?php if($menu_list['parent_id'] !=0){ echo'readonly'; }?>>
					<option value="0">No Parent</option>
					<?php 
					if(count($all_menu)>0)
					{
						foreach($all_menu as $menuname)
					{?>
					<option <?php if($menuname->id ==$menu_list['parent_id']){?> selected="selected"<?php } ?> value="<?php echo $menuname->id; ?>"><?php echo $menuname->title; ?></option>
					<?php }	
					}
					?>
				</select>
			</div>
		</div>
		<div class="control-group col-md-6">
			<label class="control-label" for="focusedInput">position&nbsp;&nbsp;&nbsp;&nbsp; </label>
			<div class="controls">
				<input class="input-xlarge focused" id="focusedInput" name="position" type="text" value="<?php echo $menu_list['position']; ?>">
			</div>
		</div>		
		<div class="control-group">
			<label class="control-label" for="selectError3">Location &nbsp;&nbsp; </label>
			<div class="controls">
			  <select id="selectError3" name="location" >
				<option value="Header"<?php if($menu_list['location']=='Header'){?> selected <?php }?>>Header</option>
				<option value="Mid"<?php if($menu_list['location']=='Mid'){?> selected <?php }?>>Mid</option>
				<option value="Footer"<?php if($menu_list['location']=='Footer'){?> selected <?php }?>>Footer</option>
			  </select>
			</div>
		</div>
		<div class="form-actions">
			<input type="hidden" name="eid" value="<?php echo $menu_list['id']; ?>"/>
			<input type="submit" class="btn btn-primary" value="Save" />
			<?php if($menu_list['id'] !='')
			{ ?>
				<input type="submit" class="btn btn-danger" value="cancel" onclick="cancel();" />
			<?php } ?>
		</div>
		</fieldset>
		</form>
	</div>
	</div><!--/span-->
	</div><!--/row-->
	<!--/row-->
	<div class="row-fluid sortable">		
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon user"></i><span class="break"></span>Menu List</h2>
			<div class="box-icon">
			<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
			<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
			<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
			</div>
		</div>
		<div class="box-content">
		<table class="table table-striped table-bordered bootstrap-datatable datatable">
			<thead>
				<tr>
					<th> Menu</th>
					<th>Parent Menu</th>
					<th>Location</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>   
			<tbody>
			<?php
			$i=1; 
			// echo'<pre>';print_r($all_menu); die;
			foreach($all_menu as $menu)
			{
			?>
			<tr>
			<td class="center"><?php echo strip_tags($menu->title); ?></td>
			<td class="center"><?php echo $menu->parent_id;//if($menu->parent_id==0){ echo 'No Parent Menu';}else{ $this->method_call->get_menu_name($menu->parent_id);} ?></td>
			<td class="center"><?php echo ucwords($menu->location); ?></td>
			<td class="center">
			<span class="label label-success"><?php echo ucwords($menu->status); ?></span>
			</td>
			<td class="center">
			<?php if($menu->status=='active'){?>
			<a class="btn btn-success" href="#" onclick="set_status('deactive',<?php echo $menu->id;?>)" style="cursor:pointer;"  title=" Click here for Active">
			<i class="halflings-icon white ok" id="iicon2"></i>  
			</a>
			<?php } 
			else {?> 
			<a class="btn btn-warning" href="#" onclick="set_status('active',<?php echo $menu->id;?>)" style="cursor:pointer;"  title=" Click here for Active">
			<i class="halflings-icon white exclamation-sign" id="iicon2"></i>  
			</a>
			<?php } ?>
			<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>add_menu/edit/<?php echo $menu->id;?>"style="cursor:pointer;">
			<i class="halflings-icon white edit"></i>  
			</a>
			<a class="btn btn-danger" onclick="delete_confirm('<?php echo $menu->id;?>')" style="cursor:pointer;">
			<i class="halflings-icon white trash"></i> 
			</a>
			</td>
			</tr>
			<?php }?>	
			</tbody>
		</table>            
		</div>
	</div><!--/span-->
	</div><!--/row-->
	</div><!--/.fluid-container-->
	<!-- end: Content -->
	</div><!--/#content.span10-->
</div><!--/fluid-row-->
<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Settings</h3>
	</div>
	<div class="modal-body">
		<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>
<?php include 'include/footer.php'?>
<script>
// code for set status
function set_status(status,id)
{
	var r=confirm("Do you really want to "+status+"?");	
	if(r==true)
	{
		window.location='<?php echo base_url();?>admin/add_menu/set_status/'+id+'/'+status;
	}
}
function delete_confirm(id)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		window.location='<?php echo base_url();?>admin/add_menu/delete_menu/'+id;
	}
}
function cancel()
{
	window.location='<?php echo base_url();?>admin/add_menu/cancel_update';
}
$(function() {
	// setTimeout() function will be fired after page is loaded
	// it will wait for 5 sec. and then will fire
	// $("#successMessage").hide() function
	setTimeout(function() {
    $(".del").hide('blind', {}, 500)
	}, 5000);
});
</script>