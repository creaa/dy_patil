<?php include 'include/header.php';?>
<style>
.img-wraps {
    position: relative;
    display: inline-block;
   
    font-size: 0;
}
.img-wraps .closes {
    position: absolute;
    top: 5px;
    right: 8px;
    z-index: 100;
    background-color: #FFF;
    padding: 4px 3px;
    
    color: #000;
    font-weight: bold;
    cursor: pointer;
   
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
    border:1px solid red;
}
.img-wraps:hover .closes {
    opacity: 1;
}
</style>
<div class="container-fluid-full">
	<div class="row-fluid"><?php include 'include/sidebar.php';?><noscript>
	<div class="alert alert-block span10">
	<h4 class="alert-heading">Warning!</h4>
	<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
	</div>
	</noscript>
		<div id="content" class="span10">
			<ul class="breadcrumb">
				<li>
					<i class="icon-Philanthropy"></i>
					<a href="index.html">Manage Banners</a>
					<i class="icon-angle-right"></i> 
				</li>
				<li>
					<i class="icon-edit"></i>
					<a href="#">Add Banners</a>
				</li>
			</ul>
			<?php 
			if($this->session->flashdata('success')){?>
			<div class="alert alert-success">
				<strong><?php echo $this->session->flashdata('success');?></strong>				
			</div>
			<?php } ?>
	<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Banners</h2>
			<div class="box-icon">
				<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
			</div>
		</div>
	<div class="box-content">
	<form class="form-horizontal"  method="post" action="<?php echo base_url();?>admin/Banner_Master/addbanners" enctype="multipart/form-data">
	<p style="color:red;" class="del"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete'); echo $this->session->flashdata('setstatus'); echo $this->session->flashdata('require');?> </p>
	<fieldset>
	<input type="hidden" name="edit_id" id="edit_id" value="">
		<div class="row" id="masterbanner">
			<div class="control-group col-md-6">
				<label class="control-label" for="focusedInput">Pages</label>
				<div class="controls">
					<select name="page_id">
						<option value="">-Select-</option>
						<?php foreach($values as $sections) { ?>
						<option value="<?=$sections->id;?>" <?php if($edit[0]['page_name_id']==$sections->id){ echo "selected"; } ?>><?=$sections->values;?></option>
						<?php } ?>
					</select>
					<span style="color: red"><?php echo form_error('title');?></span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="control-group col-md-6">
				<label class="control-label" for="focusedInput">Banner Image</label>
				<input type="file" name="banners_img[]" multiple class="input-xlarge focused">
				<div class="controls">									
					<span style="color: red"><?php echo form_error('ApplicationImg');?></span>
				</div>
			</div>									
		</div>
		<div class="control-group"></div>
		<div class="form-actions">
		<input type="submit" class="btn btn-primary" value="Save" />
		</div>
		</fieldset>
		</form>
	</div>
	</div>
	</div>
	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
			<h2><i class="halflings-icon user"></i><span class="break"></span> List</h2>
			<div class="box-icon">
			<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
			<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
			<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
			</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
						<tr>
							<th>Srno</th>
							<th>Image</th>
							<th>Location</th>
							<th>date create</th>
							<th>Action</th>
						</tr>
					</thead>   
					<tbody>
						<?php
						foreach($values as $dpnmae){
							$name[$dpnmae->id]=$dpnmae->values;
						}
						$i=1;
						foreach($banners as $img)
						{ ?>
							<tr>
							<td width="1%" class="center"><?php echo $i; ?></td>
							<td width="20%">
							<img src="<?php echo base_url('assets/uploads/banners/'.explode(',',$img['image'])[0]);?>" class="" style="thumbnail">
							</td>									
							<td class="center"><?php echo $name[$img['page_id']]?></td>
							<td class="center"><?php echo $img['date_created'] ?></td>
							<td class="center">
								<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>Banner_Master/edit/<?php echo $img['id'];?>" style="cursor:pointer;">
								<i class="halflings-icon white edit"></i>  
								</a>
								<a class="btn btn-danger" onclick="delete_confirm('<?php echo $img['id'];?>')" style="cursor:pointer;">
								<i class="halflings-icon white trash"></i> 
								</a>
							</td>
							</tr>
						<?php $i++; } ?>	
					</tbody>
				</table>            
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="modal hide fade" id="myModal">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h3>Settings</h3>
</div>
<div class="modal-body"><p>Here settings can be configured...</p></div>
<div class="modal-footer">
<a href="#" class="btn" data-dismiss="modal">Close</a>
<a href="#" class="btn btn-primary">Save changes</a>
</div>
</div>

<?php include 'include/footer.php';?>
<script>
function delete_confirm(id)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		window.location='<?php echo base_url();?>admin/Banner_Master/delete_banners/'+id;
	}
}
function cancel()
{
	window.location='<?php echo base_url();?>admin/Banner_Master/cancel_update';
}
$(function() {
	setTimeout(function() {
    $(".alert-success").hide('blind', {}, 500)
	}, 5000);
});
</script>
