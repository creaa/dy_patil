<?php include 'include/header.php';?>
<div class="container-fluid-full">
<div class="row-fluid">
<!-- start: Main Menu -->
<?php include 'include/sidebar.php';?>
<!-- end: Main Menu -->
<noscript>
<div class="alert alert-block span10">
<h4 class="alert-heading">Warning!</h4>
<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>
<!-- start: Content -->
<div id="content" class="span10">

<ul class="breadcrumb">
<li>
<i class="icon-home"></i>
<a href="index.html">Home</a>
<i class="icon-angle-right"></i> 
</li>
<li>
<i class="icon-edit"></i>
<a href="#">Manage Home Page</a>
</li>
</ul>
<!--/row-->
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Content</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">
<form class="form-horizontal"  method="post" action="<?php echo base_url();?>admin/Contact_Us/addcontent" enctype="multipart/form-data">
<p style="color:red;" class="del"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete'); echo $this->session->flashdata('setstatus'); echo $this->session->flashdata('cancel');?></p>
<fieldset>
<?php// print_r($single_row);?>
<div class="control-group col-md-6">
	<label class="control-label" for="focusedInput">Title</label>
	<div class="controls">
	<input class="input-xlarge focused" id="focusedInput" name="TITLE" type="text" value="<?php echo 
	@$single_row[0]['title']; ?>">
	<span style="color: red"><?php echo form_error('title');?></span>
	</div>
</div>
<div class="control-group col-md-6">
	<label class="control-label" for="focusedInput">Website Link</label>
	<div class="controls">
	<input class="input-xlarge focused" id="focusedInput" name="Website_link" type="text" value="<?php echo 
	@$single_row[0]['website_link']; ?>">
	<span style="color: red"><?php echo form_error('Website_link');?></span>
	</div>
</div>
<div class="control-group col-md-6">
	<label class="control-label" for="focusedInput">Email id</label>
	<div class="controls">
		<input class="input-xlarge focused" id="focusedInput" name="EMAIL_ID" type="text" value="<?php echo 
		@$single_row[0]['email']; ?>">
		<span style="color: red"><?php echo form_error('EMAIL_ID');?></span>
	</div>
</div>
<div class="control-group col-md-6">
	<label class="control-label" for="focusedInput">Phone No</label>
	<div class="controls">
	<input class="input-xlarge focused" id="focusedInput" name="PHONE_NO" type="text" value="<?php echo @$single_row[0]['phone']; ?>">
	<span style="color: red"><?php echo form_error('phone');?></span>
	</div>
</div>	
<div class="control-group col-md-6">
	<label class="control-label" for="textarea2"> Address&nbsp;&nbsp;</label>
	<div class="controls">
		<textarea name="editor1"><?php echo @$single_row[0]['address']; ?></textarea>
	<script>
		CKEDITOR.replace( 'editor1', {
			width: '150%',
			height: 200
		} );
	</script>
		<span style="color: red"><?php echo form_error('descrip');?></span>
	</div>
</div>
<div class="control-group col-md-6">
	<label class="control-label" for="textarea2">Map&nbsp;&nbsp;</label>
	<div class="controls">
		<textarea name="map"><?php echo @$single_row[0]['map']; ?></textarea>
		<span style="color: red"><?php echo form_error('map');?></span>
	</div>
</div>
<div class="control-group col-md-6">
	<label class="control-label"> Image &nbsp;&nbsp;&nbsp;</label>
	<div class="controls">
	<input class="input-file uniform_on" id="fileInput" name="IMAGE" type="file"> <?php echo isset($single_row) ? $single_row[0]['kb_contact_banner'] : ""; ?>
	</div>
</div>
<div class="control-group">
</div>
<div class="form-actions">
<input type="hidden" name="eid" value="<?php echo $single_row[0]['id']; ?>"/>
<input type="submit" class="btn btn-primary" value="Save" />
<?php if($single_row[0]['id']>0){ ?>
	<input type="button" class="btn btn-danger" value="cancel" onclick="cancel_editing()"/>
<?php } ?>

</div>
</fieldset>
</form>
</div>
</div><!--/span-->

</div><!--/row-->



<!--/row-->
<div class="row-fluid sortable">		
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon user"></i><span class="break"></span> List</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">
<table class="table table-striped table-bordered bootstrap-datatable datatable">
<thead>
<tr>
<th>SrNO</th>
<th>title</th>
<th>images</th>
<th>Address</th>
<th>Email id</th>
<th>Phone No</th>
<th>Action</th>

</tr>
</thead>   
<tbody>
<?php
	$i=1;
	// print_r($details);
	foreach($contacts as $de)
	{
	?>
<tr>
<td class="center"><?php echo ucwords($de->id); ?></td>
<td class="center"><?php echo ucwords($de->title); ?></td>
<td class="center"><img src="<?php echo base_url('assets/uploads/'.$de->kb_contact_banner); ?>" style=" height:100px; width: 100px;" /></td>
<td class="center"><?php echo strip_tags($de->address); ?></td>
<td class="center"><?php echo strip_tags($de->email); ?></td>
<td class="center"><?php echo strip_tags($de->phone); ?></td>
<td class="center">

<?php if($de->status=='active'){?>
<a class="btn btn-success" href="#" onclick="set_status('deactive',<?php echo $de->id;?>)" style="cursor:pointer;"  title=" Click here for Active">
<i class="halflings-icon white ok" id="iicon2"></i>  
</a>
<?php } 
else {?> 
<a class="btn btn-warning" href="#" onclick="set_status('active',<?php echo $de->id;?>)" style="cursor:pointer;"  title=" Click here for Active">
<i class="halflings-icon white exclamation-sign" id="iicon2"></i>  
</a>
<?php } ?>

<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>Contact_Us/edit/<?php echo $de->id;?>" style="cursor:pointer;">
<i class="halflings-icon white edit"></i>  
</a>
<a class="btn btn-danger" onclick="delete_confirm(<?php echo $de->id;?>,'<?php echo $de->kb_contact_banner;?>')" style="cursor:pointer;">
<i class="halflings-icon white trash"></i> 
</a>
</td>
</tr>

<?php }?>	
</tbody>
</table>            
</div>
</div><!--/span-->

</div>

<!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<div class="modal hide fade" id="myModal">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h3>Settings</h3>
</div>
<div class="modal-body">
<p>Here settings can be configured...</p>
</div>
<div class="modal-footer">
<a href="#" class="btn" data-dismiss="modal">Close</a>
<a href="#" class="btn btn-primary">Save changes</a>
</div>
</div>

<?php include 'include/footer.php';?>
<script>
// code for set status
function set_status(status,id)
{
	// alert(status);
	var r=confirm("Do you really want to "+status+"?");	
	if(r==true)
	{
		window.location='<?php echo base_url();?>admin/Contact_Us/set_status/'+id+'/'+status;
	}
}
// end code
// code for delete
function delete_confirm(id,path)
{
	// alert(path);
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		if(path !=''){
		window.location='<?php echo base_url();?>admin/Contact_Us/delete_data/'+id+'/'+path;
		}
		else{
		window.location='<?php echo base_url();?>admin/Contact_Us/delete_data/'+id;	
		}
	}
}
function cancel_editing()
{
	window.location="<?php echo base_url();?>admin/Contact_Us/cancel_update";
}
$(function() {
	// setTimeout() function will be fired after page is loaded
	// it will wait for 5 sec. and then will fire
	// $("#successMessage").hide() function
	setTimeout(function() {
    $(".del").hide('blind', {}, 500)
	}, 5000);
});
// end code
</script>
 