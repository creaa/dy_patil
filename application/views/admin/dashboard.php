<?php include 'include/header.php';?>
<div class="container-fluid-full">
<div class="row-fluid">
<?php include 'include/sidebar.php';?>	
<noscript>
		<div class="alert alert-block span10">
			<h4 class="alert-heading">Warning!</h4>
			<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
		</div>
	</noscript>
	<!-- start: Content -->
	<div id="content" class="span10">
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="index.html">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Dashboard</a></li>
	</ul>
	<div class="row-fluid">
		<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
			<div class="boxchart">5,6,7,2,0,4,2,4,8,2,3,3,2</div>
			<div class="number">854<i class="icon-arrow-up"></i></div>
			<div class="title">visits</div>
			<div class="footer">
				<a href="#"> read full report</a>
			</div>	
		</div>
		<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
			<div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
			<div class="number">123<i class="icon-arrow-up"></i></div>
			<div class="title">sales</div>
			<div class="footer">
				<a href="#"> read full report</a>
			</div>
		</div>
		<div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
			<div class="boxchart">5,6,7,2,0,-4,-2,4,8,2,3,3,2</div>
			<div class="number">982<i class="icon-arrow-up"></i></div>
			<div class="title">orders</div>
			<div class="footer">
				<a href="#"> read full report</a>
			</div>
		</div>
		<div class="span3 statbox yellow" onTablet="span6" onDesktop="span3">
			<div class="boxchart">7,2,2,2,1,-4,-2,4,8,,0,3,3,5</div>
			<div class="number">678<i class="icon-arrow-down"></i></div>
			<div class="title">visits</div>
			<div class="footer">
				<a href="#"> read full report</a>
			</div>
		</div>	
	</div>		
	</div><!--/.fluid-container-->
			<!-- end: Content -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	<?php include 'include/footer.php'?>
