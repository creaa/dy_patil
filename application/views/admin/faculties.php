<?php include 'include/header.php';?>
<div class="container-fluid-full">
	<div class="row-fluid">
		<?php include 'include/sidebar.php';?>
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		<div id="content" class="span10">
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="icon-angle-right"></i> 
			</li>
			<li>
				<i class="icon-edit"></i>
				<a href="#">Manage Home Page</a>
			</li>
		</ul>
		<div class="row-fluid sortable">
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Content</h2>
					<div class="box-icon">
						<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
					</div>
				</div>
				<div class="box-content">
					<form class="form-horizontal"  method="post" action="<?php echo base_url();?>admin/Faculties/addcontent" enctype="multipart/form-data">
					<p style="color:green;" class="del"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete');echo $this->session->flashdata('require');?> </p>
					<fieldset>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Department</label>
						<div class="controls">
							<select name="dpt_id" class="input-xxlarge focused">
								<option value="">-Select-</option>
								<?php foreach($values as $sections) { ?>
								<option value="<?=$sections->id;?>" <?php if($edit[0]['page_name_id']==$sections->id){ echo "selected"; } ?>><?=$sections->values;?></option>
								<?php } ?>
							</select>
							<span style="color: red"><?php echo form_error('title');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Faculty Name</label>
						<div class="controls">
						<input class="input-xxlarge focused" id="focusedInput" name="faculty_name" type="text" value="<?php echo $edit[0]['faculty_name'];?>">
						<span style="color: red"><?php echo form_error('faculty_name');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Contact No</label>
						<div class="controls">
						<input class="input-xxlarge focused" id="focusedInput" name="contact_no" type="text" value="<?php echo $edit[0]['contact_no'];?>">
						<span style="color: red"><?php echo form_error('contact_no');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Email Id</label>
						<div class="controls">
						<input class="input-xxlarge focused" id="focusedInput" name="email_id" type="text" value="<?php echo $edit[0]['email_id'];?>">
						<span style="color: red"><?php echo form_error('email_id');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Specialization</label>
						<div class="controls">
						<input class="input-xxlarge focused" id="focusedInput" name="specialization" type="text" value="<?php echo $edit[0]['specialization'];?>">
						<span style="color: red"><?php echo form_error('specialization');?></span>
						</div>
					</div>
					<div class="control-group"></div>
					<div class="form-actions">
						<input type="hidden" name="eid" value="<?php echo $edit[0]['id']; ?>"/>
						<input type="submit" class="btn btn-primary" value="Save" />
						<?php if($edit[0]['id'] !='')
						{ ?>
							<input type="submit" class="btn btn-danger" value="cancel" onclick="cancel();" />
						<?php } ?>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span> List</h2>
				<div class="box-icon">
				<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
				</div>
				<div class="box-content">
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						<thead>
							<tr>
								<th>Srno</th>
								<th>Department</th>
								<th>Faculty Name</th>
								<th>Contact No</th>
								<th>Email Id</th>
								<th>Specialization</th>
								<th>Action</th>
							</tr>
						</thead>   
						<tbody>
						<?php
						$i=1;
						foreach($details as $de)
						{ ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td class="center"><?php echo $de['dpt_id']; ?></td>
								<td class="center"><?php echo $de['faculty_name']; ?></td>
								<td class="center"><?php echo $de['contact_no']; ?></td>
								<td><?php echo $de['email_id']; ?></td>
								<td class="center"><?php echo $de['specialization']; ?></td>
								
								<td>
								<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>Faculties/edit/<?php echo $de['id'];?>" style="cursor:pointer;">
								<i class="halflings-icon white edit"></i>  
								</a>
								<a class="btn btn-danger" onclick="delete_confirm(<?php echo $de['id'];?>,'<?php echo $de['image']; ?>')" style="cursor:pointer;">
								<i class="halflings-icon white trash"></i> 
								</a>
								</td>
							</tr>
						<?php $i++; } ?>	
						</tbody>
					</table>            
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Settings</h3>
	</div>
	<div class="modal-body">
		<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>
<?php include 'include/footer.php';?>
<script>
function delete_confirm(id,path)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		if(path !=''){
			window.location='<?php echo base_url();?>admin/About_us/delete_record/'+id+'/'+path;
		}
		else{
			window.location='<?php echo base_url();?>admin/About_us/delete/'+id;
		}
			
	}
}
function cancel()
{
	window.location='<?php echo base_url();?>admin/About_us/cancel_update';
}
$(function() {
	setTimeout(function() {
    $(".del").hide('blind', {}, 500)
	}, 5000);
});
</script>
 