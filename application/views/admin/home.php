<?php include 'include/header.php';?>
<div class="container-fluid-full">
	<div class="row-fluid">
		<?php include 'include/sidebar.php';?>
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		<div id="content" class="span10">
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="index.html">Home</a>
				<i class="icon-angle-right"></i> 
			</li>
			<li>
				<i class="icon-edit"></i>
				<a href="#">Manage Home Page</a>
			</li>
		</ul>
		<div class="row-fluid sortable">
			<div class="box span12">
				<div class="box-header" data-original-title>
					<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Content</h2>
					<div class="box-icon">
						<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
						<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
						<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
					</div>
				</div>
				<div class="box-content">
					<form class="form-horizontal"  method="post" action="<?php echo base_url();?>admin/Home/addcontent" enctype="multipart/form-data">
					<p style="color:green;" class="del"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete');echo $this->session->flashdata('require');?> </p>
					<fieldset>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Page Section</label>
						<div class="controls">
							<select name="home_page_section">
								<option value="">-Select-</option>
								<?php foreach($values as $sections) { ?>
								<option value="<?=$sections->id;?>" <?php if($edit[0]['page_section_id']==$sections->id){ echo "selected"; } ?>><?=$sections->values;?></option>
								<?php } ?>
							</select>
							<span style="color: red"><?php echo form_error('title');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">Title</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="home_page_title" type="text" value="<?php echo $edit[0]['title'];?>">
						<span style="color: red"><?php echo form_error('title');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="focusedInput">View Detail Link</label>
						<div class="controls">
						<input class="input-xlarge focused" id="focusedInput" name="view_detail_link" type="text" value="<?php echo $edit[0]['vew_detail_link'];?>">
						<span style="color: red"><?php echo form_error('detail_link');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label" for="textarea2">Description&nbsp;&nbsp;</label>
						<div class="controls">
							<textarea name="editor"><?php echo $edit[0]['dsecription']; ?></textarea>
						<script>
							CKEDITOR.replace( 'editor', {
								width: '150%',
								height: 200
							} );
						</script>
							<span style="color: red"><?php echo form_error('descrip');?></span>
						</div>
					</div>
					<div class="control-group col-md-6">
						<label class="control-label">Image</label>
						<div class="controls">
						<input class="input-file uniform_on" id="fileInput" name="image" type="file"> <?php echo isset($single_row) ? $single_row[0]['image'] : ""; ?>
						</div>
					</div>
					<div class="control-group"></div>
					<div class="form-actions">
						<input type="hidden" name="eid" value="<?php echo $edit[0]['id']; ?>"/>
						<input type="submit" class="btn btn-primary" value="Save" />
						<?php if($single_row[0]['id'] !='')
						{ ?>
							<input type="submit" class="btn btn-danger" value="cancel" onclick="cancel();" />
						<?php } ?>
					</div>
					</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="row-fluid sortable">		
			<div class="box span12">
				<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span> List</h2>
				<div class="box-icon">
				<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
				<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
				</div>
				<div class="box-content">
					<table class="table table-striped table-bordered bootstrap-datatable datatable">
						<thead>
							<tr>
								<th>Srno</th>
								<th>Images</th>
								<th>Title</th>
								<th>Description</th>
								<th>Link</th>
								<th>date</th>
								<th>Action</th>
							</tr>
						</thead>   
						<tbody>
						<?php
						$i=1;
						foreach($details as $de)
						{ ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td class="center"><?php if($de->image !=""){ ?><img src="<?php echo base_url('assets/uploads/home/'.$de->image); ?>"class="img-thumbnail" width="150" height="300"/><?php } ?></td>
								<td><?php echo $de->title; ?></td>
								<td class="center"><?php echo $de->dsecription; ?></td>
								<td><?php echo $de->vew_detail_link; ?></td>
								<td><?php echo $de->date_created; ?></td>
								<td>
								<a class="btn btn-info" href="<?php echo URL_ROOT_ADMIN;?>Home/edit/<?php echo $de->id;?>" style="cursor:pointer;">
								<i class="halflings-icon white edit"></i>  
								</a>
								<a class="btn btn-danger" onclick="delete_confirm(<?php echo $de->id;?>,'<?php echo $de->image; ?>')" style="cursor:pointer;">
								<i class="halflings-icon white trash"></i> 
								</a>
								</td>
							</tr>
						<?php $i++; } ?>	
						</tbody>
					</table>            
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="modal hide fade" id="myModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Settings</h3>
	</div>
	<div class="modal-body">
		<p>Here settings can be configured...</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div>
</div>
<?php include 'include/footer.php';?>
<script>
function set_status(status,id)
{
	var r=confirm("Do you really want to "+status+"?");	
	if(r==true)
	{
		window.location='<?php echo base_url();?>admin/Home/set_status/'+id+'/'+status;
	}
}
function delete_confirm(id,path)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		if(path !=''){
			window.location='<?php echo base_url();?>admin/Home/delete_record/'+id+'/'+path;
		}
		else{
			window.location='<?php echo base_url();?>admin/Home/delete/'+id;
		}
			
	}
}
function cancel()
{
	window.location='<?php echo base_url();?>admin/Home/cancel_update';
}
$(function() {
	setTimeout(function() {
    $(".del").hide('blind', {}, 500)
	}, 5000);
});
</script>
 