<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
	<div class="nav-collapse sidebar-nav">
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li><a href="<?php echo base_url();?>admin/dashboard"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a>
			</li>	
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manage Site</span></a>
			<ul>
			<li>
			<a class="submenu" href="<?php echo URL_ROOT_ADMIN?>Site_configuration"><i class="icon-file-alt"></i><span class="hidden-tablet">Site Configuration</span></a>
			</li>                       
			
			</ul>	
		</li>							
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage Menu</span></a>
			<ul>
			<li><a href="<?php echo URL_ROOT_ADMIN?>Add_menu"><i class="icon-tasks"></i><span class="hidden-tablet"> Add Menu</span></a></li>		   			
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">List of Values</span></a>
			<ul>
			<li><a href="<?php echo URL_ROOT_ADMIN?>Values_List"><i class="icon-tasks"></i><span class="hidden-tablet">Values List</span></a></li>		   			
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage Home</span></a>
			<ul>
			<li><a href="<?php echo URL_ROOT_ADMIN?>Home"><i class="icon-tasks"></i><span class="hidden-tablet">Home Page</span></a></li>		   			
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage About Us</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>About_us"><i class="icon-tasks"></i><span class="hidden-tablet">About us</span></a>
				</li>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Committees"><i class="icon-tasks">
				</i><span class="hidden-tablet">Committees</span></a>
				</li>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Faculties"><i class="icon-tasks">
				</i><span class="hidden-tablet">Faculties</span></a>
				</li>		   			
			</ul>	
		</li>
		
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage School</span></a>
			<ul>
				<li>
					<a href="<?php echo URL_ROOT_ADMIN?>Department"><i class="icon-tasks"></i><span class="hidden-tablet">Dpt Of School</span></a>
				</li>
				<li>
					<a href="<?php echo URL_ROOT_ADMIN?>Academic_Calendar"><i class="icon-tasks"></i><span class="hidden-tablet">Academic Calendar</span></a>
				</li>
				<li>
					<a href="<?php echo URL_ROOT_ADMIN?>Calendar_Headings"><i class="icon-tasks"></i><span class="hidden-tablet">Calendar Headings</span></a>
				</li>		   			
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage Admissions</span></a>
			<ul>
				<li>
					<a href="<?php echo URL_ROOT_ADMIN?>Admission_Process"><i class="icon-tasks"></i><span class="hidden-tablet">Admission Process</span></a>
				</li>
				<li>
					<a href="<?php echo URL_ROOT_ADMIN?>Vedios"><i class="icon-tasks"></i><span class="hidden-tablet">Vedios</span></a>
				</li>		   			
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage Research</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Research"><i class="icon-tasks"></i><span class="hidden-tablet">Research</span></a>
			</li>
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Campus Facilities</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Facility"><i class="icon-tasks"></i><span class="hidden-tablet">Facility</span></a>
			</li>
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Placements & Internships</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Placements_And_Internships"><i class="icon-tasks"></i><span class="hidden-tablet">Placements And Internships</span></a>
			</li>
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Sports & Cultural</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Sports_Cultural"><i class="icon-tasks"></i><span class="hidden-tablet">Sports Cultural</span></a>
				</li>
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet">Manage Banners</span></a>
			<ul>
				<li>
				<a href="<?php echo URL_ROOT_ADMIN?>Banner_Master"><i class="icon-tasks"></i><span class="hidden-tablet">Add Banners</span></a>
			</li>
			</ul>	
		</li>
		<li>
			<a class="dropmenu" href="#"><i class="icon-folder-close-alt">
			</i><span class="hidden-tablet">Manage Contact</span></a>
			<ul>
				<li><a href="<?php echo URL_ROOT_ADMIN?>Contact_Us"><i class="icon-tasks"></i><span class="hidden-tablet">Contact Us</span></a>
				</li>
			</ul>	
		</li>
	</li>
	</ul>
	</div>
</div>