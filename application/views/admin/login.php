<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Admin Panel</title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/admin/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?php echo base_url(); ?>assets/admin/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
		<style type="text/css">
			body { background: url(<?php echo base_url(); ?>assets/admin/img/bg-login.jpg) !important; }
		</style>
</head>
<body>
		<div class="container-fluid-full">
		<div class="row-fluid">		
			<div class="row-fluid">
				<div class="login-box">
					<h2>Login to your account</h2>
					<hr style="border:1px solid #c7c3c3;height: 0px; ">
					<p style="color:red;"> <?php echo $this->session->flashdata('loginerror');  ?></p>
					<form class="form-horizontal" action="<?php echo base_url()?>admin/login/admin_login" method="post">
						<fieldset>
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="username" id="username" type="text" placeholder="type username"/>
							</div>
							<div class="clearfix"></div>
							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
							<div class="button-login">	
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
					</form>
				</div><!--/span-->
			</div><!--/row-->
	</div><!--/.fluid-container-->
		</div><!--/fluid-row-->
	<!-- start: JavaScript-->
		<script src="<?php echo base_url(); ?>assets/admin/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery-migrate-1.0.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/jquery-ui-1.10.0.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js"></script>
	<!-- end: JavaScript-->
</body>
</html>