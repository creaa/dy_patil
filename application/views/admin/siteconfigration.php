<?php include 'include/header.php';?>

<div class="container-fluid-full">
<div class="row-fluid">

<!-- start: Main Menu -->
<?php include 'include/sidebar.php';?>
<!-- end: Main Menu -->

<noscript>
<div class="alert alert-block span10">
<h4 class="alert-heading">Warning!</h4>
<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>

<!-- start: Content -->
<div id="content" class="span10">


<ul class="breadcrumb">
<li>
<i class="icon-home"></i>
<a href="index.html">Home</a>
<i class="icon-angle-right"></i> 
</li>
<li>
<i class="icon-edit"></i>
<a href="#">Add Site Configuration</a>
</li>
</ul>

<p style="color:red;"> <?php echo $this->session->flashdata('submit'); echo $this->session->flashdata('update');  echo $this->session->flashdata('delete'); echo $this->session->flashdata('setstatus');?></p>
<!--/row-->
<form class="form-horizontal group-border stripped" action="<?php echo base_url()?>admin/Site_configuration/add_site_configuration" method="post" enctype="multipart/form-data">
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Contact Details</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">

<fieldset>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Email</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="email" type="text" value="<?php echo set_value('email'); echo $site_details['email']; ?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Contact No.&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" type="text" name="contact" value="<?php echo set_value('contact'); echo $site_details['contact_no']?>">
</div>
</div>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Address</label>
<div class="controls">
<input class="input-xlarge focused" type="text" name="address" value="<?php echo set_value('address'); echo $site_details['address'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Copyright&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" type="text" name="copy" value="<?php echo set_value('copy'); echo $site_details['copyright'];?>">
</div>
</div>


</fieldset>


</div>
</div><!--/span-->

</div>
<!--/row-->
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Add Social Link</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">

<fieldset>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Facebook</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" type="text" name="facebook" value="<?php echo set_value('facebook'); echo $site_details['facebook'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Twitter&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" type="text" name="twitter" value="<?php echo $site_details['twitter'];?>">
</div>
</div>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Google Plus</label>
<div class="controls">
<input class="input-xlarge focused" type="text" name="google" value="<?php echo $site_details['google'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Linkedin&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused"  type="text" name="linkedin" value="<?php echo $site_details['linkedin'];?>">
</div>
</div>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">You Tube</label>
<div class="controls">
<input class="input-xlarge focused" type="text" name="youtube" value="<?php echo $site_details['youtube'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Instragram&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" type="text" name="copy" value="<?php echo set_value('copy'); echo $site_details['copyright'];?>">
</div>
</div>
</fieldset>


</div>
</div><!--/span-->

</div>
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Upload Logo And Favicon Icon</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">

<fieldset>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Upload Logo</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="logo_img" type="file" value="<?php echo set_value('logo'); echo $site_details['logo']; ?>">
<?php 
if($site_details['logo']!='')
{
?>
<img src="<?php echo base_url();?>assets/upload/logo/<?php echo $site_details['logo']; ?>" width="80" height="80">
<?php }?>
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Favicon Icon.&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" type="file" name="favicon_img" value="<?php echo set_value('favicon'); echo $site_details['favicon']?>">
<?php 
if($site_details['favicon']!='')
{
?>
<img src="<?php echo base_url();?>assets/upload/logo/<?php echo $site_details['favicon']; ?>" width="80" height="80">
<?php }?>
</div>
</div>

</fieldset>


</div>
</div><!--/span-->

</div>
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>For SEO</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">

<fieldset>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Add Site Title</label>
<div class="controls">
<input type="text" class="input-xlarge focused" id="focusedInput" name="title" value="<?php echo set_value('title'); echo $site_details['site_title'];?>">
</div>
</div>
<div class="control-group">

</div>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Meta Keyword</label>
<div class="controls">
<textarea class="input-xlarge focused" name="keyword" rows="3"><?php echo $site_details['keyword'];?></textarea>
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Meta Description&nbsp;&nbsp;</label>
<div class="controls">
<textarea class="input-xlarge focused" name="description" rows="3"><?php echo $site_details['description'];?></textarea>
</div>
</div>


</fieldset>


</div>
</div><!--/span-->

</div>

<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Manage Footer</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<div class="box-content">

<fieldset>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Latitude</label>
<div class="controls">
<input type="text" class="input-xlarge focused" id="focusedInput" name="lati" value="<?php echo set_value('lati'); echo $site_details['latitude'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Longitude</label>
<div class="controls">
<input type="text" class="input-xlarge focused" id="focusedInput" name="long" value="<?php echo set_value('long'); echo $site_details['longitude'];?>">
</div>
</div>

<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Title</label>
<div class="controls">
<input type="text" class="input-xlarge focused" id="focusedInput" name="footer_title" value="<?php echo set_value('footer_title'); echo $site_details['footer_title'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Description</label>
<div class="controls">
<textarea class="input-xlarge focused" name="footer_description" rows="3"><?php echo $site_details['footer_description'];?></textarea>
</div>
</div>


</fieldset>
<div class="form-actions">
<input type="text" name="eid" value="<?php echo $site_details['id'] ?>">
<input type="submit" class="btn btn-primary" value="Save" />

</div>

</div>
</div><!--/span-->

</div>
</form>
<!--/row-->


<!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->

<div class="modal hide fade" id="myModal">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h3>Settings</h3>
</div>
<div class="modal-body">
<p>Here settings can be configured...</p>
</div>
<div class="modal-footer">
<a href="#" class="btn" data-dismiss="modal">Close</a>
<a href="#" class="btn btn-primary">Save changes</a>
</div>
</div>

<?php include 'include/footer.php'?>

 