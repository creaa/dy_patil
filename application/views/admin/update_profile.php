<?php include 'include/header.php';?>
<div class="container-fluid-full">
<div class="row-fluid">
<!-- start: Main Menu -->
<?php include 'include/sidebar.php';?>
<!-- end: Main Menu -->
<noscript>
<div class="alert alert-block span10">
<h4 class="alert-heading">Warning!</h4>
<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>
<!-- start: Content -->
<div id="content" class="span10">
<ul class="breadcrumb">
<li>
<i class="icon-home"></i>
<a href="index.html">Home</a>
<i class="icon-angle-right"></i> 
</li>
<li>
<i class="icon-edit"></i>
<a href="#">Update Profile</a>
</li>
</ul>
<!--/row-->
<div class="row-fluid sortable">
<div class="box span12">
<div class="box-header" data-original-title>
<h2><i class="halflings-icon edit"></i><span class="break"></span>Update Profile</h2>
<div class="box-icon">
<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
</div>
</div>
<p style="color:red;"> <?php echo $this->session->flashdata('submit');?></p>
<div class="box-content">
<form class="form-horizontal"  method="post" action="<?php echo URL_ROOT_ADMIN?>profile/update_pro" enctype="multipart/form-data">
<fieldset>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">First Name :</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="fname" type="text" value="<?php echo $user['fname']; ?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Last Name : &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="lname" type="text" value="<?php echo $user['lname']; ?>" required>
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">User Name :</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="username" type="text" value="<?php echo $user['username']; ?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Email address : &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="email" type="text" value="<?php echo $user['email']; ?>">
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Contact Number:</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="mobile" type="text" value="<?php echo $user['contact_no']; ?>" required>
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Address: &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="address" type="text" value="<?php echo $user['address']; ?>" required>
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">City:</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="city" type="text" value="<?php echo $user['city'];?>" />
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">State: &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="state" type="text" value="<?php echo $user['state'];?>">
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Zip:</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="zip" type="text" value="<?php echo $user['zip'];?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Country: &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input class="input-xlarge focused" id="focusedInput" name="country" type="text" value="<?php echo $user['country'];?>" >
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">About me:</label>
<div class="controls">
<textarea class="form-control" style="height:100px;" name="discription"><?php echo $user['description'];?></textarea>
</div>
</div>
<div class="control-group">
<label class="control-label" for="focusedInput">Profile Image: &nbsp;&nbsp;&nbsp;</label>
<div class="controls">
<input type="file" class="form-control" name="image" value="<?php echo $user['profile_pic'];?>">
</div>
</div>
<div class="control-group col-md-6">
<label class="control-label" for="focusedInput">Banner Image:</label>
<div class="controls">
 <input type="file" class="form-control" name="benner_image"  value="<?php echo $user['banner_pic'];?>">
</div>
</div>
<div class="control-group">
</div>
<div class="form-actions">
<input type="submit" class="btn btn-primary" value="Save" />
</div>
</fieldset>
</form>
</div>
</div><!--/span-->
</div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->
</div><!--/#content.span10-->
</div><!--/fluid-row-->
<div class="modal hide fade" id="myModal">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">×</button>
<h3>Settings</h3>
</div>
<div class="modal-body">
<p>Here settings can be configured...</p>
</div>
<div class="modal-footer">
<a href="#" class="btn" data-dismiss="modal">Close</a>
<a href="#" class="btn btn-primary">Save changes</a>
</div>
</div>
<?php include 'include/footer.php'?>
<script>
function delete_confirm(id)
{
	var r=confirm("Do you really want to delete ?");	
	if(r==true)
	{ 
		window.location='<?php echo base_url();?>admin/page/delete/'+id;
	}
}
// end code
</script>