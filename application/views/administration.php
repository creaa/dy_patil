<div class="banner">
<img src="<?php echo base_url('assets/uploads/banners/'.$banners[0]['image']);?>" class="img-responsive" alt="">
</div>
<div class="main univer_section_main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="univer_section1">
				<h2><?=$detail[0]['title'];?> </h2>
				<?=$detail[0]['description'];?>
			</div>
			</div>
		</div>
	</div>
	<div class="univer_section2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="univer_section2_left">
					<?=$detail[1]['description'];?>
					<img src="<?php echo base_url('assets/uploads/about_us/'.$detail[1]['image'])?>" class="img-responsive" alt="">
				</div>
				</div>
			</div>
		</div>
	</div>
</div>