<div class="banner">
	<img src="<?php echo base_url('assets/images/admission/admission_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
				<?php 
					$cnt=1;
					foreach($adm as $process){ 
					if($process['document'] !="" || $cnt==1){ ?>
					<div class="core_values admission_process_section1">
						<h2><?=$process['title'];?></h2>	
						<a href="<?php echo base_url('assets/uploads/admission/'.$process['document']);?>" target="_blank">Download Brochure</a>
						<?=$process['description'];?>
					</div>
					<?php $cnt++; } } ?>
				</div>
			</div>
		</div>	
	</div>
	<div class="admission_process_about_section2">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
				<?php
					$cnt=1;
					foreach($adm as $process){ 
					if($process['document']==""){ ?>
					<div class="about_sec2">
						<h4>STEP <?=$cnt;?>: <?=$process['title'];?></h4>
						<?=$process['description'];?>
					</div>
					<?php $cnt++; } } ?>
				</div>
			</div>
		</div>
	</div>
</div>