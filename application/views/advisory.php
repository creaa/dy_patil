<div class="banner">
	<img src="<?php echo base_url('assets/uploads/banners/'.$banners[0]['image']);?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 advisory">
		<div class="container">
			<h2>Advisory Board</h2>
			<p>At D Y PATIL  University, we believe in constantly striving to stretch our horizons. To this end, we are ably guided by our Advisory Boards.</p>
			<?php foreach($detail as $values){ ?>
			<div class="row advisory_pad">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="advisory_left match">
						<h4><?=$values['title']?></h4>
							<?=$values['description']?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="advisory_right match">
						<img src="<?php echo base_url('assets/uploads/about_us/'.$values['image']);?>" class="img-responsive" alt="">	
					</div>
				</div>
			</div>
			<?php } ?>
		</div>	
	</div>
</div>