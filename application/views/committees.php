<div class="banner">
<img src="<?php echo base_url('assets/images/about/committees_banner.png');?>" class="img-responsive" alt="">
</div>
<div class="main univer_section_main">
	<div class="container">
		<div class="row">
			<div class="univer_section1">
				<h2><?=$heading['title'];?></h2>
				<?=$heading['description'];?>
				<div class="table-responsive">          
					<table class="table">
						<thead>
							<tr>
								<th>Sr. No</th>
								<th>Name of the Committee</th>
								<th>No. of Members</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$cnt=1;
							foreach($committee as $names){ ?>
								<tr>
									<td><?=$cnt;?></td>
									<td><?=$names['committee_name'];?></td>
									<td><?=$names['no_of_members'];?></td>
								</tr>
							<?php $cnt++; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>