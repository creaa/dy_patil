<div class="banner">
<img src="<?php echo base_url('assets/images/contact/contact_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="contact_section1">
<div class="container4">
<h2>Contact Us</h2>
<p>We’re happy to receive your message.</p>
<div class="row">
<div class="col-xs-12">
<div class="form_section">
	<form id="cform">
		<div class="all_form">
		<div class="form_left">
			<div class="form-group">
				<label>Name:</label>
              <input name="name" id="name"  type="text" placeholder="Name" class="form-control" required>
            </div>

            <div class="form-group">
				<label>Contact No:</label>
              <input type="text" name="mobileNo" placeholder="Contact No."  id="phone"  pattern="[789][0-9]{9}" class="form-control" required>
            </div>

            <div class="form-group">
				<label>Email Address:</label>
              <input name="email" id="email"  type="* Email" placeholder="Email" class="form-control" required>
            </div>
		</div>
		<div class="form_right">
		<div class="form-group">
		<label>Message Box :</label>
		<textarea  placeholder="" id="message" name="description" class="form-control"></textarea> 
	    </div>
		</div>
		</div>
		 <div class="form-group">
             <button type="submit" value="Submit" id="submit">SEND MESSAGE</button>
            </div>
	</form>
	
</div>
</div>
</div>	
</div>
</div>

<div class="contact_section2">
	<div class="container4">
		<div class="row">
			<div class="col-xs-12">
				<div class="address_sec_bottom">
					<h2>Address & Directions</h2>
					<span>D Y Patil University - Pune</span>
					<p>Sr. No. 124 & 126, A/p Ambi, MIDC Road, Tal Maval, Talegaon Dabhade, Maharashtra 410506</p>
					<ul>
						<li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:8448444827">+91-8448444827, 9730255629</a></li>
						<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:info@dypatiluniversity.com">info@dypatiluniversity.com</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>