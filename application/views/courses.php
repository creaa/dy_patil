<?php include_once"header.php"?>

<!-- banner section start -->
<div class="banner">
<img src="images/admission/courses_banner.jpg" class="img-responsive" alt="">
</div>
<!-- banner section end -->

<!-- main section start -->
<div class="main">
<div class="coures_section1">
	<div class="container">
		<h2>List of courses offered by our university </h2>
		<div class="row">
			<div class="col-xs-12">
				<div class="coures_sec_top">
					<div class="table-responsive">          
  <table class="table">
    <thead>
    	<tr>
        <th>PROGRAM BY DISCIPLINE </th>
        <th>LEVEL</th>
        <th>DURATION</th>
        <th>ELIGIBILITY</th>
        <th>XII %</th>
        <th>QUALIFYING CRITERIA Qualifying Exam</th>
        <th>FEES PER YEAR</th>
      </tr>
      <tr>
        <th colspan="3" style="background: #9f1c33; border:0; text-align: center;">SCHOOL OF ENGINEERING </th>
      </tr>
      <tr>
        <th>Commencement & End of Term</th>
        <th>First Term </th>
        <th>First Term  </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Orientation for First Years</td>
        <td>3rd July 2020</td>
        <td>6th Jan 2021 to  30th May 2021</td>
      </tr>

      <tr>
        <td>Engineering Sem-I</td>
        <td>7th July 2020  to 7th Dec 2020</td>
        <td>6th Jan 2021 to  30th May 2021</td>
      </tr>

      <tr>
        <td>1st Class  Test</td>
        <td>16th Aug  2020 to 25th Aug. 2020</td>
        <td>3rd  Feb 2021 to 07th Feb 2021</td>
      </tr>

      <tr>
        <td>1st Parent teachers meeting</td>
        <td>30th Aug.2020</td>
        <td>15th Feb. 2021</td>
      </tr>

      <tr>
        <td>2nd Class Test</td>
        <td>6th Oct 2020 to 13th Oct 2020</td>
        <td>9th  Mar.2021 to 13th Mar.2021</td>
      </tr>

      <tr>
        <td>2nd Parent teachers meeting</td>
        <td>31st  Oct 2020</td>
        <td>21st March. 2021</td>
      </tr>

      <tr>
        <td>Term End Exam</td>
        <td>11th Nov 2021 to 7th Dec 2021</td>
        <td>4th May2021 to 30th May 2021</td>
      </tr>

    </tbody>
  </table>
  </div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- main section end -->

 <?php include_once"footer.php"?> 