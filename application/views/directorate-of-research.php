<div class="banner">
<img src="<?php echo base_url('assets/images/research/research_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 architecture_section1">
		<div class="container4">
			<h2>DIRECTORATE OF RESEARCH</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="architecture_left">
						<?php foreach($dir as $txt){ ?>
						<div class="architecture_bottom">
						<?php
							echo $txt['description'];?>
						</div>
						<?php } ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="architecture_right">
						<img src="<?php echo base_url('assets/uploads/research/'.$dir[0]['image']);?>" class="img-responsive" alt="">
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>