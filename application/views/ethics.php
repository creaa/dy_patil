<div class="banner">
	<img src="<?php echo base_url('assets/uploads/banners/'.$banners[0]['image']);?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="core_values">
						<h2><?php $heading['title'];?></h2>	
							<?php $heading['description'];?>
					</div>
					<div class="ethics_accor">
						<div class="panel-group" id="accordion">
							<?php $cnt=1;
							foreach($accordians as $val) { ?>
							<div class="panel panel-default">
							  <div class="panel-heading">
								<h4 data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cnt;?>" class="panel-title expand collapsed">
								   <div class="right-arrow pull-right">+</div>
								  <a href="#"><?=$val['title'];?></a>
								</h4>
							  </div>
							  <div id="collapse<?=$cnt;?>" class="panel-collapse collapse <?=(($cnt==1)?"":"in");?>in">
								<div class="panel-body">
									<div class="accor_bottom">
										<?=$val['description'];?>
									</div>
								</div>
							  </div>
							</div>
							<?php $cnt++; } ?>
						</div>
					</div> 
				</div>
			</div>	
		</div>
	</div>
</div>