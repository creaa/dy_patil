<div class="banner">
<img src="<?php echo base_url('assets/images/campus/247-library_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="recruiters_section1 transportation_section1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
				<div class="transportation_sec_bottom">
				<h2>Faculty Details</h2>
				<div class="table-responsive tab_sec">
				<?php foreach($dpt as $dptname){?>
				<table class="table">
				<thead>
					<tr>
						<th colspan="5" style="background: #9f1c33; color: #fff; border:0; text-align: center; border: 1px solid #636363;">Faculty Details For <?=$name[$dptname];?>       
						</th>
					</tr>
					<tr>
						<th style="background: #6d6e71; color: #fff; vertical-align: middle;">Sr No</th>
						<th style="background: #6d6e71; color: #fff; vertical-align: middle;">Faculty Name</th>
						<th style="background: #6d6e71; color: #fff; vertical-align: middle;">contact No</th>
						<th style="background: #6d6e71; color: #fff; vertical-align: middle;">email Id</th>
						<th style="background: #6d6e71; color: #fff; vertical-align: middle;">Specialization</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$cnt=1;
					foreach($fac[$dptname] as $key=>$val){ ?>
						<tr>
							<td><?=$cnt;?></td>
							<td><?=$val['faculty_name'];?></td>
							<td><?=$val['contact_no'];?></td>
							<td><?=$val['email_id'];?></td>
							<td><?=$val['specialization'];?></td>
						</tr>
					<?php $cnt++; } ?>
				</tbody>
				</table>
				<?php } ?>
				</div>	
				</div>
				</div>
			</div>
		</div>
	</div>
</div>