<?php include_once"header.php"?>

<!-- banner section start -->
<div class="banner">
<img src="images/campus/gym_banner.jpg" class="img-responsive" alt="">
</div>
<!-- banner section end -->

<!-- main section start -->
<div class="main">
<div class="recruiters_section1 transportation_section1">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="transportation_sec_bottom">
					<h2>Gymnasium</h2>
					<p>The campus houses a fully equipped workout area and yoga zone where students can exercise and work towards building a healthy body and mind. Meditation and yoga sessions help the students to tackle academic pressure and infuse a sense of optimism in their life. The gym is fully equipped with advanced workout machines and the trainers monitor students as well as motivate them to exercise properly and regularly. </p>
					<img src="images/campus/gym_banner_img1.jpg" class="img-responsive" alt="">
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- main section end -->

 <?php include_once"footer.php"?> 