<div class="banner">
<img src="<?php echo base_url('assets/images/campus/gym_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="recruiters_section1 transportation_section1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php foreach($fac as $gym){ ?>
					<div class="transportation_sec_bottom">
						<h2><?=$gym['title']?></h2>
						<?=$gym['description']?>
					</div>
					<?php } ?>
				<div class="trans_slider">
				<?php foreach($fac as $gymimg){
					if($gymimg['image'] !=""){ ?>
					<div>
						<img src="<?php echo base_url('assets/uploads/facility/'.$gymimg['image']);?>" class="img-responsive" alt="">
					</div>
				<?php } } ?>
				</div>
				</div>
			</div>
		</div>
	</div>
</div> 