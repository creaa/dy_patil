	<div class="banner">
      <div class="banner_slider">
        <div class="banner_section">
          <a href="#"><img src="<?php echo base_url('assets/images/banner.png');?>" class="img-responsive" alt=""></a>
          <div class="banner_text">
            <ul>
              <li><a href="#">Admission Process</a></li>
              <li><a href="#">Download Brochure</a></li>
              <li><a href="#">Register Here</a></li>
            </ul>
          </div>
        </div>
        <div class="banner_section">
          <a href="#"><img src="<?php echo base_url('assets/images/banner.png');?>" class="img-responsive" alt=""></a>
           <div class="banner_text">
            <ul>
              <li><a href="#">Admission Process</a></li>
              <li><a href="#">Download Brochure</a></li>
              <li><a href="#">Register Here</a></li>
            </ul>
          </div>
        </div>
        <div class="banner_section">
          <a href="#"><img src="<?php echo base_url('assets/images/banner.png');?>" class="img-responsive" alt=""></a>
           <div class="banner_text">
            <ul>
              <li><a href="#">Admission Process</a></li>
              <li><a href="#">Download Brochure</a></li>
              <li><a href="#">Register Here</a></li>
            </ul>
          </div>
        </div>
        
      </div>
    </div>
    <div class="main">
      <div class="home_section1">
        <img src="<?php echo base_url('assets/images/main_sec_img.png');?>" class="img-responsive" alt="">
        <div class="para_section">
          <img src="<?php echo base_url('assets/images/para_Sec_img1.png');?>" class="img-responsive" alt="">
        </div>
      </div>
       <div class="home_section2">
        <div class="container-fluid padd">
          <h2>What makes DYPU Pune Unique and Special ?</h2>
         <div class="row mar">
			<?php $no=count($dypu);
			$cnt=1;
			$clas="";
			foreach($dypu as $va){
				if($cnt==3 || $cnt==4){
					$clas="special_flow2";
				}
				 ?>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="ccol-xs-12 col-sm-6 col-md-6 col-lg-6 padd <?=$clas;?>">
					<div class="special">
						<img src="<?php echo base_url('assets/uploads/home/'.$va->image);?>" class="img-responsive" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special special2">
						<h4><?=$va->title;?></h4>
						<?php echo $va->dsecription;?>
						<a href="<?php echo $va->vew_detail_link;?>">View Details...</a>
					</div>
				</div>
			</div>
			<?php $cnt++; } ?>
			<!--<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd special_flow">
					<div class="special">
					<img src="<?php echo base_url('assets/images/special/img2.png');?>" class="img-responsive" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special special2">
					<h4>Student Friendly Campus</h4>
					<p>Easy Bank Loans, Hostel Accommodation, In House Cafe's, Sports Centre</p>
					<a href="#">View Details...</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd special_flow2 special_flow3">
					<div class="special">
					<img src="<?php echo base_url('assets/images/special/img3.png');?>" class="img-responsive" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special special2">
						<h4>Teaching Methodology</h4>
						<p>Industry relevant student learning 
						material including e-books, 
						videos, online lectures, and 
						research papers.</p>
						<a href="#">View Details...</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd special_flow2">
					<div class="special">
						<img src="<?php echo base_url('assets/images/special/img4.png');?>" class="img-responsive" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special special2">
						<h4>Professional Linkages</h4>
						<p>Associated with top corporates 
						including Indigo Airlines, American
						Express, Cipla, Emerson, Bajaj, 
						Jet Airways, Vodafone, IBM, 
						Genpact, Taj, Oberoi’s, Marriott, 
						Radisson, Hyatt, Leela, ITC.</p>
						<a href="#">View Details...</a>
					</div>
				</div>
            </div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special">
						<img src="<?php echo base_url('assets/images/special/img5.png');?>" class="img-responsive" alt="">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd">
					<div class="special special2">
						<h4>Examination Protocol </h4>
						<p>All the information regarding the examination process for the programs at DYPU.</p>
						<a href="#">View Details...</a>
					</div>
				</div>
			</div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padd">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd special_flow">
					<div class="special">
						<img src="<?php echo base_url('assets/images/special/img6.png');?>" class="img-responsive" alt="">
					</div>
			   </div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 padd ">
					<div class="special special2">
						<h4>ASK US</h4>
						<p>Constant Academic Support from University Counsellors, Contact us via Calls, Emails, Facebook, etc.</p>
						<a href="#">View Details...</a>
					</div>
				</div>
			</div>-->
        </div>
        </div>
      </div>
      <div class="home_Section3">
        <h2>Programs Offered</h2>
        <div class="program_sec_top">
        <ul>
			<?php foreach($Programs_Offered as $programs){ ?>
			<li>
				<div class="program_sec match">
				<img src="<?php echo base_url('assets/uploads/home/'.$programs->image);?>" class="img-responsive" alt="">
				<a href="#"><?=$programs->title;?></a>
				</div>
			</li>
			<?php } ?>
        </ul>
        </div>
      </div>
      <div class="home_Section4">
        <h2>Testimonials</h2>
        <div class="testi_slider">
			<?php foreach($testimonials as $test){ ?>
			<div class="testi_section">
				<img src="<?php echo base_url('assets/uploads/home/'.$test->image);?>" class="img-responsive" alt="">
				<h3><?=$test->title;?></h3>
				<?=$test->dsecription;?>
			</div>
			<?php } ?>
        </div>
      </div>
    </div>