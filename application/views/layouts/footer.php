<!-- footer start -->
    <footer>
     <div class="container">
       <div class="row">
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           <div class="footer_sec">
            <h4>PROGRAMS</h4>
             <ul>
               <li><a href="#">Courses</a></li>
<li><a href="#">Teaching Methodology</a></li>
<li><a href="#">Examination Protocol</a></li>
<li><a href="#">Get in Touch</a></li>
<li><a href="#">Industry Association</a></li>
             </ul>
           </div>
         </div>

         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           <div class="footer_sec">
            <h4>ADMISSIONS</h4>
             <ul>
               <li><a href="#">Admission Process</a></li>
<li><a href="#">Register Now</a></li>
<li><a href="#">Placement Opportunities</a></li>
<li><a href="#">Download Brochure</a></li>
<li><a href="#">Ask us</a></li>
             </ul>
           </div>
         </div>

         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           <div class="footer_sec">
            <h4>DYPU</h4>
             <ul>
               <li><a href="#">About DYPU</a></li>
<li><a href="#">Academic Team</a></li>
<li><a href="#">Information Partner's</a></li>
             </ul>
           </div>
         </div>

         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
           <div class="footer_sec">
            <h4>FOLLOW US</h4>
            <ul class="social_link">
            <li><a href="#"><img src="<?php echo base_url('assets/images/social/face.png');?>" class="img-responsive"></a></li>
            <li><a href="#"><img src="<?php echo base_url('assets/images/social/insta.png');?>" class="img-responsive"></a></li>
            <li><a href="#"><img src="<?php echo base_url('assets/images/social/link.png');?>" class="img-responsive"></a></li>
            <li><a href="#"><img src="<?php echo base_url('assets/images/social/twi.png');?>" class="img-responsive"></a></li>
          </ul>
           </div>
         </div>
       </div>
     </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.cookcodesmenu.js');?>"></script>
    <script src="<?php echo base_url('assets/js/slick.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.matchHeight-min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.fancybox.pack.js')?>"></script>
     <script>
     $(function() {
        $('#menu').cookcodesmenu({
            brand: 'jQueryScript'
        });
    });
    
	$(document).ready(function() {
			$(".fancybox").fancybox({
			openEffect  : 'fade',
			closeEffect : 'fade'
		});
			$('#bs-carousel').carousel
		({
		pause: false,
		slideSpeed: 80000,
		interval:3000,
		});
	});
    </script>
  </body>
</html>