<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900&display=swap" rel="stylesheet">
    <title>D.Y.Patil</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/slick.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/slick-theme.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/cookcodesmenu.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/jquery.fancybox.css');?>" rel="stylesheet" />
  </head>
  <body>
    <!-- header section start  -->
    <header>
      <div class="header_inner">
      <div class="header_top">
          <div class="header_top_left">
            <ul>
              <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+918448444230">+918448444230</a></li>
               <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:admissions@dypatiluniversity.edu">admissions@dypatiluniversity.edu</a></li>
            </ul>
        </div>
        <div class="header_top_right">
          <ul>
            <li><a href="#">MY D Y</a></li>
            <li><a href="#">Email</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Virtual Tour</a></li>
             <li><a href="#">IQAC</a></li>
             <li><a href="#">Feedback</a></li>
          </ul>
          <ul class="social">
            <li><a href="#">
			<img src="<?php echo base_url('assets/images/social/face.png');?>" class="img-responsive"></a></li>
            <li><a href="#">
			<img src="<?php echo base_url('assets/images/social/insta.png');?>" class="img-responsive"></a></li>
            <li><a href="#">
			<img src="<?php echo base_url('assets/images/social/link.png');?>" class="img-responsive"></a></li>
            <li><a href="#">
			<img src="<?php echo base_url('assets/images/social/twi.png');?>" class="img-responsive"></a></li>
          </ul>
        </div>
      </div>
      <div class="header_middle">
        <div class="logo">
          <a href="<?php echo base_url();?>"><img src="<?php echo base_url('assets/images/logo.png');?>" class="img-responsive"></a>
        </div>
        <div class="search_head_sec">
          <form>
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
        </div>
      </div>
      </div>
	<nav class="mobile-menu">
	<ul>
	<li class="dropdown">
		<a href="#" id="about"  class="dropdown-toggle current" data-toggle="dropdown">ABOUT US<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('about-university');?>">University</a></li>
			<li><a href="<?php echo base_url('founder');?>">Founder President</a></li>
			<li><a href="<?php echo base_url('president');?>">President</a></li>
			<li><a href="<?php echo base_url('managing');?>">Managing Trustee</a></li>
			<li><a href="<?php echo base_url('vice-chancellor');?>">Vice Chancellor</a></li>
			<li><a href="<?php echo base_url('registrar');?>">Registrar of the University</a></li>
			<li><a href="<?php echo base_url('controller-of-examination');?>">Controller of examination</a></li>
			<li><a href="<?php echo base_url('finance-and-accounts-officer');?>">Chief Finance & Accounts Officer</a></li>
			<li><a href="<?php echo base_url('faculties');?>">Faculties </a></li>
			<li><a href="<?php echo base_url('vision-mision');?>">Vision, Mission, Goals</a></li>
			<li><a href="<?php echo base_url('core-values');?>">Core Value</a></li>
			<li><a href="<?php echo base_url('ethics');?>">Ethics</a></li>
			<li><a href="<?php echo base_url('ranking');?>">Affiliations, Rankings & Awards</a></li>
			<li><a href="<?php echo base_url('advisory');?>">Advisory Board</a></li>
			<li><a href="<?php echo base_url('administration');?>">Administration</a></li>
			<li><a href="<?php echo base_url('university-regulations');?>">University Regulations</a></li>
			<li><a href="<?php echo base_url('committees');?>">Committees</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">SCHOOLS<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">School of Engineering <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url('civil-infrastructural-engineering');?>">Department of Civil & <br>Infrastructural Engineering </a></li>
					<li><a href="<?php echo base_url('electrical-electronics-engineering')?>">Department of  Electrical & <br>Electronics Engineering </a></li>
					<li><a href="<?php echo base_url('mechanical-engineering');?>">Department of Mechanical <br>Engineering  </a></li>
					<li><a href="<?php echo base_url('information-technology-engineering');?>">Department of Information <br>Technology Engineering </a></li>
					<li><a href="<?php echo base_url('computer-information');?>">Department of Computer & <br>Information Science Engineering </a>
					</li>
					<li>
					<a href="<?php echo base_url('automobile-engineering');?>">Department of Automobile <br>Engineering</a>
					</li>
				</ul>
			</li>
			<li><a href="<?php echo base_url('school-of-architecture');?>">School of Architecture</a></li>
			<li><a href="<?php echo base_url('school-of-pharmacy');?>">School of Pharmacy</a></li>
			<li>
				<a href="<?php echo base_url('school-of-business-management');?>">School of Business Management</a>
			</li>
			<li>
				<a href="<?php echo base_url('school-of-hospitality-tourism-studies');?>">School of Hospitality & <br>Tourism Studies</a>
			</li>
			<li>
				<!--<a href="padmabhushan-vasant-dada-patil.php">Padmabhushan Vasant Dada <br>Patil College of Agriculture</a>-->
			</li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">ADMISSIONS<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('admission-process');?>">ADMISSION PROCESS </a></li>
			<li><a href="#">BROCHURE</a></li>
			<li><a href="#">COURSES</a></li>
			<li><a href="#">ADMISSION FORM</a></li>
			<li><a href="<?php echo base_url('video');?>">VIDEO</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">RESEARCH<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('directorate-of-research');?>">Directorate of Research</a></li>
			<li><a href="<?php echo base_url('plagiarism-policy');?>">Plagiarism Policy</a></li>
			<li><a href="#">Academic/Funded Research</a></li>
			<li><a href="<?php echo base_url('research-policy');?>">RESEARCH POLICY</a></li>
			<li><a href="<?php echo base_url('research-committee');?>">RESEARCH COMMITTEE</a></li>
			<li><a href="#">RESEARCH PUBLICATIONS</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="campus">CAMPUS FACILITIES<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('library');?>">24X7 Library</a></li>
			<li><a href="<?php echo base_url('transportation');?>">Transportation</a></li>
			<li><a href="<?php echo base_url('student-residences');?>">Student Residences</a></li>
			<li><a href="<?php echo base_url('cafeteria');?>">Cafeterias</a></li>
			<li><a href="<?php echo base_url('banking');?>">Banking</a></li>
			<li><a href="<?php echo base_url('gymnasium');?>">Gym</a></li>
			<li><a href="<?php echo base_url('healthcare');?>">HEALTHCARE</a></li>
			<li><a href="<?php echo base_url('sports');?>">SPORTS</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">PLACEMENTS & INTERNSHIPS<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="#">Internships</a></li>
			<li class="dropdown dropdown-submenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Placements <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url('placement-about');?>">ABOUT</a></li>
					<li><a href="<?php echo base_url('recruiters');?>">RECRUITERS</a></li>
				</ul>
			</li> 
			<li><a href="#">International IV's</a></li>
			<li class="dropdown dropdown-submenu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Alumni <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a href="#">ALUMNI NETWORK</a></li>
				<li><a href="#">ALUMNI SPEAK</a></li>
			</ul>
			</li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">SPORTS & CULTURAL<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('about-sports');?>">About</a></li>
			<li><a href="<?php echo base_url('facilities');?>">Facilities</a></li>
			<li><a href="<?php echo base_url('events');?>">Events</a></li>
		</ul>
	</li>
	<li><a href="#" id="notices">NOTICES & ANNOUNCEMENTS </a></li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">FEEDBACK<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="#">Student Feedback</a></li>
			<li><a href="#">Faculty Feedback</a></li>
			<li><a href="#">Employer Feedback</a></li>
			<li><a href="#">Parent Feedback</a></li>
			<li><a href="#">Professional's Feedback</a></li>
		</ul>
	</li>
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="admissions">CONTACT US<b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="<?php echo base_url('sitemap');?>">Site Map</a></li>
			<li><a href="<?php echo base_url('contact');?>">Contact US</a></li>
		</ul>
	</li>
	</ul>
	</nav>
    <ul id="menu">
		<li>
			<a href="#">ABOUT US</a>
			<ul>
				<li><a href="about-university.php">University</a></li>
				<li><a href="founder.php">Founder President</a></li>
				<li><a href="president.php">President</a></li>
				<li><a href="managing.php">Managing Trustee</a></li>
				<li><a href="vice-chancellor.php">Vice Chancellor</a></li>
				<li><a href="vision-mision.php">Vision, Mission, Goals</a></li>
				<li><a href="core-values.php">Core Value</a></li>
				<li><a href="ethics.php">Ethics</a></li>
				<li><a href="ranking.php">Affiliations, Rankings & Awards</a></li>
				<li><a href="advisory.php">Advisory Board</a></li>
				<li><a href="administration.php">Administration</a></li>
				<li><a href="university-regulations.php">University Regulations</a></li>
				<li><a href="committees.php">Committees</a></li>
			</ul>
		</li>
		<li><a href="#" id="school">SCHOOLS</a>
			<ul>
				<li><a href="#">School of Engineering</b></a>
					<ul>
						<li><a href="civil-infrastructural-engineering.php">Department of Civil & <br>Infrastructural Engineering </a></li>
						<li><a href="electrical-electronics-engineering.php">Department of  Electrical & <br>Electronics Engineering </a></li>
						<li><a href="mechanical-engineering.php">Department of Mechanical <br>
						Engineering  </a></li>
						<li><a href="information-technology-engineering.php">Department of Information <br>Technology Engineering </a></li>
						<li><a href="computer-information.php">Department of Computer & <br>
						Information Science Engineering </a></li>
						<li><a href="automobile-engineering.php">Department of Automobile <br>
						Engineering </a></li>
					</ul>
				</li>
				<li><a href="school-of-pharmacy.php">School of Pharmacy</a></li>
				<li><a href="school-of-business-management.php">School of Business Management</a></li>
				<li><a href="school-of-hospitality-tourism-studies.php">School of Hospitality & <br>Tourism Studies</a></li>
				<!--<li><a href="padmabhushan-vasant-dada-patil.php">Padmabhushan Vasant Dada <br>Patil College of Agriculture</a></li>-->
			</ul>
		</li>
		<li><a href="#" id="admissions">ADMISSIONS</a>
			<ul>
				<li><a href="#">ADMISSION PROCESS </a></li>
				<li><a href="#">BROCHURE</a></li>
				<li><a href="#">COURSES</a></li>
				<li><a href="#">ADMISSION FORM</a></li>
				<li><a href="#">VIDEO</a></li>
			</ul>
		</li>
		<li>
			<a href="#" id="research">RESEARCH</a>
			<ul>
				<li><a href="#">Directorate of Research</a></li>
				<li><a href="#">Plagiarism Policy</a></li>
				<li><a href="#">Academic/Funded Research</a></li>
				<li><a href="#">RESEARCH POLICY</a></li>
				<li><a href="#">RESEARCH COMMITTEE</a></li>
				<li><a href="#">RESEARCH PUBLICATIONS</a></li>
			</ul>
		</li>
		<li>
			<a href="#" id="campus">CAMPUS FACILITIES</a>
			<ul>
				<li><a href="247-library.php">24X7 Library</a></li>
				<li><a href="transportation.php">Transportation</a></li>
				<li><a href="student-residences.php">Student Residences</a></li>
				<li><a href="cafeteria.php">Cafeterias</a></li>
				<li><a href="banking.php">Banking</a></li>
				<li><a href="gym.php">Gym</a></li>
				<li><a href="healthcare.php">HEALTHCARE</a></li>
				<li><a href="sports.php">SPORTS</a></li>
			</ul>
		</li>
		<li><a href="#" id="placements">PLACEMENTS & INTERNSHIPS</a>
        <ul>
            <li><a href="#">Internships</a></li>
			<li><a href="#">Placements</a>
				<ul>
					<li><a href="#">ABOUT</a></li>
					<li><a href="#">RECRUITERS</a></li>
				</ul>
			</li> 
			<li>
			<a href="#">International IV's</a></li>
			<li><a href="#">Alumni</a>
				<ul>
					<li><a href="#">ALUMNI NETWORK</a></li>
					<li><a href="#">ALUMNI SPEAK</a></li>
				</ul>
			</li>
           
        </ul>
        </li>
		<li>
			<a href="#" id="sports">SPORTS & CULTURAL</a>
			<ul>
				<li><a href="#">About</a></li>
				<li><a href="#">Facilities</a></li>
				<li><a href="#">Events</a></li>
			</ul>
		</li>
		<li><a href="#" id="notices">NOTICES & ANNOUNCEMENTS </a></li>
		<li>
			<a href="#" id="feedback">FEEDBACK</a>
			<ul>
				<li><a href="#">Student Feedback</a></li>
				<li><a href="#">Faculty Feedback</a></li>
				<li><a href="#">Employer Feedback</a></li>
				<li><a href="#">Parent Feedback</a></li>
				<li><a href="#">Professional's Feedback</a></li>
			</ul>
        </li>
		<li>
			<a href="#" id="contact">CONTACT US</a>
			<ul>
				<li><a href="<?php echo base_url('sitemap');?>">Site Map</a></li>
				<li><a href="<?php echo base_url('contact');?>">Contact US</a></li>
			</ul>
		</li>
    </ul>
 
    </header>
    <!-- end header section -->