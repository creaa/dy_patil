<div class="banner">
<img src="<?php echo base_url('assets/images/placement/place_about_banner.jpg');?>" class="img-responsive">
</div>
<div class="main">
	<div class="recruiters_section1 placement_about_section1">
		<div class="container4">
			<h2>Placement - About</h2>
			<div class="row">
				<div class="col-xs-12">
				<?php foreach($pi as $placementInter){ 
				if($placementInter['page_section']==1){ ?>
					<div class="placement_about">
					<?=$placementInter['description']?>
					</div>
				<?php } } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="placement_about_section2">
		<div class="container4">
			<div class="row">
				<div class="col-xs-12">
					<div class="architecture_left placement_about_left">
					<?php foreach($pi as $placementsec){ 
						if($placementsec['page_section']==2){ ?>
						<div class="architecture_bottom">
						<h4><?=$placementsec['title'];?></h4>
						<?=$placementsec['description'];?>
						<img src="<?php echo base_url('assets/uploads/placement_interns/'.$placementsec['image']);?>" class="img-responsive" alt="">
						</div>
					<?php } } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>