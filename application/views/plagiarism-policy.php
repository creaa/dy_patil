<div class="banner">
	<img src="<?php echo base_url('assets/images/research/plagiarism-policy_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 architecture_section1 plagiarism_policy">
		<div class="container4">
			<h2>Plagiarism Policy of D Y Patil University Pune</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="architecture_left plagiarism_policy_left">
						<?php foreach($plagiarism as $pla){
							if($pla['page_section']==1 && $pla['image'] =="") { ?>
						<div class="architecture_bottom">
							<h4><?=$pla['title'];?></h4>
							<?=$pla['description'];?>
						</div>
						<?php } } ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php foreach($plagiarism as $pla){
					if($pla['image'] !="") { ?>
					<div class="architecture_right">
						<img src="<?php echo base_url('assets/uploads/research/'.$pla['image']);?>" class="img-responsive" alt="">
					</div>
					<?php } } ?>
				</div>
			</div>	
		</div>
	</div>
	<div class="architecture_section2">
		<div class="container4">
			<div class="row">
				<div class="col-xs-12">
					<div class="architecture_left plagiarism_policy_left plagiarism_policy2">
						<?php foreach($plagiarism as $pla){
								if($pla['page_section']==2){ ?>
						<div class="architecture_bottom">
							<h4><?=$pla['title']?></h4>
							<?=$pla['description']?>
						</div>
						<?php } } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>