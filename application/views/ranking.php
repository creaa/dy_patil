<div class="banner">
	<img src="<?php echo base_url('assets/images/about/ranking_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 about_program_section1">
		<div class="container">
			<h2>AFFILIATIONS, RANKINGS & AWARDS</h2>
			<div class="row">
				<div class="col-xs-12">
					<div class="program_about">
						<ul>
							<?php foreach($detail as $values){ ?>
							<li>
								<div class="program_about_bottom">
									<img src="<?php echo base_url('assets/uploads/about_us/'.$values['image']);?>" class="img-responsive" alt="">
									<a href="#"><?=$values['title'];?></a>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>