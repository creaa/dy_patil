<div class="banner">
	<img src="<?php echo base_url('assets/images/research/research-committee_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 architecture_section1">
		<div class="container4">
			<h2>RESEARCH COMMITTEE </h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<div class="architecture_left">
						<?php foreach($comm as $cm){ 
						if($cm['description'] !=""){ ?>
						<div class="architecture_bottom">
						<h4><?php echo $cm['title'];?></h4>
							<?php echo $cm['description'];?>
						</div>
						<?php } } ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php foreach($comm as $cmg){ if($cmg['image'] !=""){ ?>
					<div class="architecture_right">
						<img src="<?php echo base_url('assets/uploads/research/'.$cmg['image']);?>" class="img-responsive" alt="">
					</div>
				<?php } } ?>
				</div>
			</div>	
		</div>
	</div>
</div>