<?php include_once"header.php"?>

<!-- banner section start -->
<div class="banner">
<img src="images/school/computer_banner.jpg" class="img-responsive" alt="">
</div>
<!-- banner section end -->

<!-- main section start -->
<div class="main">
<div class="about_section1 architecture_section1">
<div class="container4">
<h2>SCHOOL OF <br>
COMPUTER APLLICATIONS</h2>
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="architecture_left">
	<div class="architecture_bottom">
<h4>About the Department:</h4>
<p>Master of Computer Applications (MCA) is a professional post-graduate program designed specifically for candidates wanting to delve deeper into the world of computer applications & development with the help of learning modern programming language. The program is a blend of both theoretical and practical knowledge. An MCA degree endows students’ an opportunity  to work with tools meant to develop better and faster applications. The course emphasis on latest programming languages and tools to develop better and faster applications. </p>
</div>
<div class="architecture_bottom">
<h4>Faculty:</h4>
<p>The School of Computer Applications incorporates experienced, learned faculties who are trained in all the major computing languages. Besides, the department extensively revamps the curriculum in order to incorporate all the emergent languages in the curriculum. Students get to learn the best of both worlds as regular interactive sessions are organized with industry professionals for the students. </p>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="architecture_right">
	<img src="images/school/computer_banner_img1.jpg" class="img-responsive" alt="">
</div>
</div>
</div>	
</div>
</div>

<div class="architecture_section2">
<div class="container4">
<div class="row">
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="architecture_left">
<div class="architecture_bottom">
<h4>Academics:</h4>
<p>The post graduate program is divided into 6 semesters and covers all the major languages of programming. It also includes subjects like Systems Management, Management Information Systems, Application Development, Application Software, Troubleshooting etc. </p>
</div>

<div class="architecture_bottom">
<h4>Eligibility:</h4>
<p>Graduates from any discipline with minimum 45% marks from a recognized university.</p>
</div>

<div class="architecture_bottom">
<h4>Syllabus:</h4>
<a href="#">View full syllabus - Semester Details</a>
</div>

<div class="architecture_bottom">
<h4>Documents Required:</h4>
<ul>
	<li>Class Xth marksheet & Passing Certificate</li>
	<li>Class XIIth marksheet & Passing Certificate</li>
	<li>Qualifying Exam Marksheet</li>
	<li>Graduation marksheets & passing certificates</li>
	<li>Aadhar Card Copy</li>
	<li>Pan Card Copy</li>
	<li>08 Passport Photographs</li>
</ul>
</div>

<div class="architecture_bottom">
<h4>Duration of Course: 4 Years (8 semister)</h4>
</div>

<div class="architecture_bottom">
<h4>Academic Calendar:</h4>
<div class="table-responsive">          
  <table class="table">
    <thead>
    	<tr>
        <th colspan="3" style="background: #9f1c33; border:0; text-align: center;">Academic Calendar 2020-21</th>
      </tr>
      <tr>
        <th>Commencement & End of Term</th>
        <th>First Term </th>
        <th>First Term  </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Orientation for First Years</td>
        <td>3rd July 2020</td>
        <td>6th Jan 2021 to  30th May 2021</td>
      </tr>

      <tr>
        <td>Engineering Sem-I</td>
        <td>7th July 2020  to 7th Dec 2020</td>
        <td>6th Jan 2021 to  30th May 2021</td>
      </tr>

      <tr>
        <td>1st Class  Test</td>
        <td>16th Aug  2020 to 25th Aug. 2020</td>
        <td>3rd  Feb 2021 to 07th Feb 2021</td>
      </tr>

      <tr>
        <td>1st Parent teachers meeting</td>
        <td>30th Aug.2020</td>
        <td>15th Feb. 2021</td>
      </tr>

      <tr>
        <td>2nd Class Test</td>
        <td>6th Oct 2020 to 13th Oct 2020</td>
        <td>9th  Mar.2021 to 13th Mar.2021</td>
      </tr>

      <tr>
        <td>2nd Parent teachers meeting</td>
        <td>31st  Oct 2020</td>
        <td>21st March. 2021</td>
      </tr>

      <tr>
        <td>Term End Exam</td>
        <td>11th Nov 2021 to 7th Dec 2021</td>
        <td>4th May2021 to 30th May 2021</td>
      </tr>

      <tr>
        <td colspan="3">Winter Vacation -  10th Dec to 5th Jan.2021	</td>
      </tr>
       <tr>
        <td colspan="3">Summer Vacation from  16th May To 05th July 2022</td>
      </tr>
       <tr>
        <td colspan="3">Summer Vacation from  16th May To 05th July 2022</td>
      </tr>

    </tbody>
  </table>
  </div>
</div>

<div class="architecture_bottom">
<h4>Fees:</h4>
<p>The Tuition Fees for Computer Aplications  MCA  is INR 1,00,000/- Per Year.</p>
 <p>Enrollment Fees 1000/- once in 3 Years.</p>
<p>Examination Fees 1000/- Per Semester.</p>
<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>
</div>

</div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
<div class="architecture_right">
	<img src="images/school/computer_banner_img2.jpg" class="img-responsive" alt="">
</div>
</div>
</div>	
</div>
</div>
</div>
<!-- main section end -->

 <?php include_once"footer.php"?> 