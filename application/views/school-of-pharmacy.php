
<div class="banner">
<img src="<?php echo base_url('assets/images/school/pharmacy_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="about_section1 architecture_section1">
		<div class="container4">
			<h2>SCHOOL OF PHARMACY</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<?php foreach($info as $key=>$data){ 
					if($key==0){ ?>
					<div class="architecture_left">
						<h4><?=$data['title']?></h4>
						<?=$data['description'] ?>
						<?php } else{
						if($data['title']=="Fees"){ ?>
						<div class="architecture_bottom">
						<h4>Academic Calendar:</h4>
						<div class="table-responsive tab_sec">          
						  <table class="table">
							<thead>
							  <tr>
								<th colspan="3" style="background: #9f1c33; border:0; text-align: center; border: 1px solid #636363;"><?=$header[0]['session']?></th>
							  </tr>
							  <tr>
								<th><?=$header[0]['heading_first']?></th>
								<th><?=$header[0]['heading_second']?></th>
								<th><?=$header[0]['heading_third']?></th>
							  </tr>
							</thead>
							<tbody>
							<?php foreach($calendar as $academicsCal){ 
								if($academicsCal['first_term '] !="" || $academicsCal['second_term'] !="") { ?>
								<tr>
									<td><?php echo $academicsCal['commencement_end_of_term'];?></td>
									<td><?php echo $academicsCal['first_term'];?></td>
									<td><?php echo $academicsCal['second_term'];?></td>
								</tr>
							<?php }
							elseif($academicsCal['vacation'] !="") { ?>
								<tr>
									<td colspan="3"><?=$academicsCal['vacation']?></td>
								</tr>								
							<?php } } ?>
							</tbody>
						  </table>
						  </div>
						</div>
							<div class="architecture_bottom">
								<h4><?=$data['title'].':'?></h4>
									<?=$data['description'];?>
							</div>
						<?php 
							break;
							?>
						<?php }
						else{
							$document="";
							$file=$data['image'];	
							$FileExtension=explode('.',$file);
							if($FileExtension[1]=="PDF" || $FileExtension[1]=="pdf"){
								$document .=$file;
							}
							?>
						<div class="architecture_bottom">
						<?php if($data['description']!=""){ ?>
						<h4><?=$data['title'].':'?></h4>
						<?php } else{ ?>
							<h4><?=$data['title'];?></h4>
						<?php }
						if(strtolower($data['title'])=="syllabus"){ 
							?>
						 <a href="<?php echo base_url('assets/uploads/school/'.$document);?>"><?=strip_tags($data['description'])?></a>
						 
						<?php } else {
							echo $data['description'];
						} ?>
						</div>
						<?php } } } ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<?php foreach($info as $key=>$imgs){
					if($imgs['image'] !=""){ ?>
					<div class="architecture_right">
						<img src="<?php echo base_url('assets/uploads/school/'.$imgs['image']);?>" class="img-responsive" alt="">
					</div>
				<?php } } ?>
				</div>
			</div>	
		</div>
	</div>
</div>