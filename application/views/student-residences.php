<div class="banner">
	<img src="<?php echo base_url('assets/images/campus/student-residences_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
	<div class="recruiters_section1 transportation_section1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php foreach($fac as $res){ ?>
					<div class="transportation_sec_bottom">
						<h2><?=$res['title'];?></h2>
						<?=$res['description'];?>
					</div>
					<?php } ?>
					<div class="trans_slider">
						<?php foreach($fac as $resimg){ ?>
						<div>
						<img src="<?php echo base_url('assets/uploads/facility/'.$resimg['image']);?>" class="img-responsive" alt="">
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 