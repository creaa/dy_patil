<div class="banner">
<img src="<?php echo base_url('assets/images/campus/transportation_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="recruiters_section1 transportation_section1">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<?php foreach($fac as $tras){ ?>
				<div class="transportation_sec_bottom">
					<h2><?=$tras['title']?></h2>
					<?=$tras['description']?>
				</div>
			<?php } ?>
			<div class="trans_slider">
				<?php foreach($fac as $timg){ 
				if($timg['image'] !=""){ ?>
				<div>
					<img src="<?php echo base_url('assets/uploads/facility/'.$timg['image']);?>" class="img-responsive" alt="">
				</div>
				<?php } } ?>
			</div>
		</div>
	</div>
</div>
</div>
</div>