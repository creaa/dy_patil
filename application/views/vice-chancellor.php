<div class="banner">
<img src="<?php echo base_url('assets/images/about/managing_banner.jpg');?>" class="img-responsive" alt="">
</div>
<div class="main">
<div class="about_section1">
<div class="container4">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="founder_left vice_left" data-aos="fade-right" data-aos-duration="1500">
<img src="<?php echo base_url('assets/uploads/about_us/'.$detail[0]['image']);?>" class="img-responsive" alt="">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
<div class="founder_right" data-aos="fade-left" data-aos-duration="1500">
<div class="founder_bottom">
	<h3><?php echo $detail[0]['title'];?></h3>
</div>
<?php echo $detail[0]['description'];?>
</div>
</div>
</div>	
</div>
</div>
</div>