-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 05:08 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dy_patil`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `page_name_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `page_name_id`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(1, 4, 'ABOUT UNIVERSITY', '<p>Nestled right next to the Mumbai Pune Highway, the DY Patil University Pune stands a bastion for world-class education in a student-centric environment. Spread over a vast campus, the University grounds offer a range of academic programs that are paired with state-of-the-art amenities. After all, there is no one better who can understand the importance of education and how it contributes to shaping your future.</p>\r\n\r\n<p>At DYPU, the focus is unilaterally on equipping its students with the appropriate skills for excelling in their chosen careers and turning their dreams into reality. Supporting a holistic educational experience, the University believes that nurturing the creativity of its students is just as important as imparting academic knowledge and sharpening their practical skills. Thus, our courses are designed with the same goals.</p>\r\n\r\n<p>DYPU offers a host of programs, ranging from postgraduate education in Engineering, Management, and Pharmacy to Hospitality, Architecture and Agriculture. Additionally, all the programs are recognized by their respective councils and the UGC.</p>\r\n\r\n<p>We work hard towards ensuring our students get maximum exposure on the global level to guarantee their comprehensive development. To this end, we have tie-ups with several renowned Universities around the world, and we often organize workshops, enclaves, and seminars to help the students get familiarized with International standards. Our teaching methods are a unique blend of traditional and modern elements, along with advanced laboratory work.</p>\r\n\r\n<p>The talented faculty at DYPU is a team of inspiring individuals who are seasoned experts in their subjects and nurture the students at every step of their program. They help you develop your skills, and help you get some hands-on experience to turn your passion into a rewarding career. DYPU is a world-renowned name that will help you lay down a strong foundation for your career. Our alumni working as professionals, entrepreneurs, leaders, academicians and experts are proof of our commitment to a world-class education.</p>', '694f09bedd7d5ae8bdbda031c008d59b.png', '2019-12-17', '0000-00-00'),
(3, 5, 'Dr. D Y Patil FOUNDER PRESIDENT & PADMASHRI RECIPIENT', '<p>Never before in the history of mankind has it been so pervading. Mankind is witnessing the integration of people across nations of the world into a global knowledge based society. Knowledge empowers people. It empowers the individual and accelerates his development and ensures that he commands an enhanced level of respect in society. Knowledge also enables the individual to contribute to the process of national development while being a source for wealth creation. In the society that we live in today, there is a knowledge explosion every moment all over the world. All this is driven and achieved by providing quality par excellence education right from the roots to the tertiary levels of education, Higher levels of exceptional learning through reputed colleges and universities. Students now seek education from advanced centres of learning to enhance their skills and abilities in a highly competitive world.</p>\r\n<p>D Y Patil Deemed to be University, offers a stimulating environment which brings out the very best in students. Over decades, the university has nurtured and moulded able minds who have now become stalwarts in their profession.  World class facilities, Comprehensive Curriculum, Advanced Teaching Methodology, Experienced Faculties â€“all together provide a wholesome milieu for exceptional skill development and growth. I am sure that your journey in D Y Patil Deemed to be University will be enlightening and as rewarding as it has </p>', '8fdd5d7072076ec0dedb13ce22e14765.png', '2019-12-17', '0000-00-00'),
(4, 6, 'DR. VIJAY D PATIL PRESIDENT', '<p>Vision is what you need to work towards to achieve success. It is something that should bring before your eyes a goal that needs to be achieved. The youth of today have a lot of promise and potential. It is this potential that needs to be converted into concrete action for India\'s smooth progress in this millennium. Education is the most important tool to achieve this. It is the initiative in education along with progress in health care that will bring about transformation in the country today. </p>\r\n\r\n<p>Over the years, the D. Y. Patil Group, with its commitment and dedication, has earned a reputation of being a provider of quality education in areas of professional significance. With the increasing number of talented students who join this University each year, we look forward to becoming a world-class university and creating a distinctive position for ourselves. It is our Founder Chancellor\'s dream to see the youth steer this nation on the path of progress by using the power of knowledge as their strongest tool. It is time that students of today take it upon themselves to see the words of our elders translated into action. It is our responsibility as educated citizens of India to spread the light, the light of knowledge. </p>\r\n\r\n<p>My best wishes go your way as you embark on this wonderful learning experience at the D Y Patil University, Navi Mumbai.</p>', 'bac910bc307552cf9777f26c116fbfbb.png', '2019-12-17', '0000-00-00'),
(5, 7, 'MRS. SHIVANI V PATIL MANAGING TRUSTEE', '<p>The D. Y. Patil Group is a pioneer in the field of higher learning. Our mission, to advance new ideas and promote knowledge, has created excellent academic learning. We strive to create an environment in which outstanding students and scholars from around the world are continually challenged and inspired to do their best possible work. Our campus at Navi Mumbai is amongst the finest educational complexes in the country. I am pleased to welcome you all to the University. I am sure that the memories you take home from this campus will be the most cheerful and enriching ones in your entire academic career. Wishing you the very best for your education and your respective careers.</p>', 'e3dd23552990e649280c112e74a9b206.png', '2019-12-17', '0000-00-00'),
(6, 8, 'MESSAGE FROM THE VICE CHANCELLOR', '<p>It is a matter of great pride and privilege to be part of an esteemed organization D Y Patil University that prepares today’s students to meet the challenges of the industry. I have seen the way D Y Patil University has become pioneer in providing superior quality education in the neighbouring areas.</p>\r\n<p>This has become possible with the untiring efforts of our teaching faculty which imparts teaching by relating it with real life scenarios and current developments in their respective fields. The course curriculum is devised based on the ever changing needs of industry so that students are absorbed in industry smoothly and shine as proud alumni of D Y Patil University.</p>\r\n<p>We know that for students to compete internationally and become a part of world top companies they need to be creative and innovative. We cultivate in our students right attitude with blend of extra-ordinary soft skills and hence make their personality impressive so that they become brand ambassadors of not only D Y Patil University but also the companies with which they get associated.</p>\r\n<p>It is creditable that the D Y Patil University has shaped a contemporary system to make it conceivable for staff and students to appraise their skills and talent in numerous areas like arts, social sciences, medical and technical disciplines because of rapid evolving developments in modern era. Anyone can notice that the D Y Patil University has been fairly effective in sustaining equilibrium amid contemporary leanings in teaching and its ancient civilisations.</p>\r\n<p>As an Vice Chancellor, I am confident that efforts to excel in the field of higher education and the inculcation of moral values in the students at the University Campus will continue in future with a greater zeal.</p>\r\n<p>We as a team at D Y Patil University strive to prepare students to become better citizens of our country irrespective of their cultural and financial background and contribute in the welfare of society and country’s development.</p>\r\n<p>I, therefore, call students to be part of this revolution led by D Y Patil University and become the stars of tomorrow.</p>', 'f2e4b48c75bde9b54786f7f9ae709da4.png', '2019-12-17', '0000-00-00'),
(7, 9, 'VISION', '<p>To create techno-economic and socio-economic innovative leaders with global identity through value added education and research </p>', 'ea89e2efa1362d6b2113e68b0241aaaf.png', '2019-12-17', '0000-00-00'),
(8, 9, 'MISSION', '<li>To foster quality education by incorporating participative and collaborative teaching learning process </li>\r\n	<li>To empower students for accepting multidisciplinary approach for sustainable socio economic development and lifelong learning </li>', '3e15e646dd975b06755dfc94108b9914.png', '2019-12-17', '0000-00-00'),
(9, 9, 'GOALS', '<li>To educate students to be socially responsible professionals, innovators and good human beings of tomorrow by accepting the global challenges</li>\r\n	<li>To develop skilled professionals with overall personality development </li>', '1257c5a4cdc3bee2be99d46de979998b.png', '2019-12-17', '0000-00-00'),
(10, 10, 'CORE VALUES', '<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>ACADEMIC FREEDOM - </strong> The “freedom of teaching and discussion, freedom in carrying out research and disseminating and publishing the results thereof, freedom to express freely opinions about the academic institution or system in which one works, freedom from institutional censorship and freedom to participate in professional or representative academic bodies.”</p>	\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>INSTITUTIONAL AUTONOMY - </strong> The degree of self-governance necessary for effective decision-making by higher education institutions and leaders regarding their academic work, standards, management and related activities consistent with principles of equitable access, academic freedom, public accountability, and social responsibility.</p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>SOCIAL RESPONSIBILITY - </strong> In higher education, this is the duty to use the freedoms and opportunities afforded by state and public respect for academic freedom and institutional autonomy in a manner consistent with the obligation to seek and impart truth, according to ethical and professional standards, and to respond to contemporary problems and needs of all members of society. </p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>EQUITABLE ACCESS - </strong> Entry to and successful participation in higher education and the higher education profession is based on merit and without discrimination on grounds of race, gender, language or religion, or economic, cultural or social distinctions or physical disabilities, and includes active facilitation of access for members of traditionally underrepresented groups, including indigenous peoples, cultural and linguistic minorities, economically or otherwise disadvantaged groups, and those with disabilities, whose participation may offer unique experience and talent that can be of great value to the higher education sector and society generally</p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>ACCOUNTABILITY - </strong> The institutionalization of clear and transparent systems, structures or mechanisms by which the state, higher education professionals, staff, students and the wider society may evaluate—with due respect for academic freedom and institutional autonomy—the quality and performance of higher education communities.</p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>RESPECT - </strong> Treat our classmates, professors, staff, and administrators with respect. We respect diversity and do not discriminate on the basis of race, color, ethnicity, religion, gender, disability, sexual orientation, age, or other characteristics. We respect different points of view that add to our knowledge. We respect our learning opportunities by behaving professionally in the classroom, and making academic achievement an important priority.  </p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>HONESTY - </strong> Communicate truthfully with our classmates, professors, staff, and administrators in all academic matters while remaining respectful. We observe university policies on academic dishonesty in completing all academic work. In seeking employment, internships, and other opportunities we represent ourselves truthfully, understanding that misrepresentation may not only harm our own reputation but that of our classmates and the college.</p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>INTEGRITY - </strong> Maintain integrity. Because we zealously integrate these core values into our academic work and preparation for our profession, our integrity enables us to reach our goals, overcome obstacles, and successfully resolve ethical dilemmas.</p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"> <strong>COMMITMENT - </strong> Strive for success as students, professionals, and citizens. We keep our promises, and adhere to our core values in all our activities both as students and alumni. We are committed to acting honestly, respectfully, and responsibly in our effort to achieve our goals. We understand that commitment to our core values benefits both students and the community now and in the future. </p>\r\n\r\n<p data-aos=\"fade-up\" data-aos-duration=\"1500\"><strong>RESPONSIBILITY - </strong> Embrace the responsibility we have to ourselves and to each other to maintain high ethical standards. With each task at hand comes the responsibility to uphold the core values that unite us. We support each other in our adherence to these standards. We recognize that reporting unethical conduct is a responsibility we all share.</p>', '', '2019-12-17', '0000-00-00'),
(11, 11, 'ETHICS & CODE OF CONDUCT', '<p>D Y Patil University students must abide by the rules and regulations of the institute. The institute authorities may take disciplinary action if any student violates the institute rules and regulations. Students are advised to adhere to the rules and regulations of the institute and discharge their responsibilities as  a student with diligence, fidelity and honour. The rules and regulations are categorized into three categories as mentioned below. Students are required to follow these rules and also have to submit bond of good conduct.</p>', '', '2019-12-17', '0000-00-00'),
(12, 11, 'D Y Patil University, Pune  General Rules and Regulations for Students', '<p>Students shall behave with dignity and courtesy inside and outside the college.</p>\r\n        		<ol>\r\n        			<li>Students should wear identity cards inside the campus and also when representing college in meetings outside the campus. ID-Cards are to be worn round the neck and this drill is compulsory. Any violation of these orders will lead to disciplinary action.</li>\r\n        			<li>Possession of mobile phones during the college hours in the campus is strictly prohibited. Anybody found using a mobile,inside  the  premises will be ceased and will returned only after the completion of the course.</li>\r\n        			<li>Students are not allowed to leave the institution premises during the institution timings. If a student wants to leave the institute  for  some valid reasons before the closing of institution timing they must get Gate Pass.</li>\r\n        			<li>Spitting, and throwing bits of paper inside the institution campus are unhealthy and must be avoided. Refrain from possessing, consuming or distributing alcohol, harmful drugs, narcotics, and smoking cigarettes. Any violation of these orders will lead to disciplinary action.</li>\r\n        			<li>Students should not possess firecrackers of any kind in the hostel and college campus.</li>\r\n        			<li>Do not smear colored powder and splash rangoli water in the guise of festivals and functions on or during any other occasion in the hostel or college campus.</li>\r\n        			<li>Students are advised not to harm the reputation of the institution or individual (fellow students and institution staff) through social and electronic media.</li>\r\n        			<li>Respect the institution property. Destroying or damaging the institution property is punishable. Students should not destroy/damage/deface, remove the institution property, disturb or injure a person under the pretext of celebrating/inducting/pledging or for any other reason like rivalry etc. The cost of any damage so caused will be recovered from the students collectively if the responsibility for it cannot be fixed on any individual or group of individuals.</li>\r\n        			<li>All vehicles should be parked in the allotted place. No vehicle will be allowed to enter the institute premises during the institute timings. Students coming by two wheelers have to compulsorily wear Helmet. Two wheelers will not be permitted to park without Helmet. Vehicles found parked in unauthorized places shall be impounded.</li>\r\n        			<li>Students who want  to avail bus facility in between the session will have  to pay full bus fee for the session.</li>\r\n        			<li>Students who are not availing the bus facility but caught traveling in bus will be charged with full bus fee as fine.</li>\r\n        			<li>While attending college functions, the students should conduct themselves in such a way as to bring recognition to themselves and to the institution.</li>\r\n        		</ol>', '', '2019-12-17', '0000-00-00'),
(13, 11, 'Code of Conduct for Students:', '<ul>\r\n        			<li>The identity card with photograph affixed, must be carried by the student all the times while on the campus and must be produced on demand.</li>\r\n        			<li>Student must attend the lectures and practical regularly as per the time table.</li>\r\n        			<li>Student should have minimum 75% attendance in theory and practical for attending University exams.</li>\r\n        			<li>Student’s conduct should be satisfactory.</li>\r\n        			<li>The absence from the institution without prior information will be considered  a breach of discipline.</li>\r\n        			<li>Student must appear for all the tests and examinations and should show satisfactory progress.</li>\r\n        			<li>Student are advised to read all the notices displayed in the notice boards.</li>\r\n        			<li>The conduct of the student in the premises of the institution as well as in their classes should cause no disturbance to fellow students  or  other classes.</li>\r\n        			<li>Student must not loiter in the institution premises during class timings.</li>\r\n        			<li>No society or association must be formed in the institute or in the hostels and no person should be invited to address a meeting  without  principal’s prior permission.</li>\r\n        			<li>No official trips should be arranged without prior consent of the principal.</li>\r\n\r\n        		</ul>', '', '2019-12-17', '0000-00-00'),
(14, 11, 'Anti Ragging - RAGGING is a CRIMINAL offense.', '<p><strong>D Y Patil University, Pune, is a NO-RAGGING Zone.</strong></p>\r\n        		<p>The college has an anti-ragging cell to prohibit ragging in the institution.</p>\r\n        		<p>It is mandatory for all students to fill the Anti-ragging Affidavit issued by the institution at the time of admission.</p>\r\n        		<p><strong>What constitutes Ragging?</strong></p>\r\n        		<p>Ragging constitutes one or more of the following acts:</p>\r\n        		<ol>\r\n        			<li>Any conduct by any student or students whether by words, spoken or written or by an act which has the effect of teasing, treating or handling with rudeness with a fresher or any other student.</li>\r\n        			<li>Indulging in rowdy or undisciplined activities by any student or students which causes or is likely to cause annoyance, hardship, physical or psychological harm or to raise fear or apprehension thereof in  any  fresher or any other student.</li>\r\n        			<li>Asking any student to do any act which such student will not in the ordinary course do and which has the effect of causing or generating a sense of shame, or torment or embarrassment, so as to adversely affect the physique or psyche of such fresher or any other student.</li>\r\n        			<li>Any act by a senior student that prevents, disrupts or disturbs  the  regular academic activity of any other student or a fresher.</li>\r\n        			<li>Exploiting the services of a fresher or any other student from completing the academic tasks assigned to an individual or a group of students.</li>\r\n        			<li>Any act of financial extortion or forceful expenditure burden put on a fresher or any other student by students.</li>\r\n	        		<li>Any act of physical abuse including all variants of it: sexual abuse, homosexual assaults, stripping, forcing obscene and lewd acts, gestures, causing bodily harm or any other danger to health or person.</li>\r\n	        		<li>Any act or abuse by spoken words, emails, posts, public insults which would also include deriving perverted pleasure, vicarious or sadistic thrill from actively or passively participating in the discomfiture to fresher or any other student.</li>\r\n	        		<li>Any act that affects the mental health and self-confidence of a fresher or any other student with or without an intent to derive a sadistic pleasure  or showing off power, authority or superiority by a student over any fresher or any other student.</li>\r\n        		</ol>', '', '2019-12-17', '0000-00-00'),
(15, 11, 'Punishment for Defaulters', '<p>The Management, shall consider one or more of the following punishments for students involved in ragging</p>\r\n        		<ol>\r\n        			<li>Cancellation of admission</li>\r\n        			<li>Suspension from attending classes</li>\r\n        			<li>Withholding/withdrawing scholarship/fellowship and other benefits</li>\r\n        			<li>Debarring from appearing in any test/examination or other evaluation process</li>\r\n        			<li>Withholding results</li>\r\n        			<li>Debarring from representing the institution in any regional, national or international meet, tournament, youth festival, etc.</li>\r\n        			<li>Suspension/expulsion from the hostel</li>\r\n        			<li>Rustication from the institution for period ranging from 1 to 4 semesters</li>\r\n        			<li>Expulsion from the institution and consequent debarring from admission to any other institution</li>\r\n        			<li>Lodging of an FIR with the police based on severity.</li>\r\n        			<li>Collective punishment: when the persons committing or abetting the crime of ragging are not identified, the institution shall resort to collective punishment as a deterrent to ensure community pressure on  the potential raggers.</li>\r\n        		</ol>', '', '2019-12-17', '0000-00-00'),
(16, 11, 'Anti Ragging Squad', '<p>Several senior faculty members are nominated to the <strong>Anti-Ragging Squad.</strong> All students are encouraged to approach them without any hesitation in case of ragging.</p>', '', '2019-12-17', '0000-00-00'),
(17, 11, 'Code of conduct for faculty', '<ul>\r\n        			<li>Clear  Communication	with the students in writing the instructions for each assignment.</li>\r\n        			<li>Be updated in the knowledge of subject and the current affairs.</li>\r\n        			<li>Prepare thoroughly the lecture to be delivered in advance.</li>\r\n        			<li>Use of modern tools for teaching to make the lectures more interesting (Audio/ Visual Aids).</li>\r\n        			<li>Attendance of the students should be recorded regularly.</li>\r\n        			<li>Complete the syllabus in stipulated time.</li>\r\n        			<li>Conduct all the examinations as per the scheduled time table.</li>\r\n        			<li>Evaluate all the examinations in the stipulated time.</li>\r\n        			<li>To conduct examinations that minimize the opportunity for scholastic dishonesty.</li>\r\n        			<li>Be polite to the students-Listen their problems and should make efforts to solve them.</li>\r\n        			<li>Be in contact with the parents / guardians of the students.</li>\r\n        			<li>Take the prior approval of leave from respective authority and make the alternative arrangements for the class and practical.</li>\r\n        			<li>Follow the rules, regulations and instructions of the institute from time to time.</li>\r\n        			<li>To consistently be on guard for plagiarism</li>\r\n        		</ul>\r\n        		<p>The Following mentioned code of conduct is imperative for each faculty :</p>\r\n        		<ul>\r\n        			<li>A faculty member must believe that he / she has responsibility to shape  the future of the students and therefore the duties of a faculty member do not end by completing the subject course and leaving the rest to the students. It is to be understood that all students will not  be  self motivated. Such students may need regular counseling in various forms.    A faculty member is expected to continuously make efforts to devise new ways and means to counsel and motivate the  students  towards studies and career growth.</li>\r\n        			<li>In order to achieve this, a faculty member must take lectures with atmost preparation in theory and practical, examples of the subject and by using pictures and videos to explain the subject. Encourage  students  (if required; make compulsory for the students) turn by turn to participate and explain the subject in class during the discussion. Use English to the extent possible as medium of communication for such discussion.</li>\r\n        			<il>A quality and high standard teaching is only possible when a faculty member is dedicated to the profession, its students and the subject he / she is teaching. Dedication and motivation are complementary to each other. A dedicated faculty member must seek his future in teaching profession. A faculty member must display his / her dedication for the students so that it is felt by the students. Needles to say that although dedication is un-measurable and intangible but its impact can be felt.</il>\r\n        			<li>A faculty member who is supposed to be a good thinker must evolve methodology to improve the system, academic environment of the institute, suggest ways and means to do it.</li>\r\n        			<li>General counseling of the students is required and is the responsibility of each faculty. If the student appears to be not convinced from the counseling, he / she should be given full opportunity to put forward his point of view, inside or outside the classroom, and faculty member must act wiser to explain what is best in the interest of a student.</li>\r\n        			<li>Faculty member should not enter into arguments with students in front of everybody. Converse and communicate with the student the outcomes which he may face, today or in future jobs, due to the poor way of talking with the faculty members/seniors,etc.</li>\r\n        			<li>Many a times, it happens that the student is not always at fault. So, communicate politely and respectfully so that a good rapport with students gets maintained. In such situations, keep this proverb in mind:\r\n<i>“give respect to command respect”</i>.\r\n</li>\r\n<li>Behavior of the faculty member with the students should be such that it displays authority and command with love and affection for them. Ultimately faculty member should be able to convey to the students that they are being taken care for their all round growth.</li>\r\n<li>It is the duty of a faculty member to report any act of indiscipline noticed by him / her within the campus. Also, as far as possible, faculty member should interrupt in the act of indiscipline noticed by him / her and make an effort to bring a desired order and situation.</li>\r\n<li>A faculty member must be present within the department and / or within the academic area of the institution during the working hours and must avoid holding private meetings with other staff member / faculty member during the college hours to discuss the topics other than academics.</li>\r\n<li>Behavior of the faculty member with the fellow staff member / faculty member during the college hours, especially before the students, should   be very decent which could be set as an example to follow. He/she should not criticize fellow staff member / faculty member and the management especially before the students.</li>\r\n<li>A faculty member must follow law of the land and should not indulge himself / herself in an activity which can be detrimental to the reputation of the institution.</li>\r\n        		</ul>', '', '2019-12-17', '0000-00-00'),
(18, 11, 'Code of conduct for Governing Body', '<p><strong>Introduction</strong></p>\r\n        		<p>The responsibilities of the Governing Body, as the entity controlling a large and prestigious institution are onerous. The function of governance  is  to ensure that the organization fulfills its overall purpose, achieves its intended outcomes and operates in an efficient, effective, and ethical manner. This Code is intended to ensure that members are aware of and accept the responsibilities associated with coveted membership and follow high standards of ethical and professional conduct, as members of the Governing Body.\r\n</p>\r\n\r\n<p><strong>Objectives</strong></p>\r\n<p>The objectives of the Code are:</p>\r\n<ul>\r\n	<li>To set out decided pack of ethical ideologies.</li>\r\n	<li>To endorse and preserve the confidence and faith in the governing body of D Y Patil University.</li>\r\n	<li>To avert unethical practices.</li>\r\n	<li>To endorse compliance with best management practices in all the activities of the institution.</li>\r\n</ul>\r\n<p><strong>Conduct as Members</strong></p>\r\n<p>Members shall:</p>\r\n<ul>\r\n	<li>Treat each other, Institution’s staff and students with professionalism,\r\ncourtesy and respect.</li>\r\n<li>Not negatively influence other members.</li>\r\n<li>Participate actively and work co-operatively with fellow members in carrying out their responsibilities as members.</li>\r\n<li>Act at all times honestly and in good faith.</li>\r\n<li>Have a duty to maintain the confidentiality of information received in the course of their duties and not to use such information for any purpose outside that of understanding the work of the Board. </li>\r\n</ul>', '', '2019-12-17', '0000-00-00'),
(19, 11, 'Disciplinary rules', '<p>The following are examples of behavior which the institution finds  unacceptable. The list is not exhaustive and it is acknowledged that it will be necessary to exercise judgment in all cases and to be fair and reasonable in all the circumstances.</p>\r\n        		<ul>\r\n        		<li>Any form of physical/verbal violence towards students.</li>\r\n        		<li>Physical violence, actual or threatened towards other staff or visitors to  the Institution.</li>\r\n        		<li>Sexual	offences,	sexual	insults	or	sexual	discrimination	against students, other staff or visitors to the Institute</li>\r\n        		<li>Racial offences, racial insults or racial discrimination against students, other staff or visitors to the Institution.</li>\r\n        		<li>Theft of Institution, college (or) visitors money or property .Removal from Institution premises of property which is not normally taken away  without the express authority of the Principal or of the owner of the property may be regarded as gross misconduct.</li>\r\n        		<li>Deliberate falsification of documents such as time sheets, bonus sheets, subsistence and expense claims for the purpose of gain.</li>\r\n        		<li>Acceptance of bribes or other corrupt financial practices.</li>\r\n        		<li>Willful damage of Institution property or of property belonging to other staff or visitors to the Institution.</li>\r\n        		<li>Willful disregard of safety rules or policies affecting the safety of students, other staff or visitors to the Institution.</li>\r\n        		<li>Any	willful	act	which	could	result	in	actionable	negligence	for compensation against the Institution.</li>\r\n        		<li>•	Refusal to comply with reasonable instructions given by staff with a supervisory responsibility.</li>\r\n        		<li>Gross neglect of duties and responsibilities.</li>\r\n        		<li>Unauthorized absence from the work.</li>\r\n        		<li>Being untruthful and/or engaging in deception in matters of importance within the Institution community.</li>\r\n        		<li>Deliberate breaches of confidentiality particularly on sensitive matters.</li>\r\n        		<li>Being incapable by reason of alcohol or drugs (not prescribed for a health problem) from fulfilling duties and responsibilities of employment.</li>\r\n        		<li>Conduct which substantially brings the name of the Institution into disrepute or which seriously undermines confidence in the employee.</li>\r\n        		<li>Shall be regular and punctual for the classes.</li>\r\n        		<li>Shall take part in all activities connected with the department  and  college.</li>\r\n        		<li>Shall pay the tuition fee in time. Late fee will not be entertained. Defaulters name shall be removed from rolls without any further information.</li>\r\n        		<li>Shall read notices/circulars displayed on the college/department notice board and shall take cognizance thereof.</li>\r\n        		<li>Intimate the class-in-charge before taking leave. Absenting for internal tests and model examination is not entertained and shall have serious repercussions in academics.</li>\r\n        		<li>Use of mobile phones/other electronic gadgets such as ipod, iphone  within the classrooms, laboratories, library, seminar halls  and  auditorium is strictly prohibited. Violation of this rule by any student would result in impounding of these devices and strict disciplinary action for the violation.</li>\r\n        		<li>Students shall have to wear ID-card throughout the day in the campus.</li>\r\n        		<li>Students shall abide by the rules and regulations of the college for dress code.</li>\r\n        		<li>Shall not talk or act in any manner in a way that would bring disrepute   to the department	and college.</li>\r\n        		<li>Gathering in groups at roads, entrances, exit junctions and pathways is strictly prohibited.</li>\r\n        		<li>Smoking and consumption of any kind of alcoholic drinks/drugs inside the college is strictly prohibited. Intoxicated presence in the campus shall liable for disciplinary action.</li>\r\n        		<li>Any kind of celebration among groups of students in class room, canteen and other places of college campus is strictly banned and they may be permitted with special permission.</li>\r\n        		<li>Damaging the building, electric equipments like tube-lights, fans, switch boards, computer and accessories or any other properties of the college   in anyway is strictly prohibited. Shall attract punitive actions along with fine to the extent of damage.</li>\r\n        		<li>Don\'t indulge in ragging and don\'t be a mute witness to ragging. It is a criminal offence punishable under the Indian Penal Code. Strict disciplinary action will be taken against any student indulging  in  any form of ragging or eve-teasing inside the college premises or around.</li>\r\n        		<li>Cleanliness is next to Godliness; Hence present yourself as neatly and cleanly as possible.</li>\r\n        		<li>Students should assemble before commencement of the class. The latecomers will not be entertained. Parents shall get mobile SMS alert message in their registered mobile number in case of absence to  the  class.</li>\r\n        		<li>Students should assemble before commencement of the class. The latecomers will not be entertained. Parents shall get mobile SMS alert message in their registered mobile number in case of absence to  the  class.</li>\r\n        		<li>Wearing of Helmet is compulsory for riders in case of two-wheelers.</li>\r\n        		<li>Utilize the library in proper way and participate in extracurricular activities proactively.</li>\r\n        		<li>Shall maintain silence in the class room, library and computer labs. Adhere to the lab practices & manuals wherever necessary.</li>\r\n        		<li>Students are not encouraged to go outside the campus during the break and lunch hours. The department/college shall not take any  responsibility for the misbehavior of students outside the campus.</li>\r\n        		</ul>\r\n        		<p><strong>The following are examples of behavior which could lead to formal disciplinary warnings.</strong></p>\r\n        		<ul>\r\n        			<li>Unsatisfactory timekeeping without permission.</li>\r\n        			<li>Neglect of safety rules and procedures. Some offences of willful neglect may be regarded as gross misconduct.</li>\r\n        			<li>Breaches of confidentiality. Deliberate breaches on sensitive matters maybe regarded as gross misconduct.</li>\r\n        			<li>Failure to comply with reasonable work related requirements or lack of care in fulfilling the duties of the post.</li>\r\n        			<li>Behaviour towards other employees, students, and visitors which gives justifiable offence. Certain behaviour giving rise to offence may be regarded as gross misconduct.</li>\r\n        			<li>Acting in a manner which could reasonably be regarded as  rude, impolite, contemptuous or lacking appropriate professional demeanour. In certain circumstances such behavior may be regarded as gross misconduct.</li>\r\n        			<li>Conduct which is considered to be adversely affecting either the reputation of the institution or affects confidence in the employee. Such conduct may be regarded as gross misconduct.</li>\r\n        		</ul>', '', '2019-12-17', '0000-00-00'),
(20, 12, 'AICTE, New Delhi', 'AFFILIATIONS, RANKINGS & AWARDS', '003562ceda7842897e5bafcebb90b78e.png', '2019-12-17', '0000-00-00'),
(21, 12, '', 'DTE, Government <br>\r\nof Maharashtra', 'cee93bd08e26436c588adc08e430c679.png', '2019-12-17', '0000-00-00'),
(22, 12, '', 'Council of <br>\r\nArchitecture New Delhi', '5494dd90be3676fcdcc630dcf0b030a2.png', '2019-12-17', '0000-00-00'),
(23, 12, '', 'Pharmacy Council of', '9fbdc047fa7f6be2b253ce3f8e143b38.png', '2019-12-17', '0000-00-00'),
(24, 12, '', 'University Grants <br>\r\nCommission', 'a21b91ee08ff5e572d8c2a9a148e4f9c.png', '2019-12-17', '0000-00-00'),
(25, 12, '', 'MSBTE', '9566ba452c63e5182f541e1d3d39cb79.png', '2019-12-17', '0000-00-00'),
(26, 12, '', 'Rahuri Krushi Vidyapeeth', '76c282e6883a7806eb89ff8da3414b29.png', '2019-12-17', '0000-00-00'),
(27, 13, 'BOARD OF ADVISORS', '<p>Drawn from the pool of eminent thought leaders from across the country and cutting across disciplines, the Board of Advisors help DYPU Leadership to see interconnections and perceive the interplay that exists amongst diverse domain areas, and the directions they are likely to take. The University also ensures that students get to interact with these visionaries leading to the awakening of latent ideas in them through the wide range of perspectives they bring in.</p>', 'bc5c61e4f947c60ab674e13fda035471.png', '2019-12-17', '0000-00-00'),
(28, 13, 'INDUSTRY ADVISORY BOARD', '<p>The Industry Advisory Boards (IABs) are constituted to translate the core principle of Industry-linked into actions by involving industry in relevant processes and functions of the University as equal partners. The IAB helps in narrowing the gulf that exists between academia and industry by participating in the academic functions and working with faculty on research projects. An IAB is set up for each academic programme and the members participate in the full spectrum activities from admissions to placements. The IABs will advise the DYPU leadership on industry trends and directions. They are expected to be innovative forums that will create new mechanisms to help students have a deeper and involved interaction with the Industry.</p>', 'ded4934cfcaa307a1721376fb1583050.png', '2019-12-17', '0000-00-00'),
(29, 13, 'RESEARCH ADVISORY BOARD', '<p>In order to strengthen the research orientation of D Y PATIL  University and develop it into a hub of innovation, a Research Advisory Board (RAB) has been set up consisting well-known academic and industry researchers from across the world.</p>\r\n<p>These leading researchers will bring to DYPU their deep knowledge, academic rigour and research discipline in the technologies of the future such as Robotics, Cloud Computing, Scientific Research, Innovations in field of Medicine & Pharmaceuticals, Cyber Security among others.</p>\r\n<p>The aim of RAB will be to help drive the \'Research-driven\' core principle of DYPU. The Board will advise and actively engage with DYPU on all research related matters and enable it to foster linkages and partnerships with reputed international schools of higher learning as well as R&D labs in India and abroad. Select researchers, employed in these labs will be encouraged to take up visiting/ adjunct positions at DYPU. RAB will facilitate a two-way interaction between the labs and DYPU, helping Ph.D. students at the University to connect with researchers at the R&D labs. At a broader level, the RAB will help DYPU, outline its research agenda and priorities.</p>', 'bf5425a0a18460e3259925a1e72a0828.png', '2019-12-17', '0000-00-00'),
(30, 14, 'ADMINISTRATION', '<p>The Administration of the D Y Patil University, Pune looks after campus maintenance, non academic functioning and supervision of the overall environment. All the members of the administration are committed towards providing a student centric and comfortable environment for all the students. </p>\r\n				<p>The administration works in accordance with the academic staff and ensures a holistic experience on campus for all. Some of the key responsibilities of administration are : </p>', '', '2019-12-17', '0000-00-00'),
(31, 14, '', '<ul>\r\n	<li>Academic Process Regulations</li>\r\n	<li>Discipline on Top (Dot) Policy</li>\r\n	<li>Guidelines for Malpractice Cases</li>\r\n	<li>Policy on &quot;Grievances against Sexual Harassment&quot;</li>\r\n	<li>Committee for Protection Against Sexual Harassment</li>\r\n	<li>Office Order Issuance of Duplicate ID Cards</li>\r\n	<li>Policy on Plagiarism</li>\r\n	<li>Policy and Procedures for Student Attendance Monitoring and Engagement</li>\r\n	<li>Selling of Books and Other Materials to First Year Students</li>\r\n	<li>Restriction on Mode of Interaction between students for the First and Those from Second Third and Final Year</li>\r\n	<li>Student Manual on Campus Code of Ethics and Conduct along with Standard Procedure</li>\r\n	<li>MUJ Student Council of Constitution</li>\r\n	<li>MUJ - Student Council Elections</li>\r\n	<li>Faculty-Wise Student Council Advisory Board For The Scrutiny Of Office Bearer Nomination Forms: Guidelines For Evaluation</li>\r\n	<li>Faculty-Wise Student Council Election 2019-20 List Of School Wise Election Coordinators</li>\r\n</ul>', '1583755852a4893a3bfca42ae6bef9b7.png', '2019-12-17', '0000-00-00'),
(32, 15, 'UNIVERSITY REGULATIONS', '<p>D Y Patil University  aims at providing globally accepted education of high standards. In all programs of study, great emphasis is placed on the use of modern communication technology to impart quality education to students.</p>\r\n				<p>The University follows an efficient and flexible semester system with continuous and comprehensive evaluation</p>', '', '2019-12-17', '0000-00-00'),
(33, 15, 'FOLLOWING ARE THE ACADEMIC RULES, REGULATIONS AND GUIDELINES:', '<ol>\r\n					<li>Academic Process Regulations</li>\r\n<li>Discipline on Top (Dot) Policy </li>\r\n<li>Guidelines for Malpractice Cases</li>\r\n<li>Policy on \"Grievances against Sexual Harassment\" </li>\r\n<li>Committee for Protection Against Sexual Harassment</li>\r\n<li>Office Order Issuance of Duplicate ID Cards</li>\r\n<li>Policy on Plagiarism</li>\r\n<li>Policy and Procedures for Student Attendance Monitoring and Engagement</li>\r\n<li>Selling of Books and Other Materials to First Year Students</li>\r\n<li>Restriction on Mode of Interaction between students for the First and Those from Second Third and Final Year </li>\r\n<li>Student Manual on Campus Code of Ethics and Conduct along with Standard Procedure</li>\r\n<li>MUJ Student Council of Constitution </li>\r\n<li>MUJ - Student Council Elections</li>\r\n<li>Faculty-Wise Student Council Advisory Board For The Scrutiny Of Office Bearer Nomination Forms: Guidelines For Evaluation\r\n<li>Faculty-Wise Student Council Election 2019-20 List Of School Wise Election Coordinators</li>\r\n				</ol>', '0e1c74e95f6a69f4b3b7a063178628fc.png', '2019-12-17', '0000-00-00'),
(34, 47, 'REGISTRAR', '<p>The Highest education does not  merely give us information but makes us live in harmony with all forms of existence.Textbooks inform and instruct but you have to go beyond that to impart wholesome education.D Y Patil University, Pune a pioneer in quality par excellence education stands tall as an emissary and has set an example for others to follow.</p>\r\n\r\n<p>By adopting latest technology in teaching methodology and providing extensive industrial exposure .This University nurtures young minds and moulds them into professionals,ready to take on the world.</p>\r\n\r\n<p>As a registrar, I ensure that the entire University team leaves no stone unturned in its endeavour of providing excellent academic as well as industrial training to the students. The Registrar is responsible for performing functions laid down by the UGC.I am responsible for:</p>\r\n\r\n<ol>\r\n	<li>\r\n		<p>The Registrar shall, be the Chief Administrative Officer of the university. He shall be a full-time                                           salaried officer and shall work directly under the superintend- dence, direction and control of the Vice-Chancellor.</p>\r\n	</li>\r\n\r\n	<li><p>The qualifications and experience for the purpose of selection of the Registrar shall be as laid down by the University Grants Commission and approved by the State Government.</p></li>\r\n\r\n	<li><p>The Registrar shall be appointed by the Vice-Chancellor on the recommendation of a selection committee constituted for the purpose under this Act.</p></li>\r\n\r\n	<li><p>Appointment of the Registrar shall be for a term of five years or till he attains the age of superannuation whichever is earlier and he shall be eligible for re-appointment by selection on the recommendation of a selection committee constituted for the purpose, for only one more term of five years in the university in which he is serving:</li>', '67cb9aefc8391248073f112651a97b7f.jpg', '2019-12-30', '0000-00-00'),
(35, 48, 'Controller of Examination', '<p>The hallmark of an ideal educational institution is to provide quality education. It should consist of a series of enhancements each raising the individual to a higher level of awareness, understanding and kinship leading to wholesome development of an individual and thereby contribute to the productive development of the university. In this regard university is proud to have highly technological savvy examination department which controls the entire process of student assessment right from setting up a panel of internal and external paper setters, to selecting the best of internal and external examiners & moderator and ensuring a correct student assessment.</p>\r\n\r\n<p>We, at Examination Section, D Y Patil, Pune University, envisage the need for radical reforms in traditional examination and assessment system and are working towards evolving a credible, valid, effective and transparent evaluation system that responds confidently to the challenges and newer demands of a knowledge society.</p>\r\n\r\n<p>We have set ourselves for adopting technology to facilitate easy interface for exchange of information.</p>\r\n\r\n<ul>	\r\n\r\n	<li><p>Devise and monitor strategies and implementation of useful examination practices at colleges/institutes.</p></li>\r\n\r\n	<li><p>Online mode of examination form filling that will help generate student summary & Hall Tickets to optimize time and eliminate errors.</p></li>\r\n\r\n	<li><p>Provide guidelines for creating and including questions in the banks which then can act as the sole source for question paper generation.</p></li>\r\n\r\n	<li>\r\n		<p>Create question banks for various subjects and courses to facilitate random selection of question papers.</p>\r\n	</li>\r\n\r\n	<li><p>Inform and train teaching and non-teaching faculty members about the conduct and organization of examination and examination material.</p></li>\r\n\r\n	<li><p>Strengthen Student Facilitation Centre for prompt response and delivery of student queries.</p></li>\r\n	<li><p>Coordinate with all other higher and regulatory agencies to ensure credibility and stability to examination system.</p></li>\r\n\r\n</ul>', '37b0b6e0c8a34454a4ee3dc3dcb8207f.jpg', '2019-12-30', '0000-00-00');
INSERT INTO `about_us` (`id`, `page_name_id`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(36, 49, 'Finance and Accounts Officer', '<p>The importance of Finance Office in DYPU Pune  is to effectively interact with all the Departments of the University, evaluate the Departmental Activities in terms of their financial needs, review the requirements in appropriate committees, render the Costing/Financial Assistance to generate Internal Resources . the Principles of Regularity in the scrutiny of Financial Transactions, protect the Financial Interests of the University by effecting proper recovery of the University/Government dues and its settlement through proper and timely remittances to the concerned Authorities, compile the Expenditure/Receipts through proper Journalâ€™s, Broad-sheets, Ledgers and other Books of Account, provide Financial Information to the Management  at prescribed intervals of time in regular formats, bring out the Final Accounts in various formats, get them audited in time by Statutory Auditors, ensure its submission to Government through authorized Committees/Bodies, Co-ordinate with Auditors and the Departments to achieve compliance with regard to Audit Observations etc.</p>\r\n\r\n<p><strong>Duties and Responsibility of University Finance Officer</strong></p>\r\n<ul>\r\n	<li><p>Financial Advisor</p></li>\r\n\r\n	<li><p>Preparation of budget and budgetary control</p></li>\r\n\r\n	<li><p>Preparation of accounts of the University</p></li>\r\n\r\n	<li><p>Audit of accounts</p></li>\r\n\r\n	<li><p>Financial systems and procedures followed in the University</p></li>\r\n\r\n	<li><p>Member secretary of the Finance Committee</p></li>\r\n\r\n	<li><p> Member of the Executive Committee </p> </li>\r\n<li><p>Administrative/Financial powers delegated by the University </p> </li>\r\n<li><p>Assisted by an Accounts/Technical Officer, Internal Audit Officer, Office Assistants etc.,</p></li>\r\n</ul>', '4135ca87845b8ee1c8a1cebd697314b3.jpg', '2019-12-30', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `academic_calendar`
--

CREATE TABLE `academic_calendar` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `commencement_end_of_term` varchar(255) NOT NULL,
  `first_term` varchar(255) NOT NULL,
  `second_term` varchar(255) NOT NULL,
  `vacation` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_calendar`
--

INSERT INTO `academic_calendar` (`id`, `department_id`, `commencement_end_of_term`, `first_term`, `second_term`, `vacation`, `date_created`, `date_modified`) VALUES
(1, 17, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to 30th May 2021', '', '2019-12-18', '2019-12-18'),
(2, 17, 'Architecture Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to 30th May 2021', '', '2019-12-18', '0000-00-00'),
(3, 17, '1st Class Test', '16th Aug 2020 to 25th Aug. 2020', '3rd Feb 2021 to 07th Feb 2021', '', '2019-12-18', '0000-00-00'),
(4, 17, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-18', '0000-00-00'),
(5, 17, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020	', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-18', '0000-00-00'),
(6, 17, '2nd Parent teachers meeting', '31st Oct 2020', '21st March. 2021', '', '2019-12-18', '0000-00-00'),
(7, 17, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-18', '0000-00-00'),
(8, 17, '', '', '', 'Winter Vacation - 10th Dec to 5th Jan.2021', '2019-12-18', '0000-00-00'),
(9, 17, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-18', '0000-00-00'),
(10, 17, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-18', '0000-00-00'),
(11, 18, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to 30th May 2021', '', '2019-12-19', '0000-00-00'),
(12, 18, 'Pharmacy Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to 30th May 2021', '', '2019-12-19', '0000-00-00'),
(13, 18, '1st Class Test', '16th Aug 2020 to 25th Aug. 2020', '3rd Feb 2021 to 07th Feb 2021', '', '2019-12-19', '0000-00-00'),
(14, 18, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-19', '0000-00-00'),
(15, 18, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-19', '0000-00-00'),
(16, 18, '2nd Parent teachers meeting', '31st Oct 2020', '21st March. 2021', '', '2019-12-19', '0000-00-00'),
(17, 18, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-19', '0000-00-00'),
(18, 18, '', '', '', 'Winter Vacation - 10th Dec to 5th Jan.2021', '2019-12-19', '0000-00-00'),
(19, 18, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-19', '0000-00-00'),
(20, 18, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-19', '0000-00-00'),
(21, 16, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-19', '0000-00-00'),
(22, 16, 'Civil & Infrastructural Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-19', '0000-00-00'),
(23, 16, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-19', '0000-00-00'),
(24, 16, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-19', '0000-00-00'),
(25, 16, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-19', '0000-00-00'),
(26, 16, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-19', '0000-00-00'),
(27, 16, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-19', '0000-00-00'),
(28, 16, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-19', '0000-00-00'),
(29, 16, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-19', '0000-00-00'),
(30, 16, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-19', '0000-00-00'),
(31, 19, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(32, 19, 'Business Management Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(33, 19, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(34, 19, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(35, 19, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(36, 19, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(37, 19, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(38, 19, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(39, 19, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(40, 19, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(41, 20, 'Hospitality & Tourism Studies Sem-I ', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(42, 20, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(43, 20, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(44, 20, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(45, 20, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(46, 20, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(47, 20, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(48, 20, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(49, 20, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(50, 22, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(51, 22, 'Mechanical Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(52, 22, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(53, 22, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(54, 22, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(55, 22, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(56, 22, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(57, 22, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(58, 22, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(59, 22, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(60, 21, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(61, 21, 'Electrical & Electronics Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(62, 21, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(63, 21, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(64, 21, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(65, 21, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(66, 21, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(67, 21, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(68, 21, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(69, 21, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(70, 23, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(71, 23, 'Information Technology Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(72, 23, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(73, 23, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(74, 23, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(75, 23, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(76, 23, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(77, 23, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(78, 23, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(79, 23, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(80, 24, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(81, 24, 'Computer & Information Science Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(82, 24, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(83, 24, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(84, 24, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(85, 24, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(86, 24, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(87, 24, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(88, 24, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(89, 24, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(90, 25, 'Orientation for First Years', '3rd July 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(91, 25, 'Automobile Engineering Sem-I', '7th July 2020  to 7th Dec 2020', '6th Jan 2021 to  30th May 2021', '', '2019-12-26', '0000-00-00'),
(92, 25, '1st Class  Test', '16th Aug  2020 to 25th Aug. 2020', '3rd  Feb 2021 to 07th Feb 2021', '', '2019-12-26', '0000-00-00'),
(93, 25, '1st Parent teachers meeting', '30th Aug.2020', '15th Feb. 2021', '', '2019-12-26', '0000-00-00'),
(94, 25, '2nd Class Test', '6th Oct 2020 to 13th Oct 2020', '9th  Mar.2021 to 13th Mar.2021', '', '2019-12-26', '0000-00-00'),
(95, 25, '2nd Parent teachers meeting', '31st  Oct 2020', '21st March. 2021', '', '2019-12-26', '0000-00-00'),
(96, 25, 'Term End Exam', '11th Nov 2021 to 7th Dec 2021', '4th May2021 to 30th May 2021', '', '2019-12-26', '0000-00-00'),
(97, 25, '', '', '', 'Winter Vacation -  10th Dec to 5th Jan.2021', '2019-12-26', '0000-00-00'),
(98, 25, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00'),
(99, 25, '', '', '', 'Summer Vacation from  16th May To 05th July 2022', '2019-12-26', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `academic_calendar_header`
--

CREATE TABLE `academic_calendar_header` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `heading_first` varchar(255) NOT NULL,
  `heading_second` varchar(255) NOT NULL,
  `heading_third` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_calendar_header`
--

INSERT INTO `academic_calendar_header` (`id`, `department_id`, `session`, `heading_first`, `heading_second`, `heading_third`, `date_created`, `date_modified`) VALUES
(1, 17, 'Academic Calendar 2020-21', 'Commencement & End of Term', 'First Term', 'Second Term ', '2019-12-18', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `admission_process`
--

CREATE TABLE `admission_process` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `document` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission_process`
--

INSERT INTO `admission_process` (`id`, `title`, `description`, `document`, `date_created`, `date_modified`) VALUES
(2, 'ADMISSION PROCESS', '<p>We welcome applications from around the country and it is our aim to make the process of applying smooth& hassle free for the students. Here is a step by step guide that will explain the process of applying to our university</p>', '2baa403b7f54c5279ddbc75fe17eeb43.pdf', '2019-12-26', '0000-00-00'),
(3, 'APPLY EARLY', '<p>Seats are limited; hence students should apply for courses well in advance. The application process for all the courses will begin from January 2020 and students can apply on <a href=\"#\" target=\"_blank\">www.dypatiluniversitypune.edu</a> Or call (+91)8448449859. You can take up virtual tour of the university to understand about your course, where you will live, the experiences you will gain, the people you will meet & the facilities you will get</p>', '', '2019-12-26', '0000-00-00'),
(4, 'COMPLETE THE APPLICATION FORM', '<p>Fill the online application form and mark the courses that you wish to apply for. The cost of applying for undergraduate programs is INR 1550, postgraduate programs is INR2150 & doctoral programs is INR 3150. Scan and attach your recent photograph and valid address proof along with the application form. SUBMIT YOUR APPLICATION.</p>', '', '2019-12-26', '0000-00-00'),
(5, 'ACKNOWLEDGEMENT OF APPLICATION', '<p>We will send you an acknowledgement email, which will explain how to check the status of your application. We will call you in case we require any additional information. </p>', '', '2019-12-26', '0000-00-00'),
(6, '', '<p>Once your application has been received, it will be considered and we might ask you to come and meet us for an interview/Group Discussion/Written Test or undertake the same online. We will further analyse your application to ensure you meet the eligibility criteria for the program(s) you are applying for. </p>\r\n\r\n                    <p>We consider your academic history and relevant experience to ensure you can succeed in your program. The assessment process usually ranges between 2-10 working days. Once the process is complete, we will get in touch with you and let you know the status of your application. </p>\r\n', '', '2019-12-26', '2019-12-26'),
(7, 'POSSIBLE OUTCOMES', '<p>We will get in touch with you and let you know the status of your application. The following will be the outcomes: </p>\r\n\r\n                    <p><strong>CONFIRMED : </strong>You have been granted a confirmed seat and you can claim it by submitting the relevant documents and paying the tuition fee </p>\r\n\r\n                    <p><strong>PROVISIONAL :</strong> You have met some of the eligibility requirements for entry to your chosen program of study and we would like to offer you a provisional seat till you satisfy the rest of the requirements in a stipulated time frame. If you are unable to clear the eligibility requirements, your admission will stand cancelled. </p>\r\n\r\n                    <p><strong>CONDITIONAL :</strong> You have to meet the conditions and approvals listed in your conditional offer in order to claim a formal offer from the University. \r\nDECLINED –If you don’t meet the entry requirements or all seats are taken , we might offer you an alternative program. </p>', '', '2019-12-26', '0000-00-00'),
(8, 'ACCEPTANCE OF OFFER', '<p>Once you accept our offer, we will require your documents and once the documents have been verified , you will be required to pay the tuition fee.</p>', '', '2019-12-26', '0000-00-00'),
(9, 'EASY FINANCE', '<p>We offer student friendly installment schemes & also give the facility of student loan. You can learn more about it on <a href=\"#\" target=\"_blank\">www.dypatiluniversitypune.edu</a></p>', '', '2019-12-26', '0000-00-00'),
(10, 'WELCOME', '<p>You will be invited to our Orientation Day program, during which you can complete your registration process, meet your faculties and new classmates, visit your department, check into your residence, explore the university and most importantly have fun! </p>', '', '2019-12-26', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `page_id`, `image`, `date_created`, `date_modified`) VALUES
(1, 43, 'ee0714f51b63daedd5ae587d3acfbc6d.png,1de00c3b88764235375682f9150e6b1f.png,82e3ac0ba992a9d1c23bde7b3c25952e.png', '2019-12-27', '0000-00-00'),
(2, 4, 'f495b9a0dc40a52cd45931948136550b.jpg', '2019-12-27', '0000-00-00'),
(3, 8, '4ab670bf886e2b301d5f7e064ceb0ce6.jpg', '2019-12-27', '0000-00-00'),
(4, 9, '409a25607acbdd8c8c3f0cb8b2e0aef4.png', '2019-12-27', '0000-00-00'),
(5, 10, 'b3e2f432c0fb5499f24376cd603e8fbc.png', '2019-12-27', '0000-00-00'),
(6, 11, '1b596c5234f258b7eb179b3850db7856.png', '2019-12-27', '0000-00-00'),
(7, 12, '1b3ec8bd123ee5f3284fa14cbc38a34e.jpg', '2019-12-27', '0000-00-00'),
(8, 13, '4c2eb6b3de563e4cb6f99a7a5e91186b.jpg', '2019-12-27', '0000-00-00'),
(9, 14, 'e91eb020185c826c23b83b0c5b93fa1e.png', '2019-12-27', '0000-00-00'),
(10, 15, 'e4748f99e3b3f0bb9cebf83305251468.png', '2019-12-27', '0000-00-00'),
(11, 44, '5399ad274464feaea5dd99dee5ed87f5.png', '2019-12-27', '0000-00-00'),
(12, 16, '790c02d28d18940b5d4fc99580fe7b1d.jpg', '2019-12-27', '0000-00-00'),
(13, 21, 'a7cd26fee8b3943d237f96ef925f9afb.jpg', '2019-12-27', '0000-00-00'),
(14, 22, 'a1862af038d2130204c35759cf260f94.jpg', '2019-12-27', '0000-00-00'),
(15, 23, '95605894c915c23481a6cb90840fe563.jpg', '2019-12-27', '0000-00-00'),
(16, 24, '7c53e8b551696db57c349df5bc293d14.jpg', '2019-12-27', '0000-00-00'),
(17, 25, '1ed2cae7318d2f37402398780617f805.jpg', '2019-12-27', '0000-00-00'),
(18, 17, 'f303cfd94e6cae330211172f1bf653fd.jpg', '2019-12-27', '0000-00-00'),
(19, 18, '92f85bd03572d8645318b3111e14c801.jpg', '2019-12-27', '0000-00-00'),
(20, 19, '8d34da6ce969c2e977eeee136254cb7a.jpg', '2019-12-27', '0000-00-00'),
(21, 20, '10ea769c900336598e39df0824ef2ff0.jpg', '2019-12-27', '0000-00-00'),
(22, 45, '8b17681342ce4455f29627c911f9b005.jpg', '2019-12-27', '0000-00-00'),
(23, 46, '758dd7e316dc9aefc11922af9175ae8c.jpg', '2019-12-27', '0000-00-00'),
(24, 26, '0a8d7504fd805654746773510e5d350f.jpg', '2019-12-27', '0000-00-00'),
(25, 27, 'a451102d9c8dae4834501f597bbbbfd7.jpg', '2019-12-27', '0000-00-00'),
(26, 28, '69f4285347d3382b26208b03454c3173.jpg', '2019-12-27', '0000-00-00'),
(27, 29, '1b89be80bafe2e64e3377411c5963743.jpg', '2019-12-27', '0000-00-00'),
(28, 30, 'e061c7a46d40a95e7cd178e1fd965cec.jpg', '2019-12-27', '0000-00-00'),
(29, 31, 'a30cf3e2db95d7599c69cb8bbbbb9a41.jpg', '2019-12-27', '0000-00-00'),
(30, 32, '1670647391ac39641cfdfd8942a86f5d.jpg', '2019-12-27', '0000-00-00'),
(31, 34, '881063cda168fb0a1aa18b9f4562ee54.jpg', '2019-12-27', '0000-00-00'),
(32, 35, 'b73aad90e3d79face93d727e6e69630e.jpg', '2019-12-27', '0000-00-00'),
(33, 36, 'af1bf3b53407b31217183c56b21c363c.jpg', '2019-12-27', '0000-00-00'),
(34, 37, '1560f63e239ef16f3c6ed877c05b97be.jpg', '2019-12-27', '0000-00-00'),
(35, 41, '6a232bb3f6f16f319e25d07ef774e29a.jpg', '2019-12-27', '0000-00-00'),
(36, 42, '7dccfd857d98937b4871969481f15aa2.jpg', '2019-12-27', '0000-00-00'),
(37, 38, '027b2fdd078cb2445b59682556ced46a.jpg', '2019-12-27', '0000-00-00'),
(38, 39, '4249380cf22a76214195283e36ea193f.jpg', '2019-12-27', '0000-00-00'),
(39, 40, 'a7987c370ce18812031257d58b2b96d6.jpg', '2019-12-27', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `committees`
--

CREATE TABLE `committees` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `committee_name` varchar(255) NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `committees`
--

INSERT INTO `committees` (`id`, `title`, `description`, `committee_name`, `no_of_members`, `date_created`, `date_modified`) VALUES
(1, 'COMMITTEES', '<p>The University abides by the rules and regulations laid down by the respective councils and governing bodies. For smooth and efficient governance, the University has various committees that look into different administrative, curricular and co-scholastic functions.</p>\r\n\r\n<p>All the committees have a Chairperson and are constituted by faculties as members. In some committees, student representatives are also made members so that students can openly discuss issues(if any) with the students. The University has the following committees :</p>', '', 0, '2019-12-17', '2019-12-17'),
(2, '', '', 'Internal Quality Assurance Cell (IQAC)', 15, '2019-12-17', '0000-00-00'),
(3, '', '', 'Anti Ragging Committee', 12, '2019-12-17', '0000-00-00'),
(4, '', '', 'Gender Committee', 9, '2019-12-17', '0000-00-00'),
(5, '', '', 'Discipline Committee', 11, '2019-12-17', '0000-00-00'),
(6, '', '', 'Sports Committee', 17, '2019-12-17', '0000-00-00'),
(7, '', '', 'Cultural Committee', 20, '2019-12-17', '0000-00-00'),
(8, '', '', 'Grievance Redressal  Committee', 11, '2019-12-17', '0000-00-00'),
(9, '', '', 'Examination Committee', 15, '2019-12-17', '0000-00-00'),
(10, '', '', 'Safety Committee', 11, '2019-12-17', '0000-00-00'),
(11, '', '', 'Library Committee', 15, '2019-12-17', '0000-00-00'),
(12, '', '', 'Research Committee', 21, '2019-12-17', '0000-00-00'),
(13, '', '', 'Attendance Committee', 17, '2019-12-17', '0000-00-00'),
(14, '', '', 'Student Welfare Committee', 12, '2019-12-17', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `page_id`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(1, 30, '24<span>X</span>7 LIBRARY', '<p>Books, Encyclopedias, Journals, Reference Books, E books, E journals of various disciplines are available in the library of the campus. To facilitate learning, library is open round the clock so that students can study whenever they want. The extensive E Data base of the library is an added advantage for students as they can refer to renowned international journals at one click. </p>', 'e9e32826e2afcbc1c11c969e6e8adb12.jpg', '2019-12-23', '0000-00-00'),
(2, 31, 'TRANSPORTATION', '<p>We offer bus facility to all our students from Pune railway station. This is an effort to ensure a hassle free and smooth travel for all our day scholars.  Students enjoy their ride to the campus as it gives them time to mingle with each other, establish friendships and make mesmerizing memories. </p>', '213b7aa19173dbcf9cc757424fbad2ec.jpg', '2019-12-23', '0000-00-00'),
(3, 32, 'STUDENT RESIDENCES', '<p>Our campus is equipped with state of the art, comfortable hostels that never let students miss home. The hostels can accommodate 5000+ students and the rooms are available in various categories as per the needs of students. </p>\r\n					<p>The hostel premises have been designed by renowned architects who have ensured that a safe, convenient hostel is available for the students. All the rooms have a beautiful view of the hills, are well ventilated, spacious and student friendly. </p>', 'c99ef6ffd4afb0f06898d38111c09e0d.jpg', '2019-12-23', '0000-00-00'),
(4, 32, '', '', '39676c67dae02ecec1319a9f43fcf897.jpg', '2019-12-23', '0000-00-00'),
(5, 33, 'CAFETERIA', '<p>With our 24*7 open restaurant, students have the ease to eat whatever delicacy they want round the clock. The menu of the café is elaborate and includes fast food, sumptuous meals, home style food as well as beverages. </p>', '', '2019-12-23', '0000-00-00'),
(6, 33, '', '', '99ce6f8c943d8e8e7321ddac41af28bc.jpg', '2019-12-23', '0000-00-00'),
(7, 34, 'BANKING', '<p>The campus houses an ATM machine through which students can carry out basic banking functions in the campus itself .</p>\r\n', '', '2019-12-23', '0000-00-00'),
(8, 34, '', '', '121e935adb79ee0db309ffebb9096b05.jpg', '2019-12-23', '0000-00-00'),
(9, 35, 'GYMNASIUM', '<p>The campus houses a fully equipped workout area and yoga zone where students can exercise and work towards building a healthy body and mind. Meditation and yoga sessions help the students to tackle academic pressure and infuse a sense of optimism in their life. The gym is fully equipped with advanced workout machines and the trainers monitor students as well as motivate them to exercise properly and regularly.</p>\r\n', '', '2019-12-23', '0000-00-00'),
(10, 35, '', '', '2fb67c2b4f6747f1db4a3e677732e139.jpg', '2019-12-23', '0000-00-00'),
(11, 36, 'HEALTHCARE', '<p>We care about you and your health is our priority. Our campus has 24*7 health care facilities for all the students. We have a resident doctor on campus who can administer to students.</p>\r\n', '', '2019-12-23', '0000-00-00'),
(12, 36, '', '', '024aa504d6cc95a430a71d8cc9e72df3.jpg', '2019-12-23', '0000-00-00'),
(13, 37, 'SPORTS', '<p>We offer our students a plethora of indoor as well as outdoor sports on campus. A stadium size ground is available for the students in which regular cricket matches, volleyball matches, soccer matches are held. Students also enjoy playing pool , billiards, chess, carom at the indoor sports centre.</p>\r\n', '', '2019-12-23', '0000-00-00'),
(14, 37, '', '', '84d060d857c8161618f058313f44a0da.jpg', '2019-12-23', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `dpt_id` int(11) NOT NULL,
  `faculty_name` varchar(255) NOT NULL,
  `contact_no` double NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `specialization` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `dpt_id`, `faculty_name`, `contact_no`, `email_id`, `specialization`, `date_created`, `date_modified`) VALUES
(1, 51, 'Mr. Ashish Mulajkar', 8421921161, 'ashish.mulajkar@dyptc.edu.in', 'Embedded Systems,vlsi Design', '2019-12-30', '2019-12-30'),
(2, 51, 'Mr.Santosh Bari', 8855873890, 'santosh.bari@dyptc.edu.in', 'Digital Communication Systems', '2019-12-30', '0000-00-00'),
(3, 51, 'Mr.Mahendra Patil', 9763343558, 'mahendra.patil@dyptc.edu.in', 'Communication Engineering', '2019-12-30', '0000-00-00'),
(4, 51, 'Ms.Priti Rajput', 7875959047, 'priti.rajput@dyptc.edu.in', 'Digital Electronics', '2019-12-30', '0000-00-00'),
(5, 51, 'Ms.Vaishali Thorat', 9762893900, 'vaishali.thorat@dyptc.edu.in', 'Vlsi Design', '2019-12-30', '0000-00-00'),
(6, 51, 'Mr.Sandeep Shelke', 9823468084, 'sandIp.shelke@dyptc.edu.in', 'Vlsi & Es', '2019-12-30', '0000-00-00'),
(7, 51, 'Mr.Suresh Rode', 9890728472, 'suresh.rode@dyptc.edu.in', 'Vlsi & Es', '2019-12-30', '0000-00-00'),
(8, 51, 'Mr.Kartik Argulwar', 8275287578, 'kartik.argulwar@dyptc.edu.in', 'Communication Engineering', '2019-12-30', '0000-00-00'),
(9, 51, 'Mr.S.G.Udge', 9421368129, 'sharnappa.udge@dyptc.edu.in', 'Electrical Power Systems', '2019-12-30', '0000-00-00'),
(10, 22, 'Mr. Ashwin G. Ghaysundar', 8600375297, 'ashwin.ghaysundar@dyptc.edu.in', 'CAD/CAM', '2019-12-30', '0000-00-00'),
(11, 22, 'Mrs. Asmita A. Bagade', 9860876218, 'asmita.bagade@dyptc.edu.in', 'Production Engineering', '2019-12-30', '0000-00-00'),
(12, 22, 'Mr. Ashwinkumar K. Dubey', 9011688930, 'ashwin.dubey@dyptc.edu.in', 'Tribology Status', '2019-12-30', '0000-00-00'),
(13, 22, 'Mr. Avinash S. Patil', 7741024444, 'avinash.patil@dyptc.edu.in', 'Heat Power', '2019-12-30', '0000-00-00'),
(14, 51, 'Mr.Asif Shaikh', 9960948386, 'asif.shaikh@dyptc.edu.in', 'Vlsi & Es', '2019-12-30', '0000-00-00'),
(15, 22, 'Mr. Dnyaneshwar. S. Nakate', 9921555470, 'dnyanshwar.nakate@dyptc.edu.in', 'Heat Power', '2019-12-30', '0000-00-00'),
(16, 22, 'Mr. Anant B. Khandkule', 9604254190, 'khandkule.anant@dyptc.edu.in', 'Design Engineering', '2019-12-30', '0000-00-00'),
(17, 22, 'Mr. Bhushan S. Rane', 9967279317, 'bhushan.rane@dyptc.edu.in', 'Heat Power', '2019-12-30', '0000-00-00'),
(18, 22, 'Mr. Sagar B. Shinde', 9970039933, 'sagarb.shinde@dyptc.edu.in', 'CAD/CAM', '2019-12-30', '0000-00-00'),
(19, 22, 'Mr. Vishwanath S. Chavan', 8793979897, 'vishwanath.chavan@dyptc.edu.in', 'Design Engineering', '2019-12-30', '0000-00-00'),
(20, 22, 'Mr. Rohan R. Katwate', 9860278747, 'rohan.katwate@dyptc.edu.in', 'Heat Power', '2019-12-30', '0000-00-00'),
(21, 22, 'Mr. Ravi Honnebagera C.', 9900127129, 'ravi.c@dyptc.edu.in', 'Thermal Engineering', '2019-12-30', '0000-00-00'),
(22, 22, 'Mr. Amit P. Kharche', 9420455060, 'amit.kharche@dyptc.edu.in', 'CAD/CAM', '2019-12-30', '0000-00-00'),
(23, 22, 'Mr. Sagar. B. Patil', 7588587577, 'sagar.patil@dyptc.edu.in', 'Mechanical Design Engineering', '2019-12-30', '0000-00-00'),
(24, 22, 'Mr. Vijay Bagali', 7387354817, 'vijay.bagali@dyptc.edu.in', 'Production Engineering', '2019-12-30', '0000-00-00'),
(25, 52, 'Dr. R.V.Kherde', 9922460820, 'rajesh.kherde@dyptc.edu.in', 'Water Resources Management', '2019-12-30', '0000-00-00'),
(26, 52, 'Mr. Sudhir A. Shinde', 9763312066, 'sudhir.shinde@dyptc.edu.in', 'Const. & Mang.', '2019-12-30', '0000-00-00'),
(27, 52, 'Mr. Vikas S. Mane', 9890954004, 'vikas.mane@dyptc.edu.in', 'Geotech. Engg.', '2019-12-30', '0000-00-00'),
(28, 52, 'Mr. Arjun A. Jadhav', 9518506534, 'arjun.jadhav@dyptc.edu.in', 'Geology', '2019-12-30', '0000-00-00'),
(29, 52, 'Mr. Yuvraj B. Kshirsagar', 9975534030, 'yuvraj.kshirsagar@dyptc.edu.in', 'Env. Engg.', '2019-12-30', '0000-00-00'),
(30, 52, 'Mr. Puneet Upadhye', 8329710545, 'puneet.upadhye@dyptc.edu.in', 'Structures', '2019-12-30', '0000-00-00'),
(31, 52, 'Mr. Abhishek P. Nimbekar', 9850794019, 'abhishek.nimbekar@dyptc.edu.in', 'Structures', '2019-12-30', '0000-00-00'),
(32, 52, 'Mr. Pravin S. Patil', 9607668982, 'pravin.spatil@dyptc.edu.in', 'Structures', '2019-12-30', '0000-00-00'),
(33, 52, 'Miss. Swati M. Sanap', 7720924662, 'swati.sanap@dyptc.edu.in', 'Const. & Mang.', '2019-12-30', '0000-00-00'),
(34, 52, 'Miss. Asavari Y. Patil', 7219167306, 'asavari.patil@dyptc.edu.in', 'Const. & Mang.', '2019-12-30', '0000-00-00'),
(35, 52, 'Mrs. Gauri A. Jewalikar', 9028519637, 'gauri.jewalikar@dyptc.edu.in', 'Structures', '2019-12-30', '0000-00-00'),
(36, 25, 'Dr. Avinash G.Patil', 9823688488, 'avinash.g.patil@dyptc.edu.in', 'Ph.d. (Mechanical Engg.) M.E. (Mech-Design Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(37, 25, 'Mr. Swapnil G.Fegade', 9860705146, 'swapnil.fegade@dyptc.edu.in', 'Ph.d. (Mechanical Engg. Pursuing) M.E. (Mech-Design Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(38, 25, 'Mr. Yogesh M. Kalke', 7387389482, 'yogesh.kalke@dyptc.edu.in', 'Ph.d. (Mechanical Engg.- Pursuing) M. E.(Heat Power Engg.) B.E. (Mechanical Engg.)', '2019-12-31', '0000-00-00'),
(39, 25, 'Mr. Ganesh G.Patil', 8796940279, 'ganesh.patil@dyptc.edu.in', 'Ph.d. (Mechanical Engg.- Pursuing) M. Tech.(Mechanical Engg.) B.E. (Mechanical Engg.)', '2019-12-31', '0000-00-00'),
(40, 25, 'Mr. R D Pharande', 9860438299, 'rahul.pharande@dyptc.edu.in', 'Ph.d. (Mechanical Engg.- Pursuing) M.E. (Automobile Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(41, 25, 'Mr. G.H.Kawade', 9764166064, 'ganesh.kawade@dyptc.edu.in', 'Ph.d. (Mechanical Engg.- Pursuing) M.E. (Automobile Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(42, 25, 'Mr. Bhushan N.Ghodake', 9890720257, 'bhushan.ghodake@dyptc.edu.in', 'M.E. (Mech-Design Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(43, 25, 'Mr. Gaurav Gaurkhede', 7709655988, 'gaurav.gaurkhede@dyptc.edu.in', 'M.E. (Mechanical Engg.) B.E. (Automobile Engg.)', '2019-12-31', '0000-00-00'),
(44, 25, 'Mr. Swapnil S. Bari', 9860512528, 'swapnil.bari@dyptc.edu.in', 'M.E. (Mechanical Engg.) B.E. (Mechanical Engg.)', '2019-12-31', '0000-00-00'),
(45, 25, 'Mr. Sharad K. Chikane', 8888018847, 'sharad.chikane@dyptc.edu.in', 'M.E. (Mech-Design Engg.) B.E. (Mechanical Engg.)', '2019-12-31', '0000-00-00'),
(46, 24, 'Prof. Vinod B Bharat', 9923001999, 'vinod.bharat@dyptc.edu.in', 'Machine Learning,Deep Learning,Artificial Inteligence', '2019-12-31', '0000-00-00'),
(47, 24, 'Prof. Laxmikant S Malphedwar', 9028698796, 'laxmikant.malphedwar@dyptc.edu.in', 'Machine Learning,Big Data & Data Science', '2019-12-31', '0000-00-00'),
(48, 24, 'Prof. Nilesh N Wani', 9764994653, 'nilesh.wani@dyptc.edu.in', 'Machine Learning,Deep Learning,Artificial Inteligence', '2019-12-31', '0000-00-00'),
(49, 24, 'Prof. Yuvaraj N N', 7058481958, 'yuvaraj.n@dyptc.edu.in', 'Image Processing,Artificial Inteligence,IOT', '2019-12-31', '0000-00-00'),
(50, 24, 'Prof. Prachi Salve', 8007228787, 'prachi.salve@dyptc.edu.in', 'Networking,Machine Learning,Artificial Inteligence', '2019-12-31', '0000-00-00'),
(51, 24, 'Prof. Vishal Walunj', 8793404140, 'vishal.walunj@dyptc.edu.in', 'Networking,IOT', '2019-12-31', '0000-00-00'),
(52, 24, 'Prof. Madhavi Patil', 9561553734, 'madhavi.patil@dyptc.edu.in', 'Data Mining,Networking,Cloud Computing', '2019-12-31', '0000-00-00'),
(53, 24, 'Prof. Aparna Patil', 9503338807, 'aparna.patil@dyptc.edu.in', 'Cloud Computing & Security', '2019-12-31', '0000-00-00'),
(54, 24, 'Prof. Amolkumar Jadhav', 8208920924, 'amolkumar.jadhav@dyptc.edu.in', 'Data Mining & Clustering', '2019-12-31', '0000-00-00'),
(55, 53, 'Mr.Surendra Suryavanshi', 9730179493, 'Surendra.suryavanshi@dyptc.edu.in', 'M.Sc.Chemistry', '2019-12-31', '0000-00-00'),
(56, 53, 'Mr.Navnath Gaikwad', 9975474346, 'navanth.gaikwad@dyptc.edu.in', 'M.Sc.Physics,B.Ed.', '2019-12-31', '0000-00-00'),
(57, 53, 'Ms.Rohini Ghule', 9657409143, 'rohini.ghule@dyptc.edu.in', 'M.Sc. Mathematics', '2019-12-31', '0000-00-00'),
(58, 53, 'Ms.Reshma Bangar', 9309542235, 'reshma.bangar@dyptc.edu.in', 'M.Sc. Mathematics', '2019-12-31', '0000-00-00'),
(59, 53, 'Ms.Chede Nirmala', 8830488239, 'nirmala.chede@dyptc.edu.in', 'M.Sc. Mathematics,B.Ed.', '2019-12-31', '0000-00-00'),
(60, 53, 'Ms.Shital Gaikwad', 9075323388, 'shital.gaikwad@dyptc.edu.in', 'M.Sc. Mathematics', '2019-12-31', '0000-00-00'),
(61, 18, 'Dr. A. D. Chimbalkar', 9689793383, 'ashishchimbalkar@gmail.com', 'Pharmacology', '2019-12-31', '0000-00-00'),
(62, 18, 'ms. Vaishanvi Bind', 9168177789, 'vaishnavibind@gmail.com', 'Phamaceutics', '2019-12-31', '0000-00-00'),
(63, 18, 'Mrs. Reshma Jondhale', 9503587543, 'res.jondhale@gmail.com', 'Pharmaceutics', '2019-12-31', '0000-00-00'),
(64, 18, 'Sulbha Karpude', 8329631671, 'skarpude53@gmail.com', 'B.Pharm', '2019-12-31', '0000-00-00'),
(65, 17, 'Dr. Uma Jadhao', 9422756052, 'uma.jadhav@dyptc.edu.in', 'B. Arch/M. Arch Ph.D', '2019-12-31', '0000-00-00'),
(66, 17, 'Ar. Shyam Kolhatkar', 9822409787, 'slkolhatkar@gmail.com', 'G.D.Arch', '2019-12-31', '0000-00-00'),
(67, 18, 'Dr. Ravindra Deshmukh', 9119195041, 'ravindra.deshmukh@dyptc.edu.in', 'M. Arch(Design),PhD', '2019-12-31', '0000-00-00'),
(68, 17, 'Dr. Ravindra Deshmukh', 9119195041, 'ravindra.deshmukh@dyptc.edu.in', 'M. Arch(Design),PhD', '2019-12-31', '0000-00-00'),
(69, 17, 'Ar. Swaminath Swamy', 9623092309, 'swaminath.swamy@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(70, 17, 'Ar. Swati Agashe', 9284363352, 'swati.agashe@dyptc.edu.in', 'M. Arch(Urbun Economic Development),(Energy Audit of buildings UK License)', '2019-12-31', '0000-00-00'),
(71, 17, 'Ar. Omkar Samudra', 9765691310, 'omkar.samudra@dyptc.edu.in', 'M. Arch(Urban Design)', '2019-12-31', '0000-00-00'),
(72, 17, 'Ar. Seemantini Nakil', 9558863787, 'seemantini.nakil@dyptc.edu.in', 'M. Arch(Urban Design)', '2019-12-31', '0000-00-00'),
(73, 17, 'Ar. Dhanashri Mirajkar', 8308542035, 'dhanashri.mirajkar@dyptc.edu.in', 'M. Arch(General M. Arch)', '2019-12-31', '0000-00-00'),
(74, 17, 'Ar. Supriya Patil', 9881864455, 'supriya.patil@dyptc.edu.in', 'M. Arch', '2019-12-31', '0000-00-00'),
(75, 17, 'Ar. Anuradha Date', 7558361198, 'anuradha.date@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(76, 17, 'Ar. Ketki Lomte', 9503804983, 'ketki.lomte@dyptc.edu.in', 'M. Arch(Enviroment)', '2019-12-31', '0000-00-00'),
(77, 17, 'Ar. Durgesh Kulkarni', 7768825711, 'durgesh.kulkarni@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(78, 17, 'Ar. Milind Deshmukh', 9890647560, 'milind.deshmukh@dyptc.edu.in', 'M. Arch(Enviromental Building Sceince)', '2019-12-31', '0000-00-00'),
(79, 17, 'Ar. Babasaheb Mhaske', 8888220481, 'babasaheb.mhaske@dyptc.edu.in', 'M. Arch(Product Design)', '2019-12-31', '0000-00-00'),
(80, 17, 'Ar. Sneha Ghate', 9623951854, 'sneha.ghate@dyptc.edu.in', 'M. Arch(Enviromental)', '2019-12-31', '0000-00-00'),
(81, 17, 'Ar. Anagha Pathak', 9370882540, 'anagha.pathak@dyptc.edu.in', 'M. Arch(Project Management)', '2019-12-31', '0000-00-00'),
(82, 17, 'Ar. Aakash Shah', 9374123218, 'aakash.shah@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(83, 17, 'Ar. Akif Pathan', 8149491780, 'akif.pathan@dyptc.edu.in', 'M. Arch(Project Management)', '2019-12-31', '0000-00-00'),
(84, 17, 'Ar. Poonam Solanki', 9374015484, 'poonam.solanki@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(85, 17, 'Ar. Neha Saggam', 8805468989, 'neha.saggam@dyptc.edu.in', 'M. Arch[Environmental Architecture]', '2019-12-31', '0000-00-00'),
(86, 17, 'Ar. Sayli Pawar', 9923104447, 'sayli.pawar@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(87, 17, 'Ar. Sanjukta Das', 9987334038, 'sanjukt.das@dyptc.edu.in', 'M. Arch(Urban Cinservation)', '2019-12-31', '0000-00-00'),
(88, 17, 'Ar. Ranjeet Nerlekar', 9403626394, 'ranjeet.nerlekar@dyptc.edu.in', 'M. Arch(Urban Design)', '2019-12-31', '0000-00-00'),
(89, 17, 'Ar. Ambalika Ekka', 8763548349, 'ambalika.ekka@dyptc.edu.in', 'M. Arch(Sustainable)', '2019-12-31', '0000-00-00'),
(90, 17, 'Ar. Amruta Dabhade', 9552812797, 'amruta.dabhade@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00'),
(91, 17, 'Ar. Ketki Lavate', 9579196323, 'ketki.lavate@dyptc.edu.in', 'M. Arch', '2019-12-31', '0000-00-00'),
(92, 17, 'Ar. Anuja Padole', 7841012867, 'anuja.padole@dyptc.edu.in', 'B.E.Civil', '2019-12-31', '0000-00-00'),
(93, 17, 'Ar. Mishal Shah', 8017270602, 'mishal.shah@dyptc.edu.in', 'B. Arch', '2019-12-31', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `page_section_id` int(11) NOT NULL COMMENT 'differentiate page sections',
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `dsecription` text,
  `vew_detail_link` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `page_section_id`, `image`, `title`, `dsecription`, `vew_detail_link`, `date_created`, `date_modified`) VALUES
(1, 1, '35de03cd34bec3de8ce13b707d5e8344.png', 'Recognised by UGC', NULL, 'asxas', '2019-12-16', '0000-00-00'),
(2, 1, '5d26d10c81723de7aee27ae3a980a8f4.png', 'Student Friendly Campus', '<p>Easy Bank Loans, Hostel Accommodation, In House Cafe&#39;s, Sports Centre</p>\r\n', 'test', '2019-12-16', '0000-00-00'),
(3, 1, 'ef687f235c6dde7cf557d2405e3618e7.png', 'Teaching Methodology', 'Industry relevant student learning material including e-books, videos, online lectures, and research papers.', 'test', '2019-12-16', '0000-00-00'),
(4, 1, 'be6619b85035863c80c572ee33e4dd80.png', 'Professional Linkages', 'Associated with top corporates including Indigo Airlines, American Express, Cipla, Emerson, Bajaj, Jet Airways, Vodafone, IBM, Genpact, Taj, Oberoi’s, Marriott, Radisson, Hyatt, Leela, ITC.', 'test', '2019-12-16', '0000-00-00'),
(5, 1, '73a4b870199276e73bfbf63c233288ac.png', 'Examination Protocol', 'All the information regarding the examination process for the programs at DYPU.', 'test', '2019-12-16', '0000-00-00'),
(6, 1, 'c08aeace4deab06cbd010c5401c0d15f.png', 'ASK US', 'Constant Academic Support from University Counsellors, Contact us via Calls, Emails, Facebook, etc.', 'test', '2019-12-16', '0000-00-00'),
(7, 2, '19530956c584a8e34d8a4b0fb250557d.png', 'Engineering', '', 'test', '2019-12-16', '0000-00-00'),
(8, 2, 'c034bcfd007b4f50b214313990d06bff.png', 'Business Administration (MBA)', '', 'test', '2019-12-16', '0000-00-00'),
(9, 2, '0a3d8ad8354cac5ecedb40ac9025b06e.png', 'Pharmacy', '', 'test', '2019-12-16', '0000-00-00'),
(10, 2, 'b149d62f0d9261c52c9330a24e1908b3.png', 'Architecture', '', 'test', '2019-12-16', '0000-00-00'),
(11, 2, '7d8d9c3e6c0ded760f39a7897ee7d42e.png', 'Hospitality & Tourism Studies', '', 'test', '2019-12-16', '0000-00-00'),
(12, 3, '0fecbde57612b4515f9746c637123ab0.png', 'Amit Soni', '<h4>MBA</h4>\r\n            <p>It was a wonderful experience for me being with the D Y Patil Pune for 3 years.\r\nI got a good experience in both the academics and sports.</p>\r\n', '', '2019-12-20', '0000-00-00'),
(13, 3, 'be595155197bd61a001f459b466952e3.png', 'Abhishek Pandey', '<h4>Engineer</h4>\r\n            <p>It has been a home away from home at D Y Patil Pune ,Our safety and security is the prime priority of the administration, and they have always been very supportive in all the matters & concern.</p>\r\n', '', '2019-12-20', '0000-00-00'),
(14, 3, '8d5d9ddb5f252061978d0481a5f3fcc1.png', 'Mitali Patil', '<h4>Architecture</h4>\r\n            <p>I would like to thanks D Y Patil Pune for shaping my career and giving wings to \r\n     my dreams.</p>\r\n', '', '2019-12-20', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `list_of_values`
--

CREATE TABLE `list_of_values` (
  `id` int(11) NOT NULL,
  `values` varchar(255) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_of_values`
--

INSERT INTO `list_of_values` (`id`, `values`, `date_created`) VALUES
(1, 'DYPU unique', '2019-12-16'),
(2, 'Programs Offered', '2019-12-16'),
(3, 'Testimonials', '2019-12-16'),
(4, 'About University', '2019-12-17'),
(5, 'University Founder', '2019-12-17'),
(6, 'President', '2019-12-17'),
(7, 'Managing Trustee', '2019-12-17'),
(8, 'Vice Chancellor.', '2019-12-17'),
(9, 'Vision Mision', '2019-12-17'),
(10, 'Core Values', '2019-12-17'),
(11, 'Ethics', '2019-12-17'),
(12, 'Ranking', '2019-12-17'),
(13, 'Advisory Board', '2019-12-17'),
(14, 'Administration', '2019-12-17'),
(15, 'University Regulations', '2019-12-17'),
(16, 'civil infrastructural engineering', '2019-12-19'),
(17, 'School of Architecture', '2019-12-17'),
(18, 'School of Pharmacy', '2019-12-17'),
(19, 'School of Business Management', '2019-12-17'),
(20, 'School of Hospitality & Tourism Studies', '2019-12-17'),
(21, 'electrical electronics engineering', '2019-12-19'),
(22, 'mechanical engineering', '2019-12-19'),
(23, 'information technology engineering', '2019-12-19'),
(24, 'computer information', '2019-12-19'),
(25, 'automobile engineering', '2019-12-19'),
(26, 'Directorate Of Research', '2019-12-20'),
(27, 'Plagiarism Policy', '2019-12-20'),
(28, 'Research Policy', '2019-12-20'),
(29, 'Research Committee', '2019-12-20'),
(30, 'library', '2019-12-23'),
(31, 'transportation', '2019-12-23'),
(32, 'student_residences', '2019-12-23'),
(33, 'cafeteria', '2019-12-23'),
(34, 'banking', '2019-12-23'),
(35, 'gymnasium', '2019-12-23'),
(36, 'healthcare', '2019-12-23'),
(37, 'sports', '2019-12-23'),
(38, 'About Sports', '2019-12-24'),
(39, 'Facilities', '2019-12-24'),
(40, 'Events', '2019-12-24'),
(41, 'Placement About', '2019-12-24'),
(42, 'Recruiters', '2019-12-24'),
(43, 'home page', '2019-12-27'),
(44, 'committees', '2019-12-27'),
(45, 'admission process', '2019-12-27'),
(46, 'video', '2019-12-27'),
(47, 'Register', '2019-12-30'),
(48, 'controller of examination', '2019-12-30'),
(49, 'finance and accounts officer', '2019-12-30'),
(50, 'faculties', '2019-12-30'),
(51, 'Electronics & Communication Engineering', '2019-12-30'),
(52, 'Civil Engineering Department', '2019-12-30'),
(53, 'First Year Engneering', '2019-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `menu_link` varchar(40) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `location` varchar(40) NOT NULL,
  `position` int(11) NOT NULL,
  `status` enum('active','deactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `placements_and_internships`
--

CREATE TABLE `placements_and_internships` (
  `id` int(11) NOT NULL,
  `page_section` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `placements_and_internships`
--

INSERT INTO `placements_and_internships` (`id`, `page_section`, `page_id`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(1, 1, 41, '', '<p>Campus placements are beneficial for the students in more than one way. The students get exposed to the corporate environment at the right time and learn how to prepare themselves for the competition. D Y Patil University has a robust placement cell that works throughout the year to get renowned brands on campus for recruitment. The University provides placement opportunities for all the undergraduate/postgraduate programs that it offers.</p>\r\n\r\n<p>The main objective of the Placement Cell is to bridge the gap between stringent competition in the industry and pool of talent available in the university. With adequate training and support, students enhance their skills and understand how to showcase their abilities in the best possible way. Not only the Placement Cell offers training programs to students to develop various significant skills but also facilitates the process of recruitment for all the eligible students.</p>\r\n\r\n<p>With the same aim to provide world-class training to students, the University operates its Training &amp; Placement (T&amp;P) Cell which liaisons between industry and the institute. Established with an aim to develop a strong framework for a rewarding and professional career, the T&amp;P Cell conducts pre-placement training sessions to equip students with relevant skills in their respective fields. Let us discuss why Placement Cell is important to stand out in a crowd of aspiring students.</p>\r\n\r\n<p><strong>Soft Skills Training:</strong></p>\r\n\r\n<p>Considering the importance of aptitude skills and soft skills in any organization, there is a huge demand for professionals who are industry ready. One of the major responsibilities of any Placement Cell is to organize sessions to train students on analytical, logical, verbal and critical reasoning, problem-solving and time-saving techniques along with English verbal and language adaptability.</p>\r\n\r\n<p><strong>Personality Development:</strong></p>\r\n\r\n<p>Personality development is one of the most crucial aspects of training sessions, the University Placement Cell organizes a series of tests and workshops to help students discover their potential and analyze their shortcomings. It trains students to deal with challenging situations at work and motivates them by conducting various workshops on how to increase their self-esteem.</p>\r\n\r\n<p><strong>Group Discussions:</strong></p>\r\n\r\n<p>With group discussions becoming an integral part of the hiring process, the Placement Cell conducts multiple rounds of mock group discussions to build confidence, communication skills, teamwork and enhance the technical knowledge of the subject along with current affairs.</p>\r\n\r\n<p><strong>Personal Interview:</strong></p>\r\n\r\n<p>Your final selection depends on the personal interview round, hence, the Placement Cell helps with the preparation, increasing your chances of getting hired. The Placement Cell instructs and coaches students on required interview skills which are related to dress code, confidence, creativity, ability to react and respond, and handle stressful situations.</p>\r\n\r\n<p><strong>Resume and Cover Letter Writing:</strong></p>\r\n\r\n<p>Your resume and cover letter can help you create the first best impression on a potential employer. To ensure your resume does not get added to the rejected pile, you should create an attention-grabbing resume as well an effective LinkedIn profile. The Placement Cell trains you in creating a well-organized resume which will highlight your qualifications, education, employment history, certifications, technical skills and any additional skills that match with the job profile.</p>\r\n\r\n<p>The University has an all-inclusive placement program which commences in the first semester and engages students with pre-placement training to make them industry-ready for the corporate world. With its exclusive in-house Soft Skills training program taught by experts, the Placement Cell helps students develop capabilities and competencies enabling the students to stand out in a crowd of career builders.</p>\r\n\r\n<p>With expanding the role of business globally, D Y Patil University, Pune,is the place which comes to the minds of the recruiters once the placement season starts. As far as DYPU Institutions are considered, we focus not only on the career-oriented growth but offers versatility. There has been a consistent growth in our placements record. Today, graduating does not mean getting a degree certificate alone but equipping one with in-depth knowledge of the subject. It has also become mandatory that the student instils both technical and soft skills within him in order to reach heights. SRM helps the students to achieve this by conducting various workshops, guest lectures and seminars and also by letting them participate in various other extra-curricular activities. We are passionate towards grooming the students not only as the efficient professionals but also as the responsible citizens. At D Y Patil University,Pune, it is our incessant endeavour to make all our students industry-ready.</p>\r\n\r\n<p><em>Google, Microsoft, Amazon, Siemens, Hyundai Motors, ITC,Fuji Xerox (Japan), Honda, SAP Labs, Michelin, CISCO, Shell India, Marriott Hotels, Indigo Airlines and Vestas to name a few of our recruiters. We take pride in our students that they have also received international offers</em></p>\r\n', '', '2019-12-24', '2019-12-24'),
(2, 2, 41, 'Advantages of DYPU:', '<p>D Y Patil University, Pune, attracts over 450+ recruiters from various domains of companies providing a wide range of opportunities through:</p>\r\n\r\n<ul>\r\n	<li>Super Dream and Dream Companies</li>\r\n	<li>Top Product companies</li>\r\n	<li>Market leaders in IT Services Companies</li>\r\n	<li>Leading Core Engineering Companies</li>\r\n	<li>Specialized Training programs</li>\r\n	<li>Internships with top corporates</li>\r\n</ul>\r\n\r\n<p>DYPU stands apart and unique from its counterparts in various aspects by holding:</p>\r\n\r\n<ul>\r\n	<li>World Class Infrastructure</li>\r\n	<li>Wide Range of Courses</li>\r\n	<li>Interactive Learning</li>\r\n	<li>Global Linkages</li>\r\n	<li>Students Diversity</li>\r\n</ul>\r\n', 'b0ebb0110cc339aed61811d60fb15a29.jpg', '2019-12-24', '2019-12-24'),
(4, 0, 42, '', '', '0cd6c9c0d8e74b71b055f3d645d75837.png', '2019-12-24', '0000-00-00'),
(5, 0, 42, '', '', '7df816e40b9ba685c307381750e50d3f.png', '2019-12-24', '0000-00-00'),
(6, 0, 42, '', '', '9d4013634adbaa933063022b1e465c9e.png', '2019-12-24', '0000-00-00'),
(7, 0, 42, '', '', 'fa39efc705c274df890ab7209fb30cbc.png', '2019-12-24', '0000-00-00'),
(8, 0, 42, '', '', 'b5b9737af5f7bf025da56607da9d666e.png', '2019-12-24', '0000-00-00'),
(9, 0, 42, '', '', 'b5b65afbb631161d222d2b5f4cbdb6b6.png', '2019-12-24', '0000-00-00'),
(10, 0, 42, '', '', 'd07c16f4363de4d702bd129cd4949529.png', '2019-12-24', '0000-00-00'),
(11, 0, 42, '', '', '734379485448b519705c234403744e99.png', '2019-12-24', '0000-00-00'),
(12, 0, 42, '', '', '4f2306d2de391ec6b7cb50a9c4bb4bb4.png', '2019-12-24', '0000-00-00'),
(13, 0, 42, '', '', 'e1cd52821902dc6a74aabf973549ffc2.png', '2019-12-24', '0000-00-00'),
(14, 0, 42, '', '', 'd55e9bef02f7359a97e71f0add011c98.png', '2019-12-24', '0000-00-00'),
(15, 0, 42, '', '', '126324bc1485345bd25becba8afa196e.png', '2019-12-24', '0000-00-00'),
(16, 0, 42, '', '', 'facd307d3238610832f3f26eddd25144.png', '2019-12-24', '0000-00-00'),
(17, 0, 42, '', '', '46ad9d2a9c1c25c06470e6d2b6f55f3c.png', '2019-12-24', '0000-00-00'),
(18, 0, 42, '', '', '00b91bfce1e57687199ab1c37396bd30.png', '2019-12-24', '0000-00-00'),
(19, 0, 42, '', '', '314a9778e9b20303565584868bbd8252.png', '2019-12-24', '0000-00-00'),
(20, 0, 42, '', '', '6b4c6928779d97d29df18356c26701c3.png', '2019-12-24', '0000-00-00'),
(21, 0, 42, '', '', '42f142d690917d18a02112bf9776adc3.png', '2019-12-24', '0000-00-00'),
(22, 0, 42, '', '', '9a15c26163005f021238bd36cc8f9072.png', '2019-12-24', '0000-00-00'),
(23, 0, 42, '', '', '02dcf135065c2a45869b7d6024d6b053.png', '2019-12-24', '0000-00-00'),
(24, 0, 42, '', '', '520ccaeda6f9209c5c53eb8e9c6468c2.png', '2019-12-24', '0000-00-00'),
(25, 0, 42, '', '', 'd8c134ffde98a9da034e2d527f70f205.png', '2019-12-24', '0000-00-00'),
(26, 0, 42, '', '', 'f9498731f4809a1470158539805a17f9.png', '2019-12-24', '0000-00-00'),
(27, 0, 42, '', '', '24751997bb7db22c232432f5a4805f8e.png', '2019-12-24', '0000-00-00'),
(28, 0, 42, '', '', '75cd2e6a90269ba9287dbc4b745a3c12.png', '2019-12-24', '0000-00-00'),
(29, 0, 42, '', '', 'c05c813feaa9f378c19e3aafe0e47e98.png', '2019-12-24', '0000-00-00'),
(30, 0, 42, '', '', '3f4ee6735f7354793965721c7be928bc.png', '2019-12-24', '0000-00-00'),
(31, 0, 42, '', '', '2ca99d14e8a215ab06866f94b464b8db.png', '2019-12-24', '0000-00-00'),
(32, 0, 42, '', '', '1981601b2643e525bf39a44036e8919d.png', '2019-12-24', '0000-00-00'),
(33, 0, 42, '', '', '87d74a13787d52df81b4219126a52a60.png', '2019-12-24', '0000-00-00'),
(34, 0, 42, '', '', 'b109609dc76b3f930fed4d18e807598a.png', '2019-12-24', '0000-00-00'),
(35, 0, 42, '', '', '4624b34fb46bed0a729b806df3fc1c53.png', '2019-12-24', '0000-00-00'),
(36, 0, 42, '', '', 'fc276f2c70ac0083e314c41770802a9e.png', '2019-12-24', '0000-00-00'),
(37, 0, 42, '', '', 'f0b9c6fddc63638e6431d7f565301587.png', '2019-12-24', '0000-00-00'),
(38, 0, 42, '', '', '193ada4cf267936b2b8e1ef0e74d6fe1.png', '2019-12-24', '0000-00-00'),
(39, 0, 42, '', '', '4e4a2ab57a7862ff23302adee4187650.png', '2019-12-24', '0000-00-00'),
(40, 0, 42, '', '', '76c41f35855f8ceb71acff0a446a2117.png', '2019-12-24', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `research`
--

CREATE TABLE `research` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_section` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research`
--

INSERT INTO `research` (`id`, `page_id`, `page_section`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(1, 26, 0, 'DIRECTORATE OF RESEARCH', '<p>D Y Patil University, Pune, have taken various research initiatives over the years to mobilize the available knowledge resources for research and also, to come up strongly with innovative solutions.</p>\r\n						<p>All the research centres of DYPU,Pune, function in a cooperative manner to identify and initiate applied as well as cross-disciplinary research projects, create and combine patentable Intellectual Property (IP) components, design and develop prototypes & proof of concepts, manage and market products & solutions (through know-how transferred incubated companies), win and work to deliver funded research projects.</p>', '1e197cad21c90f515db6214c606f0d53.jpg', '2019-12-20', '0000-00-00'),
(2, 26, 0, '', '<h4>Our mission:</h4>\r\n							<ul>\r\n							<li>Provide excellent research culture and infrastructure</li>\r\n							<li>Serve as a platform for strong interdisciplinary collaborations and knowledge 	\r\n							sharing</li>\r\n							<li>Publish research findings in high quality journals of international repute</li>\r\n							<li>Create scientifically profound human resources for academic & industrial research</li>\r\n							<li>Promote industrial collaborations involving active and mutually beneficial R & D projects</li>\r\n							</ul>', '', '2019-12-20', '0000-00-00'),
(6, 28, 0, 'RESEARCH POLICY', '<p>D.Y. Patil University , Pune embodies a comprehensive and conducive environment for innovations and research.  The university seeks to create and share knowledge through quality research and contribute significantly in the Indian Education System. The university promotes strengthening of research infrastructure by exploring novel paradigms to address various challenges of the society as well as the service sector. The university inculcates the environment of research amongst the students as well. At the undergraduate level, the courses have a compulsory subject on research and university has made it mandatory for the students to undertake research on a topic of their interest in order to complete their degree. The university strives to follow the following objectives of research: </p>\r\n						<ul>\r\n							<li>To facilitate research amongst students as well as staff </li>\r\n							<li>To create awareness by organising workshops on research methodology and the	latest trends in research</li>\r\n							<li>To apply to different government as well as non-government funding agencies for \r\n							research.</li> \r\n							<li>To provide guidance for undertaking research </li>\r\n							<li>To foster academia- industry linkages and promote research in the domains \r\n							suggested by the industry</li>\r\n						</ul>', 'cf87c345402251421df008ae2f6de319.jpg', '2019-12-20', '0000-00-00'),
(7, 29, 0, 'RESEARCH COMMITTEE', '<p>This is an active committee that is focused towards undertaking & promoting research amongst students as well as faculties. The committee is chaired by the convener and comprises of senior faculties as members.</p>', '978267ac88b7cb33a2d63dddda51f08a.jpg', '2019-12-20', '0000-00-00'),
(8, 29, 0, 'The functions of Research Committee are :', '<ol>\r\n		<li>To formulate policy on all research related matters which impact on the strategic	objectives of the College</li>\r\n		<li>To consider and make recommendations on matters of policy relating to research	including matters referred to it by Council and other College committees. </li>\r\n		<li>To review and oversee the implementation of College’s policy on research ethics. </li>\r\n		<li>To monitor the relevance and efficacy of established policy – addressing	shortcomings and anomalies so as to facilitate high quality research activities	within the College structures. </li>\r\n		<li>To oversee quality assurance and improvement measures in respect of research \r\n		activity, including the efficacy of research quality measures.</li>\r\n\r\n		<li>To advise the Dean of Research in his role in reporting internally and externally on 	research and related matters. </li>\r\n\r\n		<li>To review Annual Reports relating to research matters and to make \r\n		recommendations to Council. </li>\r\n\r\n		<li>To approve procedures for allocating research funds and monitor their implementation. </li>\r\n		<li>To establish Advisory Committees and Working Groups as required to develop and oversee policy in respect of research matters.</li>\r\n	</ol>', '', '2019-12-20', '0000-00-00'),
(9, 29, 0, 'Authority:', '<ol>\r\n		<li>The Committee shall operate under delegated authority from the University	Council. </li>\r\n\r\n		<li>Through the Committee membership, the Committee shall act as a channel of	communication between the Council, the research community of College and the \r\n			research administration, and shall report to Council with its considered recommendations pertaining to its remit as appropriate.</li> \r\n\r\n	<li>The Committee may investigate any matter falling within its terms of reference,	calling on whatever resources and information it considers necessary to so do. </li>\r\n\r\n	<li>The Committee is authorised to seek any information it requires from any	employee of College to enable it to discharge its responsibilities and shall have	made available to it on a timely basis all information requested from any employee in a clear and well organised manner.</li>\r\n	</ol>', '', '2019-12-20', '0000-00-00'),
(10, 29, 0, 'Performance Evaluation:', '<p>The Research Committee shall, at least once a year, review its own performance and its terms of reference and shall report its conclusions and recommend any changes it considers necessary to the University Council.</p>', '', '2019-12-20', '0000-00-00'),
(12, 27, 1, 'Preamble:', '<p>Ethics and honesty are the two most important components of the academic activities be it teaching or research. Teaching and research is a noble profession based on extremely high moral values. There cannot be any room for claiming the credit for the work he/she has not undertaken. Many times it is observed that some of the “academicians” knowingly or unknowingly publish or present other’s work as their own. Such acts will affect healthy academic atmosphere in the institute which will also harm the reputation of the institute as well as the individual. It is therefore important for D Y Patil University Pune to have in place a policy on plagiarism to avoid such type of acts. </p>', '', '2019-12-23', '0000-00-00'),
(13, 27, 1, 'Definition of Plagiarism:', '<p>Plagiarism is defined as presenting another person’s work as one’s own work. Presentation includes copying or reproducing it without the acknowledgement of the source. Plagiarism involves copying of: phrases, clauses, sentences, paragraphs or longer extracts from published or unpublished work (including from the Internet) that exceeds the boundaries of the legitimate cooperation without acknowledgement of the source. </p>', '', '2019-12-23', '0000-00-00'),
(14, 27, 1, '', '', '0d6e3cedfc2fd0be2404836686459c5b.jpg', '2019-12-23', '2019-12-23'),
(15, 27, 2, 'Types of Plagiarism:', '<ol>\r\n<li>Secondary sources (Inaccurate citation) Secondary Source Plagiarism happens when a researcher uses a secondary source like a Meta study but only cites the primary sources contained within the secondary one. Secondary source plagiarism not only fails to attribute the work of the authors of the secondary sources but also provides a false sense of the amount of review that went into the research.</li> \r\n<li>Invalid sources (Misleading citation, Fabrication, Falsification) Invalid Source Attribution occurs when researchers reference either an incorrect or non-existent source. Though this may be the result of sloppy research rather than intent to deceive, it can also be an attempt to increase the list of references and hide inadequate research.</li>\r\n\r\n<li>Duplication (Self-plagiarism, Reuse) Duplication happens when a researcher reuses work from their own previous studies and papers without attribution. Candidate will be required to resubmit the work with proper citations. Regarding self-plagiarism or reuse, a certificate has to be issued by the supervisor specifying and attaching the articles that have been published by the student from the thesis work. Only these articles should be excluded from the check. No other article of student or supervisor should not be excluded from the check. If the published work is co-authored by the others, the researcher shall submit a consent letter from co-author(s) and publisher permitting him to use the work in his thesis.</li>\r\n<li>Paraphrasing (Plagiarism, Intellectual theft) Paraphrasing is taking another person’s writing and changing the words, making it appear that an idea or even a piece of research is original when, in truth, it came from an uncited outside source. Paraphrasing ranges from simple rephrasing to completely rewriting content while maintaining the original idea or concept.</li> \r\n<li>Repetitive research (Self-plagiarism, Reuse) Repetitive Research Plagiarism is the repeating of data or text from a similar study with a similar methodology in a new study without proper attribution. This often happens when studies on a related topic are repeated with similar result but the earlier research is not cited properly.</li> \r\n<li>Replication (Author Submission Violation) Replication is the submission of a paper to multiple publications, resulting in the same manuscript being published more than once. This can be an ethical infraction, particularly when a researcher claims that a paper is new when it has been published elsewhere.</li> \r\n<li>Misleading attribution(Inaccurate Authorship) Misleading Attribution is an inaccurate or insufficient list of authors who contributed to a manuscript. This happen when authors are denied credit for partial or significant contributions made to a study, or the opposite-when authors are cited in a paper although no contributions were made.</li>\r\n\r\n<li>Unethical collaboration (Inaccurate Authorship) Unethical Collaboration happens when people who are working together violate a code of conduct. Using written work, outcomes and ideas that are the result of collaboration, without citing the collaborative nature of the study and participants involved, is unethical. Using others’ work without proper attribution is plagiarism.</li> \r\n<li>Verbatim plagiarism (Copy-and-Paste. Intellectual Theft) Verbatim Plagiarism is the copying of another’s words and works without providing proper attribution, indentation or quotation marks. This can take two forms. First, plagiarists may cite the source they borrowed from, but no indicate that it is a direct quote. In the second, no attribution at all is provided, essentially claiming the words of someone else to be their own.</li>\r\n<li>Complete plagiarism (Intellectual Theft, Stealing) Complete plagiarism is an extreme scenario when a researcher takes a study, a manuscript or other work from another researcher and simply resubmits it under his/her own name </li>\r\n\r\n		</ol>', '', '2019-12-23', '0000-00-00'),
(16, 27, 2, 'How to detect Plagiarism:', '<p>It is the prime responsibility of an institute or individual to distinguish original content from plagiarized work. The detection of plagiarism is a judgment to be made by a person who understands the subject and who is also aware of the definition of plagiarism. Such person should also be aware of the tools available to detect the plagiarism. D Y Patil University Pune will use the best tools / software to detect plagiarism. It is at most important for an academic institute like D Y Patil University Pune to educate its student and teaching community about what constitutes plagiarism, how it is detected and of course the action that is going to follow if plagiarism is proved. </p>', '', '2019-12-23', '0000-00-00'),
(17, 27, 2, 'Compliance Statements:', '<p>All students (UG, PG, Doctoral and Post Doctoral) are required to submit a signed statement that they are aware of the plagiarism policy of the University and no part of their work, be it assignment, term paper, project report, thesis or dissertation etc. is not copied in any form and it is their own creation. </p>\r\n\r\n	<p><steong>Procedure for handling alleged Plagiarism</steong></p>\r\n	<p>Procedural Fairness: The University is committed to dealing with alleged plagiarism by any section of the University community in accordance with the principles of procedural fairness, including the right to: </p>\r\n\r\n	<p>(a) 	Be informed of the allegations against them in sufficient detail to enable them to understand the precise nature of the allegations and to properly consider and respond; </p>\r\n<p>(b) Have a reasonable period of time within which to respond to the allegations against them; </p>\r\n<p>(c) Have the matter resolved in a timely manner;</p> \r\n<p>(d) Impartiality in any investigation process; </p>\r\n<p>(e) Absence of bias in any decision-making.</p>\r\n\r\n<p><strong>Counselling:</strong> As the detection of plagiarism and steps to prevent it are important, equally important is to educate students and faculty members about the dangers of plagiarism. University needs to take steps to strengthen the moral of students so that they do not take support of the unfair-means. </p>\r\n\r\n<p><strong>Identification and Assessment of Alleged Plagiarism:</strong> <br>\r\nWhere an examiner detects or is made aware of alleged plagiarism by any person, the examiner must report the alleged plagiarism to the University. The University shall appoint a committee consisting of 4 experts. This committee will have Dean of the respective Institution/college/ school /department as Convener, one senior Professor from the concerned department (other than the guide), one professor from other department and Librarian as members. The committee shall check first for plagiarism; if it is, then whether it is negligent or dishonest type and what the degree of plagiarism is. After scrutiny, the committee will submit its recommendations to the office of the Controller of Examination. The committee of experts will use the best possible software provided by UGC or National Knowledge Commission for detecting the plagiarism. </p>\r\n\r\n<p><strong>Guidelines for action:</strong> This committee will submit its report and also its recommendation to the Controller of Examinations. In consultation with the Vice Chancellor of the University, the Controller of Examinations takes appropriate action based on the level of plagiarism and the recommendations of the committee. Depending on the severity of plagiarism the punishment could be: </p>\r\n<ol>\r\n<li>Fine or warning</li>\r\n<li>Rustication for limited period or permanent</li> \r\n<li>Withdrawal of degree The action taken on the act of plagiarism will be ratified in the meetings of the Academic Council and the Management Council.</li>	\r\n</ol>', '', '2019-12-23', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `school_department`
--

CREATE TABLE `school_department` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_department`
--

INSERT INTO `school_department` (`id`, `department_id`, `title`, `description`, `image`, `date_created`, `date_modified`) VALUES
(1, 17, 'About the Department', '<p>Architecture is one of the oldest and challenging profession mankind has known and it still continues to provide challenges to architects, to find solutions to the ever changing society and other concerns. Architecture is a profession for creative minds with scientific intellect, for people with passion to overcome the challenges.</p>\r\n\r\n<p>The School of Architecture works to make a better world through education. School of Architecture opens the door for passionate learners and provides platform for students to Explore, Experience and Execute. The aim of the department is  to mentor students to become professionals with open mind and sensitive approach towards the society and environment. Here we learn to tackle global challenges, work on a culture of serving and shaping the society through critical thinking, analytical approach and creative mind set; translating ideas into actions.</p>\r\n\r\n<p>The growth of students is nurtured by dedicated faculties, allowing the minds to grow, developing an unseen foundation system strong enough to support its potential for outward growth later in the professional field. The faculties are experienced in professional field, research – both fundamental & applied and committed to teaching. The faculties dedicatedly involve themselves with students learning processes based on thinking and exploratory means, to prepare them for the future global challenges.</p>\r\n\r\n<p>Academics:  The Department aims to provide for its students the finest Architectural, Design in and Planning education which promotes and expands their creative capacities and sensibilities and establishes the foundation for a creative professional life. The extensive curriculum is designed as per the regulations of the Council of Architecture and includes a blend of Traditional Architectural subjects as well as modern day architectural designs & subjects.</p>', '90459cd7887edf6406b0c213bce9b047.jpg', '2019-12-17', '0000-00-00'),
(2, 17, 'Faculty', '<p>The Department of Architecture incorporates some of the brightest architects of the country as its core faculty members. Through dedicated learning and research oriented approach, the faculties create a stimulating environment that channelizes the thinking ability and conceptual knowledge of the students. The Faculties regularly participate in International as well as National Conferences and also engage in presenting research papers in renowned journals.</p>', 'e059f5b24a107be0b6b061fb4963e169.jpg', '2019-12-17', '0000-00-00'),
(3, 17, 'Academics', '<p>Our Architectural and Interdisciplinary studies program allows you to tailor your own degree and are ideal for students with multi-disciplinary skills who enjoy making cross curricular connections. Architecture teaches student the skills to practice architecture and an understanding of how to use your skills imaginatively in different contexts. Architecture is taught by leading practitioners and academics, in studios and workshops designed for creative flexibility and idea generation. Alongside design teaching you will take core modules in technology, history and theory as well as professional studies. With the major demographic shift towards urban living in the world&rsquo;s population, planning for sustainable urban development is a significant global issue.</p>\r\n', 'b2b02236499c0f8f4861678293f6d7ea.jpg', '2019-12-19', '0000-00-00'),
(4, 17, 'Eligibility', '<p>Class XII pass with 50 % in PCM (45% for reserved category), overall 50% in class XII and NATA qualified.</p>', '', '2019-12-17', '0000-00-00'),
(5, 17, 'Syllabus', '<p>View full syllabus - Semester Details</p>\r\n', '', '2019-12-17', '0000-00-00'),
(6, 17, 'Documents Required', '<ul>\r\n	<li>Class Xth marksheet & Passing Certificate</li>\r\n	<li>Class XIIth marksheet & Passing Certificate</li>\r\n	<li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n	<li>Qualifying Exam Marksheet (JEE2/NATA)</li>\r\n	<li>Aadhar Card Copy</li>\r\n	<li>Pan Card Copy</li>\r\n	<li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-17', '0000-00-00'),
(7, 17, 'Duration of Course: 5 Years (10 semister)', '', '', '2019-12-17', '0000-00-00'),
(8, 17, 'Fees', '<p>The Tuition Fee for B. Architecture is INR 1, 50,000/- Per year.</p> \r\n<p>Enrollment Fees 1000/- once in 5 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-17', '0000-00-00'),
(9, 18, 'About the Department', '<p>DY Patil University School of Pharmacy looks forward to establish itself as center of quality education and industry oriented training in the field of Pharmaceutical Sciences. As Indian Pharmaceutical market is growing day by day and India has been proving to be world leader in various segments of Pharmaceutical Industry, the  DY Patil University School of Pharmacy look forward to establish ourselves as center for Industry- Academia Collaboration. Affiliated and approved by the Pharmacy College of India, the Department is focused on imparting extensive Pharmacy oriented academic instruction and training. The fully equipped, advanced laboratories are an added advantage. The Department is aims to achieve a 360 degree development of students.</p>', 'adddf60d5457f67ffc15f77676fbbae3.jpg', '2019-12-19', '0000-00-00'),
(10, 18, 'Faculty', '<p>The Department of Architecture incorporates some of the brightest architects of the country as its core faculty members. Through dedicated learning and research oriented approach, the faculties create a stimulating environment that channelizes the thinking ability and conceptual knowledge of the students. The Faculties regularly participate in International as well as National Conferences and also engage in presenting research papers in renowned journals.</p>', 'ce2f162da45cdb3b0c0878d750146869.jpg', '2019-12-19', '0000-00-00'),
(11, 18, 'Academics', '<p>The Department offers a comprehensive academic system that includes PCI approved Curriculum, latest practical sessions and a plethora of workshops, seminars, guest lectures, Conferences etc. Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department. </p>', 'b04281d67417bad301f9704b6d1d87dd.jpg', '2019-12-19', '0000-00-00'),
(12, 18, 'Eligibility', '<p>Class XII passed from a recognized board in Science Stream with PCB/PCM as minimum 50%+ MHCET.</p>', '', '2019-12-19', '0000-00-00'),
(13, 18, 'Syllabus', 'View full syllabus - Semester Details', 'SCHOOL-OF-PHARMACY.pdf', '2019-12-19', '0000-00-00'),
(14, 18, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (JEE2/NATA)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-19', '0000-00-00'),
(15, 18, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-19', '0000-00-00'),
(16, 18, 'Fees', '<p>The Tuition Fee for B. Pharmacy is INR 1,75,000/-Per year.</p>\r\n<p>Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-19', '0000-00-00'),
(17, 16, 'About the Department', '<p>Civil & Infrastructural Engineering is one of the broadest and oldest of the engineering disciplines, extending across many technical specialties. Civil Engineers plan, design, and supervise the construction of facilities essential to modern life like space satellites and launching facilities, offshore structures, bridges, buildings, tunnels, highways, transit systems, dams, airports, harbors, water supply system and wastewater treatment plants. A civil engineer is responsible for planning and designing a project, constructing the project to the required scale, and maintenance of the project. A civil engineer requires not only a high standard of engineering knowledge but also supervisory and administrative skills.</p>\r\n<p>The Department of Civil Engineering creates outstanding Civil Engineers.  Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department. </p>', '01c6a7ed4dd2b34c4b12cb212dd2b11c.jpg', '2019-12-19', '0000-00-00'),
(18, 16, 'Faculty', '<p>Department of Civil Engineering is known for its faculties. They have a strong base in Research, Academics as well as bring with them years of valuable industrial experience. They specialize in structural, geotechnical, environmental, water resources, transportation, surveying, and construction engineering, as well as management, remote sensing and GIS. The faculty members contribute to academic development by publishing books and presenting papers in international and national conferences. </p>', 'edb20aca31cef2b770d93a303fe31825.jpg', '2019-12-19', '0000-00-00'),
(19, 16, 'Academics', '<p>The four year undergraduate program of BE in Civil Engineering is designed with the aim of imparting the right mix of scholastic instruction and industry exposure to the students. The students are trained extensively in domains like Mathematics, Physics, Instrumentation, Design, Structure, Economics which are essential requirements for becoming a successful Civil Engineer.</p>', '618261d84075958e6c171bba858d234e.jpg', '2019-12-19', '0000-00-00'),
(20, 16, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>\r\n', '', '2019-12-19', '0000-00-00'),
(21, 16, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-19', '0000-00-00'),
(22, 16, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-19', '0000-00-00'),
(23, 16, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-19', '0000-00-00'),
(24, 16, 'Fees', '<p>The Annual Fee for BE in Civil Engineering is INR 1,35,000/- Per Year.</p>\r\n<p>Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-19', '0000-00-00'),
(25, 19, 'About the Department', '<p>By blending and experimenting with different methodologies, the School of Management grooms students to be not only managers but also responsible and responsive citizens and also become dynamic leaders of the corporate world. The corporate world demands students to have a holistic, systematic thinking and an attitude conducive for managing and leading a business.</p>\r\n\r\n<p>The various departmental activities for e.g. the numerous conferences, guest lectures, and visit to various national and international industrial visits which include National and International Conferences etc. help in meeting the various demands of the industry. These activities help the students to get a glimpse of the working of the industrial world and also bridge the gap between industry and institute. The Department provides 100% placement assistance to  students and has a network of numerous successful alumni working with renowned organizations across the world.</p>', 'eaee862610f03231270bf5fe78769b8e.jpg', '2019-12-26', '0000-00-00'),
(26, 19, 'Faculty', '<p>With its team of dedicated, experienced and learned faculty, the School of Management imparts quality par excellence education to the students. Faculties are driven towards Research and have presented their research papers in various conferences and paper presentations. They are also inclined towards authoring books /chapters/titles pertaining to the field of Management & Managerial Studies.</p>', 'f73e3b954000891123572eb83b1fdbd4.jpg', '2019-12-26', '0000-00-00'),
(27, 19, 'Academics', '<p>The Department follows a diverse curriculum and gives students a plethora of options to select their electives. The Curriculum is extensive and is reviewed twice a year by industry experts . The Department ensures that all emergent trends are incorporated in the syllabus. The Academic Instruction is not only confined to studies, the department focuses on transforming the personality of the students and making them ready for the industry.</p>', 'f307d9a737a5212e202a044e2572f32d.jpg', '2019-12-26', '0000-00-00'),
(28, 19, 'Eligibility', '<p>For Pursuing MBA, a candidate must have done Graduation in any discipline from a recognized and valid University with minimum of 50% marks + CAT/MHCET score.</p>', '', '2019-12-26', '0000-00-00'),
(29, 19, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(30, 19, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (JEE2/NATA)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(31, 19, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(32, 19, 'Fees', '<p>The Tuition Fee of MBA is INR 1,50,000/- Per year.</p>\r\n<p>Enrollment Fees 1000/- once in 2 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(33, 20, 'About the Department', '<p>The Department of Hospitality is reckoned across academic circles as one of the pioneer Hospitality Schools of India. It offers extensive courses in the field of Hospitality, Culinary and Tourism Studies.</p>\r\n<p>Hospitality industry is an ever growing industry. In the past decade, this industry has witnessed a growth of around 63% and has created numerous jobs across the globe. Trained & Skilled Hospitality Professionals- Chefs, Housekeepers, Bartenders, F& B Associates, Sales Executives, Front Office Executives- are always in demand. The Department carefully scrutinizes the trends of the industry and trains students in a manner that they are ready to take on the industry. The Department provides Campus Placement and is proud of its extensive global alumni network.</p>', '9182070496a347e0f8ec173245a33c4b.jpg', '2019-12-26', '0000-00-00'),
(34, 20, 'Faculty', '<p>The Department boasts of its faculties. It has on board the finest chefs, the most able Housekeepers and Bartenders. The faculties are dynamic as they have sound academic knowledge as well as industry experience</p>', '492e8351bf19f1acb79d16171c653154.jpg', '2019-12-26', '0000-00-00'),
(35, 20, 'Academics', '<p>The curriculum includes a perfect mix of theoretical instruction and practical sessions. It is reviewed twice a year by scholars as well as Hospitality professionals. Changes suggested by them are incorporated in the syllabus so that the students get to learn the best. Seminars, Workshops, Guest Lectures, Virtual Sessions also happen on a regular basis.</p>', '11a1b7a60e993b731c74521686c973d3.jpg', '2019-12-26', '0000-00-00'),
(36, 20, 'Eligibility', '<p>Class XII pass from any discipline with minimum 40% marks + NCHM/MHCET score</p>', '', '2019-12-26', '0000-00-00'),
(37, 20, 'Syllabus', 'View full syllabus - Semester Details', 'TOURISM-STUDIES.pdf', '2019-12-26', '0000-00-00'),
(38, 20, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (NCHM)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(39, 20, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(40, 20, 'Fees', '<p>The Tuition Fees for B.Sc. in Hospitality is INR 1,00,000 Per year.</p>\r\n<p>Enrollment Fees 1000/- once in 3 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(41, 22, 'About the Department', '<p>The Department of Mechanical Engineering trains students in applying engineering physics, engineering mathematics, and materials science principles to design, analyze, manufacture, and maintain mechanical systems. Mechanical Engineering is perhaps the most diverse and versatile of the engineering disciplines. In addition to physics and mathematics, it encompasses key elements of aerospace, electrical, civil, chemical and even materials science and bio-engineering. Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department.  Besides a solid math Science/Engineering education, the student can improve its skills tailoring the course to his requirements by choosing among a number of option modules in application fields such as  Embedded Systems, Networking, Communication, Control Systems, Microwave and Antenna, Basic Electronics and Signal & Image Processing.\r\n</p>\r\n\r\n<p>Mechanical Engineering touches virtually every aspect of modern life, from mobile phones and biomedical devices, to aircrafts and power plants. Not only engineering, mechanical engineers deal with economic issues, from the cost of a single component, to the economic impact of a manufacturing plant. Besides this, mechanical engineers can also be found in sales, engineering management, and corporate management. Versatility is another unique advantage in a world that is undergoing constant economic, political, industrial, and social change. Mechanical engineers are educated and positioned, not only to adapt, but to define and direct change.</p>', '7421460765dd8d1b4b00c0eb627901f2.jpg', '2019-12-26', '0000-00-00'),
(42, 22, 'Faculty', '<p>The Department of Mechanical Engineering has experienced faculties that are driven towards Research. They bring along  academic knowledge and industry exposure. Hence, the trio of Industry, Research and Academics forms the strength of this department. </p>', '9450b10d7e058c86dac89e2ba5fd90db.jpg', '2019-12-26', '0000-00-00'),
(43, 22, 'Academics', '<p>The goal of the Mechanical Engineering Curriculum is to create a flexible undergraduate educational experience in Design, Mathematics, Modeling, Computing, Management, Engineering Science etc. Principal study topics include Fluid Mechanics, Thermodynamics & Heat Transfer, Solid Mechanics, Materials Engineering, Manufacturing, Energy Systems, Dynamics & Control Computer Aided Design (CAD), Computer Integrated Manufacturing (CIM) and others.</p>', '1d32f4376a84aa2dafb2996bd4998755.jpg', '2019-12-26', '0000-00-00'),
(44, 22, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>', '', '2019-12-26', '0000-00-00'),
(45, 22, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(46, 22, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(47, 22, '<h4>Duration of Course: 4 Years (8 semister)</h4>', '', '', '2019-12-26', '0000-00-00'),
(48, 22, 'Fees', '<p>The Annual Fee for BE in Mechanical Engineering  is INR 1,35,000/- Per Year.</p>\r\n<p>Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(49, 21, 'About the Department', '<p>One of the most robust departments, EEE Department is always engrossed in innovation. The department is research centric and focuses on creating unconventional models. Electronics & Electrical Engineering is a branch of science which deals with the applications of electricity, electronics, and electromagnetism. It is a diverse field and has partnered with other fields to create numerous new avenues in the realm of Engineering. Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department.  Besides a solid math Science/Engineering education, the student can improve its skills tailoring the course to his requirements by choosing among a number of option modules in application fields such as IoT, Embedded Systems, Networking, Communication, Control Systems, Microwave and Antenna, Basic Electronics and Signal & Image Processing.</p>', '7282b1fafd835341b988559c4d67eb98.jpg', '2019-12-26', '0000-00-00'),
(50, 21, 'Faculty', '<p>The faculty in the department comprises of scholars, academicians and professionals. The dedicated staff members have sound knowledge in emerging areas like embedded systems, power electronics applications in power systems, expert systems, etc. The breadth and depth of the research interests of the academic staff ensures a high standard of lecture courses and provides excellent opportunities for challenging and stimulating projects.</p>', 'c2a65e5749b40e667c35c4597f3dbdd5.jpg', '2019-12-26', '0000-00-00'),
(51, 21, 'Academics', '<p>The four year undergraduate program of BE in Electronics & Electrical Engineering is designed with the aim of imparting the right mix of scholastic instruction and industry exposure to the students. The research division covers various aspects of electrical engineering. There is an active and growing area of research in the field of power electronics & drives, power apparatus and systems, whilst collaborating with each other and a variety of industrial partners. The research group focuses on power electronic devices and integrated circuits, and their uses in various applications. Other major research strands include FACTS and their integration in power systems, integrated design of electrical machines and drives and electromagnetic modeling.</p>', '4e017eab32a2e345e9aaf1ceb0a9ea3d.jpg', '2019-12-26', '0000-00-00'),
(52, 21, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>', '', '2019-12-26', '0000-00-00'),
(53, 21, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(54, 21, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(55, 21, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(56, 21, 'Fees', '<p>The Annual Fee for BE in Electrical & Electronics Engineering is INR 1,35,000/- Per Year</p>\r\n<p> Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(57, 23, 'About the Department', '<p>The Information Technology Department is driven towards providing quality and value-laden education for students, in the traditional and contemporary areas of Information Technology and develop students with good ethical practices, thus showcasing their all-round personality development. Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department.</p>\r\n<p>IT engineers apply their technical knowledge to solve a variety of technological challenges. They may also create new technologies, including the development of networking solutions and software programs. T engineers help to meet their employer\'s needs for computer hardware, software and networking tools. They work to develop, test, install, configure and troubleshoot computer hardware and software. </p>', '084834ce4b834875d1d17a7136dea5a4.jpg', '2019-12-26', '0000-00-00'),
(58, 23, 'Faculty', '<p>The vibrant faculty members of the department possess demonstrated expertise in many areas of information technology and flair for teaching different courses. The department consists of a medley of faculty members with industrial and academic experience. Faculty members are involved in carrying out collaborative research projects with leading industries and international universities.</p>', '0489a002e4be59c5b0f5c137608a1810.jpg', '2019-12-26', '0000-00-00'),
(59, 23, 'Academics', '<p>Research in the Information Technology department focuses on creating and evaluating innovative learning experiences, inspired by educational principles and technical progress. The key areas of research include semantic web, data mining, software reliability, wireless sensor networks, network security, big data analytics and cloud computing.  Students are motivated to pursue multi-disciplinary research projects on long term basis jointly with other department students.</p>', 'e4512127ee3bc7a725c4909ed600dd0e.jpg', '2019-12-26', '0000-00-00'),
(60, 23, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>', '', '2019-12-26', '0000-00-00'),
(61, 23, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(62, 23, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(63, 23, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(64, 23, 'Fees', '<p>The Annual Fee for BE in Information Technology  is INR 1,35,000/-. Per Year.</p>\r\n<p> Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(65, 24, 'About the Department', '<p>The Department of Computer Science Engineering never rests on its laurels. The department is focused on   advancing, evolving and enhancing Computer Science and Engineering fundamentals to build the intellectual capital of the society. The students have access to latest technologies and get hand on training on all the advancements that have come up in the recent years. The USP of the department is that it blends traditional learning with modern technology and offers to the students a wholesome platter of abound knowledge. Research Stimulating Environment, Advanced Teaching Methodology, Curriculum as per international standards, State of the Art Infrastructure, Advanced Labs ,International Internships- all add to the academic deliverables of the department.</p>', '33ac294be1b4b4a3eaf24320c57d247f.jpg', '2019-12-26', '0000-00-00'),
(66, 24, 'Faculty', '<p>The department has well qualified and experienced faculties from academic as well as industrial backgrounds. Apart from regular lectures, the department regularly engages professionals to interact with the students and take lectures for them.</p>', 'adcc1758962bebd4daa0bd0e98062666.jpg', '2019-12-26', '0000-00-00'),
(67, 24, 'Academics', '<p>The curriculum of BE in Computer Engineering is extensive and in syn with the latest developments in the field of Computers. It includes subjects like Artificial Intelligence, Data Analysis, Mobile Computing, Information Security, Machine Learning, Applied Mathematics etc. </p>', '558f432f4acf69a0d61835f7daa8d11a.jpg', '2019-12-26', '0000-00-00'),
(68, 24, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>', '', '2019-12-26', '0000-00-00'),
(69, 24, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(70, 24, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(71, 24, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(72, 24, 'Fees', '<p>The Annual Fee for BE in Computer Science is INR 1,35,000/-. Per Year.</p>\r\n<p>Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00'),
(73, 25, 'About the Department', '<p>The Department of Automobile Engineering incorporates traditional automobile system as well as the advancements in technology that have transformed the realm of Automobile industry. The Department of Automobile Engineering has got well equipped laboratories such as Engine Testing & Pollution Measurement Laboratory, Auto Scanning Laboratory, Automotive Components Laboratory, Fuels and Lubricants Laboratory In addition to this; basic laboratories such as Chassis Components, Engine Component and Vehicle Maintenance Laboratory are also there in the department. Other basic laboratories like – Strength of Materials, Fluid Mechanics and Machinery, Thermal Engineering and Metrology Lab have also been developed and is backed up by a modern workshop containing Carpentry, Smithy, Fitting, Welding, Machine Shop and Foundry Shop that cater to the needs of the students.</p>', '1f846d8b1a30e02e08a82ba92daffbea.jpg', '2019-12-26', '0000-00-00'),
(74, 25, 'Faculty', '<p>The department has a team of highly-qualified and well-experienced faculty and staff members of proven ability and administrative skills. They specialize in IC Engines, Alternative, Fuels, Automotive Technology, Automotive Design, Production, and Thermal & Electric Vehicles.</p>', '6dd9679082193a593b2018720887c29d.jpg', '2019-12-26', '0000-00-00'),
(75, 25, 'Academics', '<p>Through industry integrated workshops, seminars and conferences the students of automobile engineering are exposed to latest technology in designing the computer modeled and simulated components and development of new components/vehicles through relatively inexpensive process. They are encouraged to improve analytical and technical skills to gain employment in the areas such as Automotive control, Engine Design, FEA analysis, Chassis Design, CFD simulations and Vehicle Dynamics.</p>', '4199314379407069619a05af83adbb14.jpg', '2019-12-26', '0000-00-00'),
(76, 25, 'Eligibility', '<p>Class XII pass from a recognized board with minimum 50% from Science Stream (40% for reserved category)+ JEE score/MHCET.</p>', '', '2019-12-26', '0000-00-00'),
(77, 25, 'Syllabus', 'View full syllabus - Semester Details', '', '2019-12-26', '0000-00-00'),
(78, 25, 'Documents Required', '<ul>\r\n  <li>Class Xth marksheet & Passing Certificate</li>\r\n  <li>Class XIIth marksheet & Passing Certificate</li>\r\n  <li>Leaving Certificate/Transfer Certificate/Migration Certificate</li>\r\n  <li>Qualifying Exam Marksheet (AIEEE/MHCET,etc)</li>\r\n  <li>Aadhar Card Copy</li>\r\n  <li>Pan Card Copy</li>\r\n  <li>08 Passport Photographs</li>\r\n</ul>', '', '2019-12-26', '0000-00-00'),
(79, 25, 'Duration of Course: 4 Years (8 semister)', '', '', '2019-12-26', '0000-00-00'),
(80, 25, 'Fees', '<p>The Annual Fee for BE in Automobile Engineering is INR 1,35,000/- Per Year.</p>\r\n<p>Enrollment Fees 1000/- once in 4 Years.</p>\r\n<p>Examination Fees 1000/- Per Semester.</p>\r\n<p>Hostel Fees 60,000/- Per Year (Food Excluded)</p>', '', '2019-12-26', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `site_cofig`
--

CREATE TABLE `site_cofig` (
  `id` int(11) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `instragram` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `site_title` varchar(555) DEFAULT NULL,
  `keyword` mediumtext,
  `description` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `footer_title` mediumtext,
  `footer_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_cultural`
--

CREATE TABLE `sports_cultural` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `page_section` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `facility_img` varchar(255) NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sports_cultural`
--

INSERT INTO `sports_cultural` (`id`, `page_id`, `page_section`, `title`, `description`, `image`, `facility_img`, `date_created`, `date_modified`) VALUES
(1, 38, 1, '0', '<p>While academics are always important, sports play a major role in shaping the character of a student. Not only do sports keep your body fit and healthy, it offers a great career too. That is the reason university sports are introduced to keep students learn ways to handle pressure in every kind of situation. Sports offer amazing benefits to people. Firstly, they take care of your health to keep your body hale and healthy. Body fat is reduced and aging process is delayed. It also streamlines the lifestyle of students and maintains a great balance of physical and mental development. In addition, you attain an attractive physique.\r\n					We all need a break from classroom and lectures to rejuvenate , extracurricular activities are the best way to regain that energy and enhance your personality. Whether getting involved in student clubs, volunteering activities, sports tournaments, or part-time job, these activities outside the regular classroom, help students meet new people and develop their social skills. </p><p>While academics play a significant role, sports-related activities are also important in shaping the personality and character of a student. With the same ideology, we emphasize on University sports and encourage students to participate in various tournaments. Students not only stay fit and healthy by actively participating in sports, but they also learn effective ways to handle pressure in times of crisis. Some of the benefits are: </p>', '', '', '2019-12-24', '0000-00-00'),
(2, 38, 1, '0', '', 'd9cc0195f7d3337bbee4187328fa6b94.jpg', '', '2019-12-24', '0000-00-00'),
(3, 38, 2, '', '<strong>Improves academic performance:</strong>\r\n					<p>Studies have revealed that students who are involved in athletic activities achieve better scores through their education. Understanding the correlation between physical fitness and academic success, D Y Patil University Pune organizes various sports-related programs including badminton, table tennis, basketball, football, etc. for students.</p>', '', '', '2019-12-24', '0000-00-00'),
(4, 38, 2, '', '<strong>Develops fitness habits:</strong>\r\n					<p>Students develop better fitness habits and coordination by engaging in sports and exercises. Proper stretching exercises and yoga from an early age help them in taking care of their bodies while avoiding health problems.</p>', '', '', '2019-12-24', '0000-00-00'),
(5, 38, 2, '', '<strong>Provides mental and emotional benefits:</strong>\r\n					<p>Not only do sports provide great physical benefits but also help students in boosting and maintaining mental and emotional well-being. When you exercise on a daily basis, your body releases chemicals called endorphins which reduce stress and trigger a positive feeling in the body. It has been proved that students who are physically active and engage in sports are happier and lead a less stressful life.</p>', '', '', '2019-12-24', '0000-00-00'),
(6, 38, 2, '', '<strong>Builds specific skills:</strong>\r\n					<p>Through sports, students are able to build a wide range of abilities and skills such as leadership, confidence, teamwork, patience, self-reliance, trust, and many more which facilitate the overall development of an individual. You become proactive when you need to solve problems while playing on the court. Students also learn to manage time between their lectures, sports, and personal life.</p>', '', '', '2019-12-24', '0000-00-00'),
(7, 38, 2, '', '<strong>Maintains a positive spirit:</strong>\r\n					<p>One of the important advantages of University sports is developing a positive spirit to achieve success while playing for your University. This passion and positive spirit help students achieve their career goals easily. Through sports, they not only bring laurels to their University but get an opportunity to earn a decent income as well. If interested, you can always make an exciting career in sports or athletics.</p>', '', '', '2019-12-24', '0000-00-00'),
(8, 38, 2, '', '<p>While sports offer amazing benefits to students in terms of health and mental development, it also enhances academic performance and helps them achieve better grades. Other than these benefits, students also get a sense of entertainment while getting exposed to different sports tournaments. At our University, you will find many options to stay fit and active. With state-of-the-art facilities on the campus including Cricket Ground, Basketball Court, Football Ground, Multi-gym Station, fitness programs, inter-college sports tournaments, the University provides opportunities for students, faculty members, and staff who aspire for more in-depth sports experience.</p>', '', '', '2019-12-24', '0000-00-00'),
(9, 38, 2, '', '<h4>CULTURAL:</h4>\r\n						<p>The importance of social and cultural activities is preparing students for real life and strengthening their personal skills. Social/cultural activities not only help students to identify themselves with the university, but also assist students to develop themselves in a desired field and also improve skills such as organizational, presentation, leadership and interpersonal communication. As social and cultural activities are of paramount importance, the University encourages all extra-curricular activities that are both in line with the educational objectives of the institution and meet the needs of the students. Culture can be defined as the arts as well as the intangible shared beliefs, values, and practices of a community. Students participate in arts and culture at varying levels of skill and engagement. Some create, while others listen to, watch, teach, critique, or learn a cultural activity, art form, or expression. Some are professional artists, designers, and inventors, while others engage informally in expressive activities or create innovative tools, relationships, or products. The field as a whole can be represented within a framework that has four main aspects: degree of professionalism, type of activity, locations and spaces, and level of participation and involvement. Together, these formal and informal, tangible and intangible, professional and amateur artistic and cultural activities constitute a communityâ€™s cultural assets. Some of the benefits of participating in co-curricular activities are as follows : </p>', '', '', '2019-12-24', '0000-00-00'),
(10, 38, 2, '', '<strong>Social: </strong>\r\n					<p>Extracurricular activities increase opportunities for social interaction and new relationship development. As most of these activities are group-oriented which have students from different niches, which gives them a chance to more know about people of different passions and cultures. Besides, this they can also find clubs or groups that share similar social, religious or even entertainment interest for socialization. Interaction with people of different backgrounds helps in development of interpersonal skills of students.</p>', '', '', '2019-12-24', '0000-00-00'),
(11, 38, 2, '', '<strong>Practical: </strong>\r\n					<p>Extracurricular activities teach students how to work for a common goal and this ultimately develops a sense of responsibility in them. It increases the level of confidence and also teaches them how to co-operate and work with people in different conditions. They learn to face the challenges that come in education and career. Extracurricular activities like dance require a person to remain physically fit. Thus students have to be particular about their health and diet. They have to take diet which can make them physically stronger and also do exercise daily. They start loving their body, which is very important for a happier life.</p>', '', '', '2019-12-24', '0000-00-00'),
(12, 38, 2, '', '<strong>Educational: </strong>\r\n					<p>Most of the employers love to hire people who are all rounders. Sustained involvement in more than one activity reflects the talent and potential of the student. Hiring officials look for these talents in people. Besides, this when applying for admission in colleges for higher degrees, these students get a preference too. While pursuing these activities in college along with education students learn prioritization and time management skills too. These academically and co-circularly talented students have well-groomed personality, which helps them to face the world in a better way.</p>', '', '', '2019-12-24', '0000-00-00'),
(13, 38, 2, '', '', 'f77a1eed9c3fc3be4cf47fe3de0f7ed1.jpg', '', '2019-12-24', '0000-00-00'),
(14, 39, 0, 'CRICKET GROUND', '', '6cfa5010453e5f606ef38c6015c4f1aa.jpg', '84067e52f1dfee09f93742bcbe7f3e52.jpg', '2019-12-24', '0000-00-00'),
(15, 39, 0, 'BASKETBALL COURT', '', 'c6752337ce94392799998d866446bed1.jpg', '9e68bf1fd5d579d1c2e7799ab6f1f34b.jpg', '2019-12-24', '0000-00-00'),
(16, 39, 0, 'FOOTBALL GROUND', '', '7ebbf75280d6c6fd3ee916e1d14dfeef.jpg', '3dc286252b734daeca5872a2518017e7.jpg', '2019-12-24', '0000-00-00'),
(17, 39, 0, 'BADMINTON COURT', '', 'd53610cd305f947ff7dd69fe9d302412.jpg', 'ed1b1f6f7a8d41e724962dad6a900b9e.jpg', '2019-12-24', '0000-00-00'),
(18, 39, 0, 'GYMNASIUM', '', 'd87bbe065989cd50e7f303c309cdc68d.jpg', '973adf0d09003247d9a94e93b7d4767c.jpg', '2019-12-24', '0000-00-00'),
(19, 39, 0, 'SQUASH COURT', '', '057e458082781be5c1e148d3deaa92b8.jpg', 'ba2576305c55da4162b391f5e9de2768.jpg', '2019-12-24', '0000-00-00'),
(20, 39, 0, 'RUNNING TRACK', '', '1a0a7e4b790b2f648db240344c29321f.jpg', '2ba681178f50458089952dcc301139d2.jpg', '2019-12-24', '0000-00-00'),
(21, 39, 0, 'CAROM', '', '2ae554bebf9ae6a4b1d3579d45a81b86.jpg', '34f6cd86fb83578437675a10e1e03b40.jpg', '2019-12-24', '0000-00-00'),
(22, 39, 0, 'TENNIS COURT', '', 'e7c096f4b67f86d6cf99f5e75cc10b39.jpg', '0d608a2b6b5836009f9aa76dd2772181.jpg', '2019-12-24', '0000-00-00'),
(23, 39, 0, 'CHESS', '', '6fceb9570c48b8300a5e89b8e39ece99.jpg', '5bb26ed0e5d3943b3c42bc9e624ef461.jpg', '2019-12-24', '0000-00-00'),
(24, 39, 0, 'POOL', '', '25605e3972092f4c2d6479526191be17.jpg', '05cc89e975b97fd40796be9047b40751.jpg', '2019-12-24', '0000-00-00'),
(25, 39, 0, 'BOXING', '', 'cb049701283a3eddc897f9cb1d3e4a7f.jpg', 'dc115fe74eaeb23169022540aaf82805.jpg', '2019-12-24', '0000-00-00'),
(26, 39, 0, 'Facilities', '<p>The university also has a 600 capacity auditorium and 4 plush halls in which cultural and co-curricular events are organized. Besides, the university also has an amphitheatre for staging street plays and group performances.  Student s participate in intra collegiate programs as well as participate in fests of universities across the country. </p>', '', '', '2019-12-24', '0000-00-00'),
(27, 40, 0, 'Events', '<p>The campus is abuzz with sports tournaments and cultural programs throughout the year. Inter and intra collegiate sports tournaments are organized in cricket, football, badminton & tennis. The university also holds its own Chess League in which students from different colleges participate.</p>\r\n<p>\r\nAll the major festivals of the country are celebrated in the university. These celebrations are flanked by cultural performances, painting & drawing contests, fashion shows, elocution contests, debate competitions, symposiums, street plays etc. </p>', '', '', '2019-12-24', '0000-00-00'),
(28, 40, 0, '', '', 'c14b9aa43e81a364cb47ed53d0f3300f.jpg', '9ff2cd3efa7619ff6b79bc8394b9c761.jpg', '2019-12-24', '0000-00-00'),
(29, 40, 0, '', '', '42fbed55c039d87baad4b06436ea869e.jpg', 'b2becf72152092803a38ecf7672ac4f1.jpg', '2019-12-24', '0000-00-00'),
(30, 40, 0, '', '', '7166e77ae8245ee12c3b9fab9817f56a.jpg', '9858a80781c5b82064cac3c42af9b383.jpg', '2019-12-24', '0000-00-00'),
(31, 40, 0, '', '', '88a420ca2293bbaa56b774bcd60b0cd1.jpg', 'a6c5db7fe706e564acc8ffd75b214be4.jpg', '2019-12-24', '0000-00-00'),
(32, 40, 0, '', '', '588a8064dca0fcc42b428166a6891dfb.jpg', 'e3c1451efe3500bf7f54df73e0efeef9.jpg', '2019-12-24', '0000-00-00'),
(33, 40, 0, '', '', '878988f6f48d267c459dca395afd3969.jpg', 'c8da6bd0d2250de2ce54622ffa039bda.jpg', '2019-12-24', '0000-00-00'),
(34, 40, 0, '', '', '9efcab26b05f0211bf60c8a4501601a7.jpg', '87d63b2dd71eaf02e72a58afa5858539.jpg', '2019-12-24', '0000-00-00'),
(35, 40, 0, '', '', '5727e595281dfc3d0a99011fe13826bf.jpg', '8e87ba481202db313cd6e400823ceff4.jpg', '2019-12-24', '0000-00-00'),
(36, 40, 0, '', '', 'ebf83c608ec34761f1639a8fdd3b6c1e.jpg', '38cd789568e080eff3c8c01c67b5f70b.jpg', '2019-12-24', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` text NOT NULL,
  `address` text NOT NULL,
  `map` text NOT NULL,
  `status` enum('active','deactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id` int(11) NOT NULL,
  `username` varchar(140) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` int(11) DEFAULT '0' COMMENT '0=admin,1=user',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `zip` varchar(15) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `contact_no` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `description` text,
  `profile_pic` varchar(200) DEFAULT NULL,
  `banner_pic` varchar(200) DEFAULT NULL,
  `verify_key` varchar(100) DEFAULT NULL,
  `verify` enum('Yes','No') DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`id`, `username`, `password`, `role`, `created_at`, `fname`, `lname`, `email`, `designation`, `address`, `zip`, `city`, `contact_no`, `state`, `country`, `description`, `profile_pic`, `banner_pic`, `verify_key`, `verify`) VALUES
(2, 'admin', '4297f44b13955235245b2497399d7a93', 0, '2016-10-07 13:40:12', 'John', 'Deo', 'admin@gmail.com', 'Administrator', 'b-317, sharv dhram colony, kolar road bhopal', '462042', 'bhopal', '8982127797', 'india', 'India', 'Buki Yuushuu proudly presents our Professional Martial Arts Weapons. We manufacture the best weapons in the industry including Nunchucks, Bo Staffs, Kali, Arnis, Escrima and Kamas.', 'f22580735e52ae25f658d98dd030c94f.jpg', '789d7a6ad7f7c547b183be420d6ebd93.png', NULL, 'Yes'),
(6, 'shriram14', '4297f44b13955235245b2497399d7a93', 1, '2016-12-14 20:58:59', 'shriram', 'chaurasiya', 'rahul@gmail.com', 'Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(5, 'rohit@gmail.com', '4297f44b13955235245b2497399d7a93', 1, '2016-12-14 20:53:52', 'chaurasiya', 'wedding', 'shri.ramchaurasiya14@gmail.com', 'marketing executive', 'b-317, sharv dhram colony, kolar road bhopal', '462042', 'bhopal', '8982127797', 'india', 'India', 'Buki Yuushuu proudly presents our Professional Martial Arts Weapons. We manufacture the best weapons in the industry including Nunchucks, Bo Staffs, Kali, Arnis, Escrima and Kamas.', '8d0a3d7b4f6bc5db1ca41eab270d0c57.png', 'd9bbe5fdeb90b39859f3dbda37d96d3b.jpg', NULL, 'Yes'),
(13, 'dksri', '202cb962ac59075b964b07152d234b70', 1, '2016-12-26 16:27:35', 'chaurasiya', 'test', 'info@peacear.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(14, 'ianmark', '4297f44b13955235245b2497399d7a93', 1, '2016-12-31 07:23:58', 'Ian', 'mark', 'Ian@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(15, 'gandharvtamrakar', '64d5c72c7cac31f37dc550a86a7c5cc5', 1, '2016-12-31 17:57:48', 'gandharv', 'Tamrakar', 'gandharv.tamrakar@gmail.com', NULL, '', '', '', '', '', '', '', NULL, '9c7c6ee4e3f1394084454e5208eb4638.jpg', NULL, 'Yes'),
(16, 'test', 'e10adc3949ba59abbe56e057f20f883e', 1, '2017-01-02 16:00:26', 'test', 'test', 'test@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(17, 'Danishwe', '9013353389588ac108222f37157d7119', 1, '2017-01-02 16:09:30', 'Danishwe', 'Danishwe', 'Danishwe@gmail.com', NULL, '', '', '', '', '', '', '', '1ff10fb3cf6a58f49250438709391719.png', '1ff10fb3cf6a58f49250438709391719.png', NULL, 'Yes'),
(18, 'greesh', '64d5c72c7cac31f37dc550a86a7c5cc5', 1, '2017-01-04 12:44:45', 'greesh', 'tamrakar', 'greesh.tamrakar@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(19, 'test', 'e10adc3949ba59abbe56e057f20f883e', 1, '2017-01-07 03:31:39', 'test', 'test', 'test@om.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(20, '', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2017-01-23 09:38:35', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(21, 'test', 'e10adc3949ba59abbe56e057f20f883e', 1, '2017-01-27 16:39:14', 'test', 'test', 'info@rajsoft.in', NULL, '', '', '', '', '', '', '', '3f940e3829ccf887d8edd1e895d29095.jpg', '3f940e3829ccf887d8edd1e895d29095.jpg', NULL, 'Yes'),
(22, 'sssss', '81dc9bdb52d04dc20036dbd8313ed055', 1, '2017-01-27 16:57:26', 'sss', 'sss', 'ssss@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(23, 'swadeshiniraj', '0a5083cf18bf9522c9c414b754bddacf', 1, '2017-01-31 06:25:10', 'niraj', 'om', 'swadeshiniraj@rediffmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes'),
(24, 'ian', '4297f44b13955235245b2497399d7a93', 1, '2017-02-01 11:22:20', 'ian 1', 'mark 12', 'lintoffandrew581@gmail.com', NULL, 'kolar', '485115', 'Satna', '8982127797', 'Madhya Pradesh', 'India', 'We hope this article helped you find the best free 404 plugins for your WordPress site. You may also want to take a look at these WordPress 404 error page designs for inspiration. ', '09a1a8e825d2dff4c01f282273c719b0.jpg', '86dd51667d0849b1335e06780b0b9f0d.jpg', 'Vm7zN8L0xUJE5eqsH3PFgtBW9Rkv1c64', 'Yes'),
(25, 'Ianmark', '4297f44b13955235245b2497399d7a93', 1, '2017-02-01 18:11:39', 'ian', 'mark', 'flintoffandrew581@gmail.com', NULL, '', '78976', '', '', '', '', '', '0bb9a17d874b62492d73747dfc4c6211.jpg', '6b8cf80a4325498a8b3a13f807365101.jpg', 'doH73a4xJEB961YsWqkcRvN8iPFzUeg2', 'Yes'),
(26, 'danish', '4297f44b13955235245b2497399d7a93', 1, '2017-02-01 18:13:01', 'danish', 'parvez', 'danish.ndseo@gmail.com', NULL, 'Software Used', '', '', '12344', '', '', '', 'c8c3cbf4c57a99d9d6d7622c278682ec.png', '5d4f67cce7c63dc6143bbfe12b35e5cf.png', 'ym0WR3q2YxJvEeB7Pt9ag4185UFH6coN', 'Yes'),
(27, 'jaiho', '68053af2923e00204c3ca7c6a3150cf7', 1, '2017-02-02 06:19:34', 'jai', 'ho', 'sales@rajsoft.in', NULL, '', '', '', '', '', '', '', 'site direction.jpg', 'b890c9b635214bfd2f26ea9792cb4c96.jpg', 'taiFL6km8co1qs59J23NE0HtgveUW4PR', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `academic_calendar`
--
ALTER TABLE `academic_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `academic_calendar_header`
--
ALTER TABLE `academic_calendar_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission_process`
--
ALTER TABLE `admission_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committees`
--
ALTER TABLE `committees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_of_values`
--
ALTER TABLE `list_of_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `placements_and_internships`
--
ALTER TABLE `placements_and_internships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `research`
--
ALTER TABLE `research`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_department`
--
ALTER TABLE `school_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_cofig`
--
ALTER TABLE `site_cofig`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_cultural`
--
ALTER TABLE `sports_cultural`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `academic_calendar`
--
ALTER TABLE `academic_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `academic_calendar_header`
--
ALTER TABLE `academic_calendar_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admission_process`
--
ALTER TABLE `admission_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `committees`
--
ALTER TABLE `committees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `list_of_values`
--
ALTER TABLE `list_of_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `placements_and_internships`
--
ALTER TABLE `placements_and_internships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `research`
--
ALTER TABLE `research`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `school_department`
--
ALTER TABLE `school_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `site_cofig`
--
ALTER TABLE `site_cofig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sports_cultural`
--
ALTER TABLE `sports_cultural`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
